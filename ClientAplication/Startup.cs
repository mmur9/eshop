﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClientAplication.Startup))]
namespace ClientAplication
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
