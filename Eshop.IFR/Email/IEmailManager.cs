﻿namespace Eshop.IFR.Email
{
    /// <summary>
    /// Interfaz con metodos de emailManager
    /// </summary>
    public interface IEmailManager
    {
        void SendMyMail(string destinatario, string asunto, string mensaje);
    }
}