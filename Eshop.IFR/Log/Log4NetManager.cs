﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Let log4net know that it can look for configuration in the default application config file
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace Eshop.IFR.Log
{
    public class Log4NetManager : ILogEvent
    {

        /// <summary>
        /// Interfaz de log
        /// </summary>
        private readonly ILog Log = null;

        /// <summary>
        /// constructor
        /// </summary>
        public Log4NetManager()
        {
            //Creamos el logger de log4net con la config
            Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }



        /// <summary>
        /// metodo para crear mensajes de info
        /// </summary>
        /// <param name="message"></param>
        public void WriteInfo(string message)
        {

            Log.Info(message);

        }

        /// <summary>
        /// metodo para crear mensajes de error
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public void WriteError(string message, Exception ex)
        {
            Log.Error(message, ex);
        }

    }
}
