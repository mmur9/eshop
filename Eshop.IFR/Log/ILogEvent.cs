﻿using System;

namespace Eshop.IFR.Log
{
    public interface ILogEvent
    {

        void WriteInfo(string message);
        void WriteError(string message, Exception ex);

    };
}