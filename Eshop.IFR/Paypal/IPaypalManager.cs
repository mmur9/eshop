﻿using PayPal.Api;

namespace Eshop.IFR.Paypal
{
    public interface IPaypalManager
    {
        APIContext GetAPIContext();
    }
}