﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.IFR.Paypal
{
    class PaypalManager : IPaypalManager
    {
        public readonly static string clientId;
        public readonly static string clientSecret;


        static PaypalManager()
        {
            var config = getConfig();
            clientId = "ARG4vtx6JJNx-SZq1BBYx4iumlIpk1RI5rlmxcnSQqn40RR_WHnCJTeDO7pk2Yubz9glpxJ16K9vt9W6";
            clientSecret = "EKXMmz7I1T-8oMzxruX8tttGUOKpVtx_rR8kEi3HIgkyStouvoWqv8gHAPiJpelSq-eLWGMiULUiT6Fb";
        }


        private static Dictionary<string, string> getConfig()
        {
            return PayPal.Api.ConfigManager.Instance.GetProperties();
        }

        private static string GetAccessToken()
        {
            string accessToken = new OAuthTokenCredential(clientId, clientSecret, getConfig()).GetAccessToken();

            return accessToken;
        }

        //antes era estatico, lo pasamos a publico para la interfaz
        public APIContext GetAPIContext()
        {
            APIContext apiContext = new APIContext(GetAccessToken());

            apiContext.Config = getConfig();

            return apiContext;
        }
    }
}
