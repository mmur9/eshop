﻿using Eshop.CORE.Contracts;
using Eshop.IFR.Log;
using Eshop.MVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eshop.MVC.Areas.Administrator.Controllers
{

    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {

        private IMyUserManager _userManager = null;
        private ILogEvent _logManager = null;


        public UsersController(IMyUserManager userManager, ILogEvent logManager)
        {
            this._userManager = userManager;
            this._logManager = logManager;
        }




        // GET: Administrator/Users
        public ActionResult Index()
        {
            var roleStore = new RoleStore<IdentityRole>();
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            var roles = roleMngr.Roles.ToList();



            var model = _userManager.GetUsers().Select(e => new EditUserViewModel
            {

                Id = e.Id,
                Email = e.Email,
                Name = e.FirstName,
                LastName = e.LastName,
                Phone = e.PhoneNumber,
                Address = e.Address,
                City = e.City,
                Country = e.Country,
                PostalCode = e.PostalCode,
                State = e.State,
                Role = e.Roles.FirstOrDefault().RoleId



            });

            return View(model);
        }

        // GET: Administrator/Users/Details/5
        public ActionResult Details(String id)
        {

            var model = _userManager.GetUsers().Where(e => e.Id == id).Select(e => new EditUserViewModel
            {
                Id = e.Id,
                Email = e.Email,
                Name = e.FirstName,
                LastName = e.LastName,
                Phone = e.PhoneNumber,
                Address = e.Address,
                City = e.City,
                Country = e.Country,
                PostalCode = e.PostalCode,
                State = e.State,
                Role = e.Roles.FirstOrDefault().RoleId
            }).FirstOrDefault();

            return View(model);
        }


        // GET: Administrator/Users/Edit/5
        public ActionResult Edit(String id)
        {

            try
            {
                var roleStore = new RoleStore<IdentityRole>();
                var roleMngr = new RoleManager<IdentityRole>(roleStore);

                var roles = roleMngr.Roles.ToList();

                IEnumerable<IdentityRole> enumRoles = roles.AsEnumerable();


                var model = _userManager.GetUsers().Where(e => e.Id == id).Select(e => new EditUserViewModel
                {
                    Id = e.Id,
                    Email = e.Email,
                    Name = e.FirstName ?? string.Empty,
                    LastName = e.LastName ?? string.Empty,
                    Phone = e.PhoneNumber ?? string.Empty,
                    Address = e.Address ?? string.Empty,
                    City = e.City ?? string.Empty,
                    Country = e.Country ?? string.Empty,
                    PostalCode = e.PostalCode ?? string.Empty,
                    State = e.State ?? string.Empty,
                    Role = e.Roles.FirstOrDefault().RoleId



                }).FirstOrDefault();

                // añadimos lista de roles para poder escoger en dropdown
                if (model != null)
                {
                    model.ExistentRoles = enumRoles;

                }

                return View(model);
            }
            catch (Exception e)
            {
                _logManager.WriteError(e.Message, e);
                return RedirectToAction("Index");
            }

        }

        // POST: Administrator/Users/Edit/5
        [HttpPost]
        public ActionResult Edit(EditUserViewModel model)
        {
            //Recogemos los roles existentes

            var roleStore = new RoleStore<IdentityRole>();
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            var roles = roleMngr.Roles.ToList();

            IEnumerable<IdentityRole> enumRoles = roles.AsEnumerable();

            try
            {
                var currentUser = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(model.Id);
                if (currentUser != null)
                {

                    currentUser.FirstName = model.Name;
                    currentUser.LastName = model.LastName;
                    currentUser.PhoneNumber = model.Phone;
                    currentUser.Address = model.Address;
                    currentUser.City = model.City;
                    currentUser.Country = model.Country;
                    currentUser.PostalCode = model.PostalCode;
                    currentUser.State = model.State;


                    /*Limpiamos los roles del usuario*/

                    //Añadimos rol

                    String usuario = model.Id;
                    String usuarioRol = model.Role;



                    //HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().RemoveFromRoles(currentUser.Id, currentUser.Roles.ToString());

                    try
                    {
                        //eliminamos roles de usuario

                        String[] rolesDeUsuario = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().GetRoles(model.Id).ToArray();
                        HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().RemoveFromRoles(model.Id, rolesDeUsuario);

                        //añadimos usuario al rol
                        HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().AddToRole(usuario, usuarioRol);


                    }
                    catch (Exception e)
                    {
                        String h = "";
                        _logManager.WriteError(e.Message, e);

                    }

                    //Actualizamos el usuario
                    HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().Update(currentUser);
                    //Le pasamos al modelo los roles existentes para el listado
                    model.ExistentRoles = enumRoles;


                }
                else
                {

                }

                //Si todo ha ido bien retornamos la vista de edit pasandole el modelo que hemos actualizado (emulando una recarga de la pagina)


                return Redirect("https://localhost:44337/Administrator/Users/Edit/" + model.Id);
            }
            catch (Exception e)
            {

                ModelState.AddModelError("", "Se ha producido un error, contacte con su administrador.");
                _logManager.WriteError(e.Message, e);

                return View("Index");
            }
        }

        // GET: Administrator/Users/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administrator/Users/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logManager.WriteError(e.Message, e);

                return View();
            }
        }
    }
}
