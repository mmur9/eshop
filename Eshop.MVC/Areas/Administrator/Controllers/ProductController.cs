﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.IFR.Log;
using Eshop.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eshop.MVC.Areas.Administrator.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductController : Controller
    {

        private ILogEvent _log = null;
        private IProductManager _productManager = null;
        private ICategoryManager _categoryManager = null;
        private IImageManager _imageManager = null;


        public ProductController(IProductManager productManager, ILogEvent log)
        {

            this._productManager = productManager;
            this._log = log;

        }

        public ProductController(IProductManager productManager, ICategoryManager categoryManager, ILogEvent log)
        {

            this._productManager = productManager;
            this._categoryManager = categoryManager;
            this._log = log;

        }

        public ProductController(IProductManager productManager, ICategoryManager categoryManager, IImageManager imageManager, ILogEvent log)
        {

            this._productManager = productManager;
            this._categoryManager = categoryManager;
            this._imageManager = imageManager;
            this._log = log;
        }


        // GET: Administrator/Product
        public ActionResult IndexBonito()
        {
            try
            {

                _log.WriteInfo("Entrando en indexbonito");

            }
            catch (Exception e)
            {

                string prueba = "2";
            }

            var model = _productManager.GetAll().Select(e => new ProductListAdmin
            {
                Id = e.Id,
                Name = e.Name.ToString(),
                Description = e.Description.ToString(),
                UnitsInStock = e.UnitsInStock,
                Duration = e.Duration,
                Price = e.Price,
                Imagenes = e.Imagenes,
                YtURL = e.YtURL,
                Category = e.Category


            });
            return View(model);
        }

        // GET: Administrator/Product
        public ActionResult Index()
        {

            var model = _productManager.GetAll().Select(e => new ProductListAdmin
            {
                Id = e.Id,
                Name = e.Name.ToString(),
                Description = e.Description.ToString(),
                UnitsInStock = e.UnitsInStock,
                Duration = e.Duration,
                Price = e.Price,
                Imagenes = e.Imagenes,
                YtURL = e.YtURL,
                Category = e.Category


            });
            return View(model);
        }

        // GET: Administrator/Product/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                var product = _productManager.GetProductByIdWithImages(id).FirstOrDefault();
                if (product != null)
                {
                    ProductEditModel model = new ProductEditModel
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Description = product.Description,
                        Duration = product.Duration,
                        Price = Decimal.ToInt32(product.Price),
                        UnitsInStock = product.UnitsInStock,
                        YtURL = product.YtURL,
                        Category = product.Category,


                        Categorias = _categoryManager.GetAllCategories().Select(p => new SelectListItem
                        {
                            Value = p.Id.ToString(),
                            Text = p.Name
                        })
                    };
                    return View(model);


                }
                else
                {
                    return RedirectToAction("Index");

                }
            }
            catch (Exception e)
            {
                _log.WriteError(e.Message, e);

                return RedirectToAction("Index");

            }


        }

        // GET: Administrator/Product/Create
        public ActionResult Create()
        {

            try
            {

                //recogemos el modelo de las categorias para pasarselo a la vista, así podremos elegir mediante un DDL
                ProductEditModel model = new ProductEditModel
                {

                    Categorias = _categoryManager.GetAllCategories().Select(p => new SelectListItem
                    {
                        Value = p.Id.ToString(),
                        Text = p.Name
                    })
                };


                return View(model);

            }
            catch (Exception e)
            {
                _log.WriteError(e.Message, e);
                return RedirectToAction("Index");



            }

        }

        // POST: Administrator/Product/Create
        [HttpPost]
        public ActionResult Create(ProductEditModel model)
        {
            //recogemos la categoria seleccionada del DDL del modelo
            model.Category = _categoryManager.GetAllCategories().Where(c => c.Id.Equals(model.SelectedCategory)).FirstOrDefault();

            try
            {

                //Cremamos un nuevo producto con las propiedades del modelo del formulario
                Product product = new Product
                {
                    Name = model.Name,
                    Description = model.Description,
                    UnitsInStock = model.UnitsInStock,
                    Duration = model.Duration,
                    Price = model.Price,
                    YtURL = model.YtURL,
                    Category = model.Category


                };

                //Creamos el producto y guardamos cambios en el contexto de datos
                _productManager.Add(product);
                _productManager.Context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _log.WriteError(e.Message, e);

                return View();
            }
        }

        // GET: Administrator/Product/Edit/5
        public ActionResult Edit(int id)
        {

            try
            {
                //recogemos el prodcto buscando por id
                var product = _productManager.GetProductByIdWithImages(id).FirstOrDefault();

                //si el producto existe creamos un modelo con los datos del producto
                if (product != null)
                {
                    ProductEditModel model = new ProductEditModel
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Description = product.Description,
                        Duration = product.Duration,
                        Price = Decimal.ToInt32(product.Price),
                        UnitsInStock = product.UnitsInStock,
                        YtURL = product.YtURL,
                        Category = product.Category,
                        Imagenes = product.Imagenes,

                        //recogemos las categorias existentes para pasarselas al desplegable de selección de categoria
                        Categorias = _categoryManager.GetAllCategories().Select(p => new SelectListItem
                        {
                            Value = p.Id.ToString(),
                            Text = p.Name
                        })
                    };
                    return View(model);


                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _log.WriteError(e.Message, e);

                return RedirectToAction("Index");
            }

        }

        // POST: Administrator/Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProductEditModel model)
        {
            //recogemos la categoria seleccionada del DDL
            model.Category = _categoryManager.GetAllCategories().Where(c => c.Id.Equals(model.SelectedCategory)).FirstOrDefault();

            try
            {
                //recogemos el producto por el id del param y asi poder saber cual guardar
                var product = _productManager.GetProductByIdWithImages(id).FirstOrDefault();

                //si existe ese producto le pasamos los atributos del modelo rellenado x el formulario
                if (product != null)
                {
                    product.Name = model.Name;
                    product.Description = model.Description;
                    product.Duration = model.Duration;
                    product.UnitsInStock = model.UnitsInStock;
                    product.YtURL = model.YtURL;
                    product.Category = model.Category;
                    product.Price = model.Price;
                    //excepcion para prueba
                    //throw new Exception("");

                    //guardamos cambios 
                    _productManager.Context.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {

                _log.WriteError(e.Message, e);

                return RedirectToAction("Index");
            }
        }

        // GET: Administrator/Product/Delete/5
        public ActionResult Delete(int id)
        {

            try
            {
                //recogemos el producto a eliminar a través de su ID
                var product = _productManager.GetById(id);

                //si existe el produucto rellenamos el modelo con las propiedades del producto encontrado
                if (product != null)
                {
                    ProductListAdmin model = new ProductListAdmin
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Description = product.Description,
                        Duration = product.Duration,
                        Price = Decimal.ToInt32(product.Price),
                        UnitsInStock = product.UnitsInStock,
                        YtURL = product.YtURL,
                        Category = product.Category
                    };
                    return View(model);
                    //ejemplo comprobacion persona
                    /*
                     * if(incidence.User_Id == User.Identity.GetUserId())
                     */
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _log.WriteError(e.Message, e);

                return RedirectToAction("Index");

            }

        }

        // POST: Administrator/Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ProductListAdmin model)
        {
            try
            {
                //recogemos el producto a eliminar a través de su ID
                var product = _productManager.GetById(id);

                //si el producto existe, lo eliminamos mediante el manager
                if (product != null)
                {

                    _productManager.Remove(product);
                    _productManager.Context.SaveChanges();
                }


                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _log.WriteError(e.Message, e);
                return RedirectToAction("Index");
            }
        }


        // POST: Administrator/Product/AddImage

        /// <summary>
        /// Metodo que añade imagenes de producto
        /// </summary>
        /// <param name="id">ID DEL PRODUCTO</param>
        /// <param name="urlImage">URL DE LA IMAGEN A AÑADIR</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddImage(int idProducto, String urlImageParam)
        {
            try
            {
                //si el producto existe creamos un nuevo objeto imagen con objeto producto y url de imagen
                var product = _productManager.GetProductByIdWithImages(idProducto).FirstOrDefault();
                if (product != null)
                {

                    Image newImage = new Image { Product = product, urlImage = urlImageParam };

                    //mediante el manager de producto creamos una nueva
                    product.Imagenes.Add(newImage);

                    //excepcion para prueba
                    //throw new Exception("");

                    _productManager.Context.SaveChanges();
                }
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _log.WriteError(e.Message, e);

                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult RemoveImage(int idImage)
        {
            try
            {
                //recogemos el objeto imagen por su id
                var imageToDelete = _imageManager.GetById(idImage);

                if (imageToDelete != null)
                {
                    //eliminamos la imagen
                    _imageManager.Remove(imageToDelete);
                    _imageManager.Context.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _log.WriteError(e.Message, e);

                return RedirectToAction("Index");
            }
        }

    }
}
