﻿using Eshop.CORE.Contracts;
using Eshop.IFR.Email;
using Eshop.IFR.Log;
using Eshop.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eshop.MVC.Areas.Administrator.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OrderController : Controller

    {

        private IOrderManager _orderManager = null;
        private IEmailManager _emailManager = null;
        private ILogEvent _logManager = null;

        public OrderController(IOrderManager orderManager, IEmailManager emailManager, ILogEvent logManager)
        {

            this._orderManager = orderManager;
            this._emailManager = emailManager;
            this._logManager = logManager;
        }

        // GET: Administrator/Order
        public ActionResult Index()
        {
            //recogemos todos los pedidos y los retornamos a la vista
            var model = _orderManager.GetAll().Select(e => new OrderAdminViewModel
            {
                Id = e.Id,
                OrderDate = e.OrderDate,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Address = e.Address,
                City = e.City,
                State = e.State,
                PostalCode = e.PostalCode,
                Country = e.Country,
                Phone = e.Phone,
                Email = e.Email,
                Total = e.Total,
                PaymentTransactionId = e.PaymentTransactionId,
                HasBeenShipped = e.HasBeenShipped,
                User_Id = e.User_Id,
                Status = e.Status


            });
            return View(model);
        }

        // GET: Administrator/Order/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                //recogemos el pedido mediante su id
                var order = _orderManager.GetByIdIncOrderDet(id);

                //si existe creamos un modelo con sus datos y lo retornamos a la vista
                if (order != null)
                {
                    OrderAdminViewModel model = new OrderAdminViewModel
                    {
                        Id = order.Id,
                        OrderDate = order.OrderDate,
                        FirstName = order.FirstName,
                        LastName = order.LastName,
                        Address = order.Address,
                        City = order.City,
                        State = order.State,
                        PostalCode = order.PostalCode,
                        Country = order.Country,
                        Phone = order.Phone,
                        Email = order.Email,
                        Total = order.Total,
                        PaymentTransactionId = order.PaymentTransactionId,
                        HasBeenShipped = order.HasBeenShipped,
                        User_Id = order.User_Id,
                        Status = order.Status,
                        OrderDetails = order.OrderDetails
                    };
                    return View(model);

                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                _logManager.WriteError(e.Message, e);

                return RedirectToAction("Index");

            }


        }


        // GET: Administrator/Order/Edit/5
        public ActionResult Edit(int id)
        {

            try
            {

                //recogemos el pedido por su ID
                var order = _orderManager.GetById(id);

                //si existe rellenamos el modelo y lo pasamos a la vista
                if (order != null)
                {
                    OrderAdminViewModel model = new OrderAdminViewModel
                    {
                        Id = order.Id,
                        OrderDate = order.OrderDate,
                        FirstName = order.FirstName,
                        LastName = order.LastName,
                        Address = order.Address,
                        City = order.City,
                        State = order.State,
                        PostalCode = order.PostalCode,
                        Country = order.Country,
                        Phone = order.Phone,
                        Email = order.Email,
                        Total = order.Total,
                        PaymentTransactionId = order.PaymentTransactionId,
                        HasBeenShipped = order.HasBeenShipped,
                        User_Id = order.User_Id,
                        Status = order.Status
                    };
                    return View(model);

                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                _logManager.WriteError(e.Message, e);

                return RedirectToAction("Index");
            }

        }

        // POST: Administrator/Order/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, OrderAdminViewModel model)
        {
            try
            {
                //recogemos pedido por id
                var order = _orderManager.GetById(id);
                //si existe le pasamos la propiedad estado, ya que es la unica que permitimos cambiar
                if (order != null)
                {


                    order.Status = model.Status;


                    //guardamos cambios
                    _orderManager.Context.SaveChanges();
                }
                // TODO: Add update logic here

                //enviamos email
                _emailManager.SendMyMail(order.Email, "Actualización sobre su pedido " + order.Id + " EshopM", "Su pedido " + order.Id + " ha sido actualizado, su nuevo estado es " + order.Status.ToString());

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {

                _logManager.WriteError(e.Message, e);

                return RedirectToAction("Index");
            }
        }

        // GET: Administrator/Order/Delete/5
        public ActionResult Delete(int id)
        {

            try
            {
                //recogemos pedido por ID
                var order = _orderManager.GetById(id);
                //si existe rellenamos el modelo con sus datos y lo retornamos a la vista
                if (order != null)
                {
                    OrderAdminViewModel model = new OrderAdminViewModel
                    {
                        Id = order.Id,
                        OrderDate = order.OrderDate,
                        FirstName = order.FirstName,
                        LastName = order.LastName,
                        Address = order.Address,
                        City = order.City,
                        State = order.State,
                        PostalCode = order.PostalCode,
                        Country = order.Country,
                        Phone = order.Phone,
                        Email = order.Email,
                        Total = order.Total,
                        PaymentTransactionId = order.PaymentTransactionId,
                        HasBeenShipped = order.HasBeenShipped,
                        User_Id = order.User_Id,
                        Status = order.Status
                    };
                    return View(model);

                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logManager.WriteError(e.Message, e);

                return RedirectToAction("Index");
            }

        }

        // POST: Administrator/Order/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, OrderAdminViewModel model)
        {
            try
            {
                //recogemos el pedido por su id
                var order = _orderManager.GetById(id);

                //si existe lo pasamos al manager para eliminarlo y guardar cambios
                if (order != null)
                {

                    _orderManager.Remove(order);
                    _orderManager.Context.SaveChanges();
                }


                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                _logManager.WriteError(e.Message, e);

                return RedirectToAction("Index");
            }
        }
    }
}
