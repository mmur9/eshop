﻿using Eshop.IFR.Log;
using Eshop.MVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eshop.MVC.Areas.Administrator.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleController : Controller
    {

        private ILogEvent _logManager = null;


        public RoleController(ILogEvent logManager)
        {
            this._logManager = logManager;
        }
        /// <summary>
        /// Metodo que nos devuelve una lista de roles existentes
        /// </summary>
        /// <returns></returns>
        public List<IdentityRole> getRoles()
        {
            //recogemos roles existentes
            var roleStore = new RoleStore<IdentityRole>();
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            var roles = roleMngr.Roles.ToList();

            //retornamos el listado
            return roles;

        }

        // GET: Administrator/Role
        public ActionResult Index()
        {


            //hacemos un listado 
            var roles = getRoles();

            //recogemos los roles y los pasamos a la vista
            var model = roles.Select(e => new RoleViewModel
            {
                Id = e.Id,
                Nombre = e.Name,
                nUsuarios = e.Users.Count

            });

            return View(model);
        }



        // POST: Administrator/Role/Create
        [HttpPost]
        public ActionResult Create(RoleViewModel model)
        {
            //instanciamos rolemanager
            var roleStore = new RoleStore<IdentityRole>();
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            try
            {
                //Creamos rol con el nombre del formulario
                IdentityRole identityRole = new IdentityRole
                {
                    Name = model.Nombre

                };

                //Creamos mediante el manager
                roleMngr.Create(identityRole);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logManager.WriteError(e.Message, e);

                return View();
            }
        }

        // GET: Administrator/Role/Edit/5
        public ActionResult Edit(String id)
        {

            try
            {
                var roleStore = new RoleStore<IdentityRole>();
                var roleMngr = new RoleManager<IdentityRole>(roleStore);

                //buscamos rol por ID
                var rolEdit = roleMngr.FindById(id);

                //le pasamos al modelo los atributos del rol y los pintamos
                RoleViewModel model = new RoleViewModel
                {
                    Id = rolEdit.Id,
                    Nombre = rolEdit.Name
                };

                return View(model);
            }
            catch (Exception e)
            {
                _logManager.WriteError(e.Message, e);

                return RedirectToAction("Index");


            }



        }

        // POST: Administrator/Role/Edit/5
        [HttpPost]
        public ActionResult Edit(RoleViewModel model)
        {
            try
            {
                var roleStore = new RoleStore<IdentityRole>();
                var roleMngr = new RoleManager<IdentityRole>(roleStore);

                //creamos rol con los datos del modelo del formulario y actualizados con el manager
                IdentityRole rolActualizado = new IdentityRole
                {
                    Id = model.Id,
                    Name = model.Nombre
                };

                roleMngr.Update(rolActualizado);



                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logManager.WriteError(e.Message, e);

                return View();
            }
        }

        // GET: Administrator/Role/Delete/5
        public ActionResult Delete(String id)
        {
            var roleStore = new RoleStore<IdentityRole>();
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            //capturamos el rol a eliminar
            var rolDel = roleMngr.FindById(id);

            //si no es null pasamos a modelo y retornamos en la vista
            if (rolDel != null)
            {
                RoleViewModel model = new RoleViewModel
                {
                    Id = rolDel.Id,
                    Nombre = rolDel.Name
                };

                return View(model);

            }

            return RedirectToAction("Index");



        }

        // POST: Administrator/Role/Delete/5
        [HttpPost]
        public ActionResult Delete(RoleViewModel model)
        {
            try
            {

                var roleStore = new RoleStore<IdentityRole>();
                var roleMngr = new RoleManager<IdentityRole>(roleStore);

                //capturamos el rol a eliminar
                if (model != null)
                {
                    var rolDel = roleMngr.FindById(model.Id);

                    //si existe lo eliminamos y volvemos al index
                    if (rolDel != null)
                    {

                        roleMngr.Delete(rolDel);
                    }

                }

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {

                _logManager.WriteError(e.Message, e);

                return View();
            }
        }



    }
}
