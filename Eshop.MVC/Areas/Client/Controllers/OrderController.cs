﻿using Eshop.CORE.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Eshop.MVC.Models;
using Eshop.IFR.Log;

namespace Eshop.MVC.Areas.Client.Controllers
{

    [Authorize(Roles = "Client,Admin")]
    public class OrderController : Controller
    {

        private IOrderManager _orderManager = null;
        private ILogEvent _logManager = null;

        public OrderController(IOrderManager orderManager, ILogEvent logManager)
        {

            this._orderManager = orderManager;
            this._logManager = logManager;
        }


        // GET: Client/Order
        public ActionResult Index()
        {
            try
            {

                if (User.Identity.IsAuthenticated)
                {
                    var model = _orderManager.GetByUserId(User.Identity.GetUserId()).Select(e => new OrderClientViewModel
                    {
                        Id = e.Id,
                        OrderDate = e.OrderDate,
                        FirstName = e.FirstName,
                        LastName = e.LastName,
                        Address = e.Address,
                        City = e.City,
                        State = e.State,
                        PostalCode = e.PostalCode,
                        Country = e.Country,
                        Phone = e.Phone,
                        Email = e.Email,
                        Total = e.Total,
                        PaymentTransactionId = e.PaymentTransactionId,
                        HasBeenShipped = e.HasBeenShipped,
                        User_Id = e.User_Id,
                        Status = e.Status,
                        OrderDetails = e.OrderDetails.ToList()


                    });

                    return View(model);
                }

            }
            catch(Exception e)
            {
                _logManager.WriteError(e.Message, e);
                return RedirectToAction("Index", "Shop", new { area = "" });

            }

            return RedirectToAction("Index", "Shop", new { area = "" });

        }

        // GET: Client/Order/Details/5
        public ActionResult Details(int id)
        {
            try
            {

                if (User.Identity.IsAuthenticated)
                {

                    var myOrder = _orderManager.GetByIdIncOrderDet(id);

                    if (myOrder != null && myOrder.User_Id.Equals(User.Identity.GetUserId()))
                    {

                        OrderClientViewModel model = new OrderClientViewModel
                        {
                            Id = myOrder.Id,
                            OrderDate = myOrder.OrderDate,
                            FirstName = myOrder.FirstName,
                            LastName = myOrder.LastName,
                            Address = myOrder.Address,
                            City = myOrder.City,
                            State = myOrder.State,
                            PostalCode = myOrder.PostalCode,
                            Country = myOrder.Country,
                            Phone = myOrder.Phone,
                            Email = myOrder.Email,
                            Total = myOrder.Total,
                            PaymentTransactionId = myOrder.PaymentTransactionId,
                            HasBeenShipped = myOrder.HasBeenShipped,
                            User_Id = myOrder.User_Id,
                            Status = myOrder.Status,
                            OrderDetails = myOrder.OrderDetails
                        };

                        return View(model);

                    }
                    else
                    {
                        return RedirectToAction("Index", "Order");

                    }
                }

            }
            catch(Exception e)
            {
                _logManager.WriteError(e.Message, e);

                return RedirectToAction("Index", "Shop", new { area = "" });

            }

            return RedirectToAction("Index", "Shop", new { area = "" });
        }


    }
}
