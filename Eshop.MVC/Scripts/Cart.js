﻿/*
	Add to cart fly effect with jQuery. - May 05, 2013
	(c) 2013 @ElmahdiMahmoud - fikra-masri.by
	license: https://www.opensource.org/licenses/mit-license.php
*/

$('.add-to-cart').on('click', function(){
    var productId = $(this).data("id");
    var quantity = $(this).data();
    $.ajax({
        type: "POST",
        url: "/CartItem/Create",
        data: { productId: productId, quantity: quantity },
        success: function(data)
        {            
            effect(productId);
            setTimeout(function () {
                $(".cartItems").text(data);
            }, 1500);    
            
        }
    });
});

$('.remove-cart-item').on('click', function () {
    var cartId = $(this).data("id");
    var productId = $(this).data("id2");
    var x = 1;

    $.ajax({
        type: "POST",
        url: "https://localhost:44337/ShoppingCart/Delete",
        data: { cartId: cartId, idProducto: productId },
        success: function (data) {

            //$("#" + cartId).remove();
            location.reload();
        }
    });
});

$('.update-cart-item').on('click', function () {
    var cartId = $(this).data("id");
    var productId = $(this).data("id2");
    var idcampo = "quantityField_" + productId;
    var nuevaCantidad = $("#" + idcampo).val();
    nuevaCantidad = parseInt(nuevaCantidad);
    var x = 1;

    $.ajax({
        type: "POST",
        url: "https://localhost:44337/ShoppingCart/Edit",
        data: { cartId: cartId, quantity: nuevaCantidad, idProducto: productId },
        success: function (data) {

            //$("#" + cartId).remove();
            location.reload();
        }
    });
});



function effect(productId) {
    var cart = $('.shopping-cart');    
    var imgtodrag = $(".Image_" + productId);
    if (imgtodrag) {
        var imgclone = imgtodrag.clone()
            .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
            .css({
                'opacity': '0.5',
                'position': 'absolute',
                'height': '150px',
                'width': '150px',
                'z-index': '100'
            })
            .appendTo($('body'))
            .animate({
                'top': cart.offset().top + 10,
                'left': cart.offset().left + 10,
                'width': 75,
                'height': 75
            }, 1000, 'easeInOutExpo');

        setTimeout(function () {
            cart.effect("shake", {
                times: 2
            }, 200);
        }, 1500);

        imgclone.animate({
            'width': 0,
            'height': 0
        }, function () {
            $(this).detach()
        });
    }
}