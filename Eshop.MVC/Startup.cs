﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Eshop.MVC.Startup))]
namespace Eshop.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
