﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace Eshop.MVC.Controllers
{
    public class ShopController : Controller
    {


        IProductManager productManager = null;
        ICategoryManager categoryManager = null;


        private List<ProductModelCatalog> Products { get; set; }
        public List<CategoryModelCatalog> Categories { get; set; }

        /// <summary>
        /// Constructor vacio
        /// </summary>
        public ShopController(ICategoryManager categoryManager)
        {
            Categories = categoryManager.GetAllCategories().Select(p => new CategoryModelCatalog
            {
                Id = p.Id,
                Name = p.Name
            }).ToList();


        }

        public ShopController(IProductManager productManager, ICategoryManager categoryManager)
        {

            this.productManager = productManager;
            this.categoryManager = categoryManager;

            // Traemos las categorias de su clase enum
            //Categories = Enum.GetValues(typeof(Category)).Cast<Category>().ToList();
            //Categories = categoryManager.GetAllCategories().ToList();

            Categories = categoryManager.GetAllCategories().Select(p => new CategoryModelCatalog
            {
                Id = p.Id,
                Name = p.Name
            }).ToList();


            //recogemos listado de productos via manager
            Products = productManager.GetAll().Select(e => new ProductModelCatalog
            {
                Id = e.Id,
                Name = e.Name.ToString(),
                Description = e.Description.ToString(),
                UnitsInStock = e.UnitsInStock,
                Duration = e.Duration,
                Price = e.Price,
                Imagenes = e.Imagenes,
                YtURL = e.YtURL,
                Category = e.Category


            }).ToList();
        }

        // GET: Shop
        public ActionResult Index(int? category = null)
        {

            ShopModel model = new ShopModel
            {
                Categories = Categories,
                Products = Products.Where(p => category == null || p.Category.Id.Equals((int)category)).ToList()
            };



            return View(model);
        }



        // GET: Shop/Create
        public ActionResult Create()
        {
            return View();
        }



        /// <summary>
        /// metodo que añade al carrito
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public ActionResult AddCartItem(int productId)
        {
            var count = AddProduct(productId);
            return Content(count.ToString());
        }


        private int AddProduct(int productId, int quantity = 1)
        {
            string userId = null;
            if (User.Identity.IsAuthenticated)
            {
                userId = User.Identity.GetUserId();
            }
            else
            {
                userId = Session.SessionID;
            }

            Dictionary<int, int> cart = Session["CartItem"] as Dictionary<int, int>;
            if (cart == null)
                cart = new Dictionary<int, int>();
            if (cart.ContainsKey(productId))
                cart[productId] = cart[productId] + quantity;
            else
                cart.Add(productId, 1);

            Session["CartItem"] = cart;

            return cart.Sum(e => e.Value);
        }
    }
}
