﻿using Eshop.CORE.Contracts;
using Eshop.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Eshop.IFR.Log;
using Eshop.IFR.Paypal;
using PayPal.Api;
using Eshop.CORE;
using Eshop.IFR.Email;

namespace Eshop.MVC.Controllers
{
    public class ShoppingCartController : Controller
    {

        //Manager de CartItem
        private ICartItemManager _cartItemManager = null;
        private ILogEvent _logManager = null;
        private IPaypalManager _paypalManager = null;
        private  IProductManager _productManager = null;
        private IOrderManager _orderManager = null;
        private IMyUserManager _userManager = null;
        private IEmailManager _emailManager = null;

        public int totalValue;
        /// <summary>
        /// Constructor de cartItemController
        /// </summary>
        /// <param name="productManager"></param>
        public ShoppingCartController(ICartItemManager cartItemManager)
        {
            this._cartItemManager = cartItemManager;
        }

        public ShoppingCartController(IMyUserManager myUserManager, ICartItemManager cartItemManager, IPaypalManager paypalManager, ILogEvent log, IProductManager productManager, IOrderManager orderManager , IEmailManager emailManager)
        {
            this._cartItemManager = cartItemManager;
            this._logManager = log;
            this._paypalManager = paypalManager;
            this._productManager = productManager;
            this._orderManager = orderManager;
            this._userManager = myUserManager;
            this._emailManager = emailManager;
        }

        public List<CartItemModel> UserCartItems { get; set; }


        // GET: ShoppingCart
        public ActionResult Index()
        {
            UserCartItems = _cartItemManager.GetCartItemByUser(User.Identity.GetUserId()).Select(c => new CartItemModel
            {
                Id = c.Id,
                CartId = c.CartId,
                DateCreated = c.DateCreated,
                Product = c.Product,
                ProductId = c.ProductId,
                Quantity = c.Quantity,
                Imagen = c.Product.Imagenes.FirstOrDefault().urlImage

            }).ToList();


            return View(UserCartItems);
        }


        // GET: ShoppingCart/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ShoppingCart/Edit/5
        [HttpPost]
        public ActionResult Edit(String cartId, int quantity, int idProducto)
        {
            try
            {
                // recogemos el cartItem
                var cartItem = _cartItemManager.GetCartItemByUserAndProduct(cartId, idProducto).FirstOrDefault();
                //recogemos el producto para comprobar stock
                int stock = _productManager.GetById(idProducto).UnitsInStock;

                if (cartItem != null)
                {


                    //si  el nuevo stock es mayor que -1
                    if((stock - quantity) > -1)
                    {
                        //guardamos las propiedades que nos interesan

                        cartItem.Quantity = quantity;
                        _cartItemManager.Context.SaveChanges();
                    }
                    else
                    {
                        //Actualizamos el carrito con el maximo de stock disponible

                        cartItem.Quantity = stock;
                        _cartItemManager.Context.SaveChanges();

                    }

                }

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                _logManager.WriteError(ex.Message, ex);
                return View();
            }
        }

        // GET: ShoppingCart/Delete/5
        public ActionResult Delete(int id, String prueba)
        {
            return View();
        }

        // POST: ShoppingCart/Delete/5
        [HttpPost]
        public ActionResult Delete(String cartId, int idProducto)
        {

            try
            {
                // recogemos el objeto de carrito de parametro
                var cartItem = _cartItemManager.GetCartItemByUserAndProduct(cartId, idProducto).FirstOrDefault();
                if (cartItem != null)
                {
                                        
                    //lo eliminamos mediante el manager
                    _cartItemManager.Remove(cartItem);
                    _cartItemManager.Context.SaveChanges();
                }


                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                _logManager.WriteError(ex.Message, ex);

                return View();
            }
        }


        //pago paypal

        private Payment payment;

        //creamos un pago de paypal usando su contexto



        private Payment CreatePayment(APIContext apiContext, string redirectUrl)
        {
            var listItems = new ItemList() { items = new List<Item>() };

            List<CartItemModel> cartItemsList = _cartItemManager.GetCartItemByUser(User.Identity.GetUserId()).Select(c => new CartItemModel
            {
                Id = c.Id,
                CartId = c.CartId,
                DateCreated = c.DateCreated,
                Product = c.Product,
                ProductId = c.ProductId,
                Quantity = c.Quantity,
                Imagen = c.Product.Imagenes.FirstOrDefault().urlImage

            }).ToList();


            foreach(var cartItem in cartItemsList)
            {
                listItems.items.Add(new Item()
                {

                    name = cartItem.Product.Name,
                    currency = "EUR",
                    price = cartItem.Product.Price.ToString(),
                    quantity = cartItem.Quantity.ToString(),
                    sku = "sku"

                });
            }

            var payer = new Payer() { payment_method = "paypal" };

            //configuracion de redirectURLS con el objeto

            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl,
                return_url = redirectUrl
            };

            //creamos el objeto details
            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                subtotal = cartItemsList.Sum(x=> x.Quantity * x.Product.Price).ToString() 

            };

            //creamos objeto amount

            var amount = new Amount()
            {
                currency = "EUR",
                total = (Convert.ToDouble(details.tax) + Convert.ToDouble(details.shipping) + Convert.ToDouble(details.subtotal)).ToString(), //tax+shipping+subtotal
                details = details
            };

            //creamos transaccion
            var transactionList = new List<Transaction>();
            transactionList.Add(new Transaction()
            {
                description = "Transacción de Eshop Miguel",
                invoice_number = (Convert.ToString(new Random().Next(100000))),
                amount = amount,
                item_list = listItems
            });

            payment = new Payment()
            {
                intent = "sale",
                payer = payer, 
                transactions = transactionList,
                redirect_urls = redirUrls
            };


            return payment.Create(apiContext);

        }

        //creamos execute payment method

        private Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };
            payment = new Payment()
            {
                id = paymentId
            };

            return payment.Execute(apiContext, paymentExecution);
        }

        //metodo para crear paymentwithpaypal

        public ActionResult PaymentWithPaypal()
        {
            //recogemos contexto con el manager

            APIContext apiContext = _paypalManager.GetAPIContext();

            try
            {

                string payerId = Request.Params["PayerID"];
                if (string.IsNullOrEmpty(payerId))
                {

                    //creamos un pago

                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/ShoppingCart/PaymentWithPaypal?";
                    var guid = Convert.ToString(new Random().Next(100000));
                    var createdPayment = CreatePayment(apiContext, baseURI + "guid=" + guid);


                    //obtenemos de vuelta los links de paypal de la respuesta

                    var links = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = string.Empty;

                    while (links.MoveNext())
                    {
                        Links link = links.Current;

                        if (link.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = link.href;
                        }
                    }
                    Session.Add(guid, createdPayment.id);




                    return Redirect(paypalRedirectUrl);


                }
                else
                {
                    //esto se ejecutara cuando hayamos recibido todos los params del pago de la llamada anterior
                    var guid = Request.Params["guid"];
                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);

                    if(executedPayment.state.ToLower() != "approved")
                    {
                        return View("Failure");
                    }

                }

            }catch(Exception ex)
            {
                _logManager.WriteError(ex.Message, ex);

                return View("Failure");

            }

            

            try
            {
                //capturamos usuario 
                var orderClient = GetClient();

                //creamos pedido con los datos del cliente
                var paypalOrder = CreateOrder(orderClient);

                //lo añadimos con el manager
                _orderManager.Add(paypalOrder);
                _orderManager.Context.SaveChanges();

            }
            catch(Exception ex)
            {
                _logManager.WriteError(ex.Message, ex);
            }

            //capturamos el pedido que se acaba de crear
            var createdOrder = _orderManager.GetByUserId(User.Identity.GetUserId()).OrderByDescending(o => o.OrderDate).FirstOrDefault();

            //creamos orderdedtails del producto
            CreateOrderDetails(createdOrder);

            //si todo ha ido bien, limpiamos el carrito del usuario
            LimpiarCarrito();


            //Mandamos mail al usuario confirmando el pedido
            string orderLink = "https://localhost:44337/Client/Order/Details/" + createdOrder.Id;
            
            _emailManager.SendMyMail(createdOrder.Email, "Se ha confimado su pedido " + createdOrder.Id + " EshopM", "Se ha confimado su pedido " + createdOrder.Id + " Eshop con un importe total de: " + createdOrder.Total + " €. Para ver más detalles <a href=" + orderLink + ">Click</a>");


            //redireccionamos a los detalles del pedido pasandole que ha ido OK
            return Redirect("../Client/Order/Details/" + createdOrder.Id+"?data=OK");

            //return View("Success");

        }

        /// <summary>
        /// metodo que limpia el carrito del usuario tras la compra
        /// </summary>
        private void LimpiarCarrito()
        {
            _cartItemManager.CleanCart(User.Identity.GetUserId());
        }

        /// <summary>
        /// metodo que recibe el producto y el stock a restar
        /// </summary>
        /// <param name="product"></param>
        /// <param name="cantidadResta"></param>
        private void updateStock(Product product, int cantidadResta)
        {
            product.UnitsInStock = product.UnitsInStock - cantidadResta;

            _productManager.Context.SaveChanges();
        }

        /// <summary>
        /// Metodo que crea order details de pedido
        /// </summary>
        /// <param name="createdOrder"></param>
        private void CreateOrderDetails(CORE.Order createdOrder)
        {

            var listItems = new ItemList() { items = new List<Item>() };

            List<CartItemModel> cartItemsList = _cartItemManager.GetCartItemByUser(User.Identity.GetUserId()).Select(c => new CartItemModel
            {
                Id = c.Id,
                CartId = c.CartId,
                DateCreated = c.DateCreated,
                Product = c.Product,
                ProductId = c.ProductId,
                Quantity = c.Quantity,
                Imagen = c.Product.Imagenes.FirstOrDefault().urlImage

            }).ToList();


            foreach (var cartItem in cartItemsList)
            {
                try
                {
                    var result = _productManager.Context.Products.SingleOrDefault(p => p.Id == cartItem.ProductId);

                    createdOrder.OrderDetails.Add(new OrderDetail
                    {
                        OrderId = createdOrder.Id,
                        Product = cartItem.Product,
                        ProductId = cartItem.ProductId,
                        Quantity = cartItem.Quantity,
                        UnitPrice = result.Price,
                        Username = User.Identity.GetUserName()

                    });

                    updateStock(result, cartItem.Quantity);

                    _orderManager.Context.SaveChanges();

                }
                catch(Exception ex)
                {
                    _logManager.WriteError(ex.Message, ex);
                }


            }

            }

                    
        

        /// <summary>
        /// metodo que crea pedido con los datos del usuario actual
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private CORE.Order CreateOrder(ApplicationUser user)
        {
            //obtenemos el coste del pedido

            CORE.Order newOrder = new CORE.Order()
            {
                Address = user.Address,
                City = user.City,
                Country = user.Country,
                Email = user.Email,
                FirstName = user.FirstName,
                HasBeenShipped = false,
                LastName = user.LastName,
                OrderDate = DateTime.Now,
                PaymentTransactionId = payment.id,
                Phone = user.PhoneNumber,
                PostalCode = user.PostalCode,
                State = user.State,
                Status = OrderStatus.Nuevo,
                Total = _cartItemManager.GetCartAmount(user.Id),
                User = user,
                User_Id = user.Id
                

        };


            return newOrder;
        }


        
        /// <summary>
        /// metodo que trae el cliente de la compra
        /// </summary>
        /// <returns></returns>
        private ApplicationUser GetClient()
        {

            var user = _userManager.GetUsersByEmail(User.Identity.GetUserName()).FirstOrDefault();

            return user;

        }

         
    }
}
