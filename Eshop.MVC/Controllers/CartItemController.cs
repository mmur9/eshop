﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.IFR.Log;
using Eshop.MVC.Models;
using Microsoft.AspNet.Identity;

namespace Eshop.MVC.Controllers
{


    public class CartItemController : Controller

    {

        //Manager de cartItem

        private ICartItemManager _cartItemManager = null;
        private ILogEvent _logManager = null;

        /// <summary>
        /// Constructor de cartItemController
        /// </summary>
        /// <param name="productManager"></param>
        public CartItemController(ICartItemManager cartItemManager, ILogEvent logManager)
        {
            this._cartItemManager = cartItemManager;
            this._logManager = logManager;
        }

        // GET: CartItem
        public ActionResult Index()
        {
            return View();
        }

        // GET: CartItem/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CartItem/Create
        public ActionResult Create()
        {

            return View();

        }

        // POST: CartItem/Create
        [HttpPost]
        public ActionResult Create(int productId, int quantity = 1)
        {

            String cartId = null;
            var count = 0;

            try
            {

                //cogemos id del usuario
                // si esta atenticado guardamos su id
                if (User.Identity.IsAuthenticated)
                {

                    //si el producto ya esta en su carrito que actualice la cantidad

                    try
                    {

                        var cartItem = _cartItemManager.GetCartItemByUserAndProduct(User.Identity.GetUserId().ToString(), productId).SingleOrDefault();

                        //sino que lo cree nuevo

                        if (cartItem == null)
                        {
                            cartItem = new CartItem
                            {
                                Id = Guid.NewGuid().ToString(),
                                CartId = User.Identity.GetUserId(),
                                DateCreated = DateTime.Now,
                                Quantity = quantity,
                                ProductId = productId,
                                Product = _cartItemManager.Context.Products.SingleOrDefault(p => p.Id == productId)


                            };

                            //añadimos el producto

                            if (cartItem.Product.UnitsInStock > 0)
                            {
                                _cartItemManager.Add(cartItem);
                            }
                            else
                            {
                                //no es necesario, si no hay stock no aparece el botón
                                Boolean stock = false;
                            }
                        }
                        //si el producto ya esta en el carrito, se incrementa la cantidad
                        else
                        {
                            //si hay stock que incremente cantidad
                            if (cartItem.Product.UnitsInStock > 0)
                            {
                                cartItem.Quantity++;

                            }
                            //si no hay stock, error
                            else
                            {
                                //TODO: NO HAY STOCK
                            }
                        }

                        //guardamos cambios del contexto de datos
                        _cartItemManager.Context.SaveChanges();

                        count = GetItemsCount();

                    }
                    catch(Exception ex)
                    {
                        //no se ha podido añadir al carrito
                        _logManager.WriteError(ex.Message, ex);
                        

                    }




                }
                //si no esta autenticado lo guardaremos en la sesion //TODO: sesion cartItem
                else
                {
                    CartItem cartItem = new CartItem
                    {
                        Id = Guid.NewGuid().ToString(),
                        CartId = Session.SessionID.ToString(),
                        DateCreated = DateTime.Now,
                        Quantity = quantity,
                        ProductId = productId,
                        Product = _cartItemManager.Context.Products.SingleOrDefault(p => p.Id == productId)

                        //Product = applicationDbContext.Products.SingleOrDefault(p => p.Id == prueba.ProductId)

                    };

                    _cartItemManager.Add(cartItem);
                    _cartItemManager.Context.SaveChanges();
                }


                //return RedirectToAction("Administrator/Product/Index");
                return Content(count.ToString());


            }
            catch(Exception e)
            {

                _logManager.WriteError(e.Message, e);
                return View();

            }
        }

        /// <summary>
        /// Metodo que añade al carrito un producto
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public ActionResult AddCartItem(int productId)
        {
            var count = AddProduct(productId);
            return Content(count.ToString());
        }

        /// <summary>
        /// Metodo que guarda un producto en la sesion
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        private int AddProduct(int productId, int quantity = 1)
        {
            string userId = null;
            if (User.Identity.IsAuthenticated)
            {
                userId = User.Identity.GetUserId();
            }
            else
            {
                userId = Session.SessionID;
            }

            Dictionary<int, int> cart = Session["CartItem"] as Dictionary<int, int>;
            if (cart == null)
                cart = new Dictionary<int, int>();
            if (cart.ContainsKey(productId))
                cart[productId] = cart[productId] + quantity;
            else
                cart.Add(productId, 1);

            Session["CartItem"] = cart;

            return cart.Sum(e => e.Value);
        }

        // GET: CartItem/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CartItem/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: CartItem/Delete/5
        public ActionResult Delete(int id)
        {


            return View();
        }

        // POST: CartItem/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, CartItemModel model)
        {
            try
            {
                // TODO: Add delete logic here
                var cartItem = _cartItemManager.GetById(id);
                if (cartItem != null)
                {

                    _cartItemManager.Remove(cartItem);
                    _cartItemManager.Context.SaveChanges();
                }


                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                //si no se elimina guardamos en log
                _logManager.WriteError(ex.Message, ex);
                return View();
            }
        }


        /// <summary>
        /// metodo que obtiene el nº de articulos en carrito
        /// </summary>
        /// <returns></returns>
        public int GetItemsCount()
        {

            String ShoppingCartId = User.Identity.GetUserId().ToString();

            int? count = (from cartItems in _cartItemManager.Context.ShoppingCartItems
                          where cartItems.CartId == ShoppingCartId
                          select (int?)cartItems.Quantity).Sum();

            //Devuelve 0 si todas son null
            return count ?? 0;
            

        }

    }
}
