﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.DAL;
using Eshop.IFR.Log;
using Eshop.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eshop.MVC.Controllers
{
    public class ProductController : Controller
    {
        private IProductManager _productManager = null;
        private ILogEvent _logManager = null;


        public ProductController(IProductManager productManager, ILogEvent logManager)
        {

            this._productManager = productManager;
            this._logManager = logManager;
        }
        // GET: Product
        public ActionResult Index()
        {


            var model = _productManager.GetAll().Select(e => new ProductModelCatalog
            {
                Id = e.Id,
                Name = e.Name.ToString(),
                Description = e.Description.ToString(),
                UnitsInStock = e.UnitsInStock,
                Duration = e.Duration,
                Price = e.Price,
                Imagenes = e.Imagenes,
                YtURL = e.YtURL,
                Category = e.Category


            });

            //Categories = Enum.GetValues(typeof(Category)).Cast<Category>().ToList(); 
            //Products = model.ToList();

            return View(model);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {

            try
            {

                var model = _productManager.GetAll().Where(e => e.Id == id).Select(e => new ProductModelCatalog
                {
                    Id = e.Id,
                    Name = e.Name.ToString(),
                    Description = e.Description.ToString(),
                    UnitsInStock = e.UnitsInStock,
                    Duration = e.Duration,
                    Price = e.Price,
                    Imagenes = e.Imagenes,
                    YtURL = e.YtURL,
                    Category = e.Category
                }).FirstOrDefault();

                //si el modelo existe pasamos la vista, sino redirigimos a la tienda
                if (model != null)
                {
                    return View(model);

                }
                else
                {
                    return RedirectToAction("../Shop");

                }



            }
            catch (Exception e)
            {

                _logManager.WriteError(e.Message, e);

                return RedirectToAction("Index");
            }

        }


    }
}
