﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eshop.MVC.Models
{
    /// <summary>
    /// Clase para el listado de pedidos
    /// </summary>
    public class OrderList
    {
        /// <summary>
        /// Identificador del pedido
        /// </summary>
        [Display(Name = "ID PEDIDO" )]
        public int Id { get; set; }
        /// <summary>
        /// Nombre del usuario
        /// </summary>
        public String FirstName { get; set; }
        /// <summary>
        /// Apellido
        /// </summary>
        public String LastName { get; set; }
        /// <summary>
        /// direccion de envio
        /// </summary>
        public String Address { get; set; }
        /// <summary>
        /// email del usuario
        /// </summary>
        public String Email { get; set; }
        /// <summary>
        /// importe total
        /// </summary>
        public Decimal Total { get; set; }
        /// <summary>
        /// id del usuario
        /// </summary>
        public String User_Id { get; set; }







    }
}