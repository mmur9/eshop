﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eshop.MVC.Models
{
    public class ImageViewModel
    {

        /// <summary>
        /// Id de la imagen
        /// </summary>
        public int Id { get; set; }


        ///<summary>
        /// Url de la imagen
        ///</summary>
        public String urlImage { get; set; }

        /// <summary>
        /// Id del producto
        /// </summary>
        public int idProducto { get; set; }
    }
}