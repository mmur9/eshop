﻿using Eshop.CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Eshop.MVC.Models
{
    public class OrderAdminViewModel
    {

        public int Id { get; set; }

        [DisplayName("Fecha")]

        public System.DateTime OrderDate { get; set; }


        [DisplayName("Nombre")]
        public string FirstName { get; set; }


        [DisplayName("Apellido")]
        public string LastName { get; set; }


        [DisplayName("Dirección")]

        public string Address { get; set; }

        [DisplayName("Ciudad")]
        public string City { get; set; }

        [DisplayName("Estado")]
        public string State { get; set; }

        [DisplayName("CP")]

        public string PostalCode { get; set; }

        [DisplayName("País")]

        public string Country { get; set; }

        [DisplayName("Teléfono")]

        public string Phone { get; set; }

        [DisplayName("Email")]

        public string Email { get; set; }

        [DisplayName("Importe")]

        public decimal Total { get; set; }

        [DisplayName("ID Pago")]
        public string PaymentTransactionId { get; set; }

        [DisplayName("Enviado")]
        public bool HasBeenShipped { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }


        /// <summary>
        /// Usuario del pedido
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Identificador del usuario que ha realizado el pedido
        /// </summary>
        [ForeignKey("User")]
        public string User_Id { get; set; }



        /// <summary>
        /// Estado del pedido
        /// </summary>
        [DisplayName("Estado")]
        public OrderStatus Status { get; set; }

    }
}
