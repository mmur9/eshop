﻿using Eshop.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eshop.MVC.Models
{
    public class ProductModel
    {

        /// <summary>
        /// Id del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// Duración - ? es nullable
        /// </summary>
        public int? Duration { get; set; }

        /// <summary>
        /// Precio por unidad
        /// </summary>

        public decimal Price { get; set; }

        /// <summary>
        /// Listado de imagenes de producto
        /// </summary>
        public Category Category { get; set; }



    }
}
