﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Eshop.MVC.Models
{
    public class RoleViewModel
    {
        /// <summary>
        /// Identificador del rol
        /// </summary>
        public String Id { get; set; }

        /// <summary>
        /// Nombre del rol
        /// </summary>
        public String Nombre { get; set; }

        /// <summary>
        /// Cuenta de usuarios en el rol
        /// </summary>
        /// 
        [DisplayName("Nº Usuarios")]
        public int nUsuarios { get; set; }

    }
}