﻿using Eshop.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eshop.MVC.Models
{
    public class ShopModel
    {

        public List<CategoryModelCatalog> Categories { get; set; }
        public List<ProductModelCatalog> Products { get; set; }
    }
}