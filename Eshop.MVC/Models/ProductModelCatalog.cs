﻿using Eshop.CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Eshop.MVC.Models
{
    /// <summary>
    /// Clase para el listado de productos
    /// </summary>
    public class ProductModelCatalog
    {
        
        /// <summary>
        /// Id del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        [DisplayName("Título")]
        public string Name { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Unidades en Stock
        /// </summary>
        [DisplayName("Stock")]

        public int UnitsInStock { get; set; }

        /// <summary>
        /// Duración - ? es nullable
        /// </summary>
        public int? Duration { get; set; }

        /// <summary>
        /// Precio por unidad
        /// </summary>

        public decimal Price { get; set; }

        /// <summary>
        /// Un producto puede tener muchas imagenes
        /// </summary>

        public List<Image> Imagenes { get; set; }


        /// <summary>
        /// Url trailer youtube pelicula
        /// </summary>
        [DisplayName("Trailer")]

        public string YtURL { get; set; }

        /// <summary>
        /// Categoria producto
        /// </summary>
        [DisplayName("Categoría")]

        public Category Category { get; set; }
    }




}
