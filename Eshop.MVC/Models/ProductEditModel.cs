﻿using Eshop.CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eshop.MVC.Models
{
    /// <summary>
    /// Modelo para editar producto con dropdown de valores categoria
    /// </summary>
    public class ProductEditModel
    {
        /// <summary>
        /// Id del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        [DisplayName("Título")]
        public string Name { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Unidades en Stock
        /// </summary>
        [DisplayName("Stock")]

        public int UnitsInStock { get; set; }

        /// <summary>
        /// Duración - ? es nullable
        /// </summary>
        public int? Duration { get; set; }

        /// <summary>
        /// Precio por unidad
        /// </summary>

        public decimal Price { get; set; }


        /// <summary>
        /// Url trailer youtube pelicula
        /// </summary>
        [DisplayName("Trailer")]

        public string YtURL { get; set; }

        /// <summary>
        /// Categoria producto
        /// </summary>
        [DisplayName("Categoría")]

        public Category Category { get; set; }

        public IEnumerable<SelectListItem> Categorias { get; set; }

        /// <summary>
        /// id de categoria seleccionada para guardar
        /// </summary>
        public int SelectedCategory { get; set; }

        /// <summary>
        /// Listado de imagenes de producto
        /// </summary>
        public List<Image> Imagenes { get; set; }



    }
}