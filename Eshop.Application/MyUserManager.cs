﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.Application
{
    public class MyUserManager : GenericManager<ApplicationUser>, IMyUserManager
    {
        /// <summary>
        /// Constructor del manager de producto
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public MyUserManager(IApplicationDbContext context) : base(context)
        {

        }



        //public Product GetAll()
        //{
        //    return Context.Set<Product>().Include("TotalValue").Where(i => i.Id > 0).SingleOrDefault();
        //}

        /// <summary>
        /// Metodo que saca todos los usuarios que contengan una @
        /// </summary>
        /// <returns>objeto usuario</returns>
        public IQueryable<ApplicationUser> GetUsers()
        {
            return Context.Set<ApplicationUser>().Where(e => e.Email.Contains("@"));
        }

        /// <summary>
        /// Metodo que devuelve los / el usuario que contenga el mail pasado por parametro
        /// </summary>
        /// <param name="email">cadena mail que se busca</param>
        /// <returns>Objeto usuario con el mail correspondiente</returns>
        public IQueryable<ApplicationUser> GetUsersByEmail(string email)
        {
            return Context.Set<ApplicationUser>().Where(e => e.Email.Equals(email));
        }

        public IQueryable<ApplicationUser> GetUsersById(string id)
        {
            return Context.Set<ApplicationUser>().Where(e => e.Id.Equals(id));
        }

    }
}
