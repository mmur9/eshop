﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.Application
{
    /// <summary>
    /// Manager de producto
    /// </summary>
    public class ImageManager : GenericManager<Image>, IImageManager
    {
        /// <summary>
        /// Constructor del manager de producto
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public ImageManager(ApplicationDbContext context) : base(context)
        {

            //    public override void Method1()
            //{
            //    Console.WriteLine("Derived - Method1");
            //}

        }


        //public IQueryable<Image> GetDataImageById(int Id)
        //{
        //    return Context.Set<Image>().Where(e => e.Id == Id);
        //}

        public IQueryable<Image> GetDataImageByProductId(int Id)
        {
            return Context.Set<Image>().Where(e => e.Product.Id == Id);
        }




    }
}
