﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.Application
{
    /// <summary>
    /// Clase manager de pedido
    /// </summary>
    public class OrderManager : GenericManager<Order>, IOrderManager
    {
        /// <summary>
        /// Constructor de la clase manager de pedido
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public OrderManager(IApplicationDbContext context) : base(context)
        {

        }
        /// <summary>
        /// Metodo que retorna los pedidos de un usuario
        /// </summary>
        /// <param name="userId">Identificador del usuario</param>
        /// <returns>Todos los pedidos de un usuario</returns>
        public IQueryable<Order> GetByUserId(string userId)
        {
            return Context.Set<Order>().Include("OrderDetails").Where(e => e.User_Id == userId);
        }


        public IQueryable<Order> GetByUsername(string userName)
        {
            return Context.Set<Order>().Include("OrderDetails").Where(e => e.Email == userName);
        }
        /// <summary>
        /// Obtiene un pedido con su totalvalue
        /// </summary>
        /// <param name="id">identificador del pedido</param>
        /// <returns>Pedido con su total value si existe o null en caso de no existir</returns>
        //public Order GetByIdAndTotalValue(int id)
        //{
        //    return Context.Set<Order>().Include("TotalValue").Where(i => i.Id == id).SingleOrDefault();
        //}

        /// <summary>
        /// obtenemos pedido por id pero ademas obtenemos orderDetails
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Order GetByIdIncOrderDet(int id)
        {
            return Context.Set<Order>().Include("OrderDetails").Where(i => i.Id == id).SingleOrDefault();
        }


    }
}
