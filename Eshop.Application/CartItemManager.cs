﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.Application
{
    /// <summary>
    /// Manager de elementos en carrito
    /// </summary>
    class CartItemManager : GenericManager<CartItem>, ICartItemManager
    {
        /// <summary>
        /// Constructor del manager de cartItem
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public CartItemManager(IApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Consulta que nos devuelve los objetos de carrito de un usuario y un producto concreto, para por ejemplo ver si un objeto ya esta en un carrito
        /// </summary>
        /// <param name="userCart"></param>
        /// <param name="productCart"></param>
        /// <returns></returns>
        public IQueryable<CartItem> GetCartItemByUserAndProduct(String userCart, int productCart)
        {
            return Context.Set<CartItem>().Where(e => e.CartId.Equals(userCart) && e.ProductId == productCart);
        }

        /// <summary>
        /// Consulta que nos devuelve los objetos de carrito de un usuario
        /// </summary>
        /// <param name="userCart"></param>
        /// <param name="productCart"></param>
        /// <returns></returns>
        public IQueryable<CartItem> GetCartItemByUser(String userCart)
        {
            return Context.Set<CartItem>().Where(e => e.CartId.Equals(userCart));
        }

        public IQueryable<CartItem> GetCartItemById(String idCart)
        {
            return Context.Set<CartItem>().Where(e => e.Id.Equals(idCart));
        }

        /// <summary>
        /// metodo que nos devuelve la suma del precio del carrito de un usuario
        /// </summary>
        /// <param name="userCart"></param>
        /// <returns></returns>
        public int GetCartAmount(String userCart)
        {
            var suma = Context.Set<CartItem>().Where(e => e.CartId.Equals(userCart)).Sum(x => x.Quantity * x.Product.Price).ToString();

            int total = Int32.Parse(suma);

            return total;
        }

        /// <summary>
        /// metodo que elimina todos los cartItems del carrito
        /// </summary>
        /// <param name="userCart"></param>
        public void CleanCart(string userCart)
        {
            List<CartItem> cartItemsList = Context.Set<CartItem>().Where(e => e.CartId.Equals(userCart)).ToList();

            foreach (var cartItem in cartItemsList)
            {
                Context.Set<CartItem>().Remove(cartItem);
            }

            Context.SaveChanges();
        }

    }


}

