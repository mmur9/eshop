﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.Application
{/// <summary>
/// Manager de categoria
/// </summary>
    public class CategoryManager : GenericManager<Category>, ICategoryManager
    {

        /// <summary>
        /// constructor del manager de categoria
        /// </summary>
        /// <param name="context"></param>
        public CategoryManager(IApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// metodo de prueba
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IQueryable<Category> GetCategoriesTest(int id)
        {
            return Context.Set<Category>().Where(e => e.Id == id);
        }

        public IQueryable<Category> GetAllCategories()
        {
            return Context.Set<Category>();
        }
    }





}
