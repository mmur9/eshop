﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.Application
{
    /// <summary>
    /// Manager de producto
    /// </summary>
    public class ProductManager : GenericManager<Product>, IProductManager
    {
        /// <summary>
        /// Constructor del manager de producto
        /// </summary>
        /// <param name="context">contexto de datos</param>
        public ProductManager(IApplicationDbContext context) : base(context)
        {

        }

        //public Product GetAll()
        //{
        //    return Context.Set<Product>().Include("TotalValue").Where(i => i.Id > 0).SingleOrDefault();
        //}

        public IQueryable<Product> GetProducts2()
        {
            return Context.Set<Product>().Where(e => e.Id > 0);
        }

        public IQueryable<Product> GetProductByIdWithImages(int id)
        {
            return Context.Set<Product>().Include("Category").Include("Imagenes").Where(e => e.Id == id);
        }


    }
}
