﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        /// <summary>
        /// coleccion persistible de pedidos
        /// </summary>
        public DbSet<Order> Orders { get; set; }
        /// <summary>
        /// coleccion persistible de detalles de pedidos
        /// </summary>
        public DbSet<OrderDetail> OrderDetails { get; set; }

        /// <summary>
        /// coleccion persistible de productos
        /// </summary>
        public DbSet<Product> Products { get; set; }

        public DbSet<Image> Images { get; set; }

        public DbSet<CartItem> ShoppingCartItems { get; set; }

        public DbSet<Category> Categories { get; set; }






        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{

        //    modelBuilder.Entity<Image>().HasRequired(x => x.Product);

        //    base.OnModelCreating(modelBuilder);
        //}


    }
}

