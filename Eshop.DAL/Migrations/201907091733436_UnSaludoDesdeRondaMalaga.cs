namespace Eshop.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnSaludoDesdeRondaMalaga : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageTitle = c.String(),
                        ImageData_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Images", t => t.ImageData_Id)
                .Index(t => t.ImageData_Id);
            
            CreateTable(
                "dbo.ProductImages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Images", t => t.ImageId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ImageId)
                .Index(t => t.ProductId);
            
            DropColumn("dbo.Products", "Image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "Image", c => c.String());
            DropForeignKey("dbo.ProductImages", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductImages", "ImageId", "dbo.Images");
            DropForeignKey("dbo.Images", "ImageData_Id", "dbo.Images");
            DropIndex("dbo.ProductImages", new[] { "ProductId" });
            DropIndex("dbo.ProductImages", new[] { "ImageId" });
            DropIndex("dbo.Images", new[] { "ImageData_Id" });
            DropTable("dbo.ProductImages");
            DropTable("dbo.Images");
        }
    }
}
