namespace Eshop.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddYTurl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "YtURL", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "YtURL");
        }
    }
}
