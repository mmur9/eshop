namespace Eshop.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeCategoryStructure : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "ProductCategory", "dbo.Categories");
            DropIndex("dbo.Products", new[] { "ProductCategory" });
            DropTable("dbo.Categories");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Products", "ProductCategory");
            AddForeignKey("dbo.Products", "ProductCategory", "dbo.Categories", "Id", cascadeDelete: true);
        }
    }
}
