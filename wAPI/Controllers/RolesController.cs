﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace wAPI.Controllers
{

    [EnableCors("*", "*", "*")]
    public class RolesController : ApiController
    {
        
        [HttpGet]
        public IEnumerable<IdentityRole> Get()
        {
            var roleStore = new RoleStore<IdentityRole>();
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            var roles = roleMngr.Roles.ToList();

            return roles;
        }
    }
}
