﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.IFR.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace wAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ProductController : ApiController
    {
        IProductManager _productManager = null;
        ILogEvent _logManager = null;


        /// <summary>
        /// constructor con product manager y log
        /// </summary>
        /// <param name="productManager"></param>
        /// <param name="logManager"></param>
        public ProductController(IProductManager productManager, ILogEvent logManager)
        {

            this._productManager = productManager;
            this._logManager = logManager;
        }

        /// <summary>
        /// permite consultar los productos
        /// </summary>
        /// <param name="productManager"></param>
        /// <param name="logManager"></param>
        /// 
        [Authorize]
        [HttpGet]
        public IEnumerable<Product> Get()
        {
            //TODO: esto antes era GetProducts2
            var listado = _productManager.GetAll().ToList();
            return listado;
        }

        /// <summary>
        /// Metodo para obtener un producto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var myProduct = _productManager.GetProductByIdWithImages(id).FirstOrDefault();

            if (myProduct != null)
            {

                return Request.CreateResponse(HttpStatusCode.OK, myProduct);

            }
            else
            {
                var message = string.Format("El producto {0} no existe", id);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }

        }

        /// <summary>
        /// con la etiqueta frombody indicamos que el parametro vendrá del body
        /// </summary>
        /// <param name="product"></param>
        public HttpResponseMessage Post([FromBody] Product product)
        {
            try
            {
                _productManager.Add(product);
                _productManager.Context.SaveChanges();

                var message = Request.CreateResponse(HttpStatusCode.Created, product);
                message.Headers.Location = new Uri(Request.RequestUri + product.Id.ToString());
                return message;


            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }




        }

        /// <summary>
        /// metodo que elimina un producto pasandole el id
        /// </summary>
        /// <param name="id"></param>
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var productToDelete = _productManager.GetById(id);
                if (productToDelete == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "El producto " + id + " no existe");
                }
                else
                {
                    
                    _productManager.Remove(productToDelete);
                    _productManager.Context.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK);

                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }


        /// <summary>
        /// metodo para actualizar un producto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newProduct"></param>
        /// <returns></returns>
        public HttpResponseMessage Put(int id, [FromBody] Product newProduct)
        {

            try
            {
                if (newProduct != null)
                {
                    var productToEdit = _productManager.GetById(id);

                    productToEdit.Description = newProduct.Description;
                    productToEdit.Duration = newProduct.Duration;
                    productToEdit.Name = newProduct.Name;
                    productToEdit.Price = newProduct.Price;
                    productToEdit.UnitsInStock = newProduct.UnitsInStock;
                    productToEdit.YtURL = newProduct.YtURL;

                    _productManager.Context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, productToEdit);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "el producto " + id + " no existe");
                }
            }
            catch(Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            }

        }

    }
}
