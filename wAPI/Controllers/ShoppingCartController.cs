﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.IFR.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using wAPI.Models;

namespace wAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ShoppingCartController : ApiController
    {
        private ICartItemManager _cartItemManager = null;
        private ILogEvent _log = null;
        private IProductManager _productManager = null;
        private IOrderManager _orderManager = null;
        private IMyUserManager _userManager = null;


        public ShoppingCartController(IMyUserManager myUserManager, ICartItemManager cartItemManager, ILogEvent log, IProductManager productManager, IOrderManager orderManager)
        {
            this._cartItemManager = cartItemManager;
            this._log = log;
            this._productManager = productManager;
            this._orderManager = orderManager;
            this._userManager = myUserManager;
        }

        public List<CartItemModel> UserCartItems { get; set; }



        [HttpGet]
        [Authorize]
        public HttpResponseMessage Get()
        {



            if (User.Identity.IsAuthenticated)
            {
                var username = User.Identity.Name;
                var usuario = _userManager.GetUsersByEmail(username).FirstOrDefault();

                var userCartItems = _cartItemManager.GetCartItemByUser(usuario.Id).ToList();


                if (userCartItems != null)
                {


                    return Request.CreateResponse(HttpStatusCode.OK, userCartItems);

                }
                else
                {
                    var message = string.Format("El usuario {0} no tiene elementos en el carrito", username);
                    HttpError err = new HttpError(message);
                    return Request.CreateResponse(HttpStatusCode.NotFound, err);
                }
            }
            else
            {
                var message = string.Format("No tiene permisos para ver esto");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, message);


            }









        }


        /// <summary>
        /// metodo para actualizar un producto
        /// </summary>
        /// <param name="idProducto"></param>
        /// <returns></returns>
        public HttpResponseMessage Post([FromBody] AddToCartModel productoCarrito)
        {
            int idProducto = productoCarrito.productId;
            try
            {

                //int idProducto = Int32.Parse(productId);

                //si el producto ya esta en el carrito, que simplemente actualice la cantidad

                var cartItem = _cartItemManager.GetCartItemByUserAndProduct(User.Identity.Name, idProducto).SingleOrDefault();
                var user = _userManager.GetUsersByEmail(User.Identity.Name).FirstOrDefault();


                if (cartItem == null)
                {
                    cartItem = new CartItem
                    {
                        Id = Guid.NewGuid().ToString(),
                        CartId = user.Id,
                        DateCreated = DateTime.Now,
                        Quantity = 1,
                        ProductId = idProducto,
                        Product = _cartItemManager.Context.Products.SingleOrDefault(p => p.Id == idProducto)



                    };

                    //añadimos el producto

                    if (cartItem.Product.UnitsInStock > 0)
                    {
                        _cartItemManager.Add(cartItem);

                    }
                    else
                    {
                        //no es necesario, si no hay stock no aparece el botón
                        Boolean stock = false;

                    }
                }
                //si el producto ya esta en el carrito, se incrementa la cantidad
                else
                {
                    //si hay stock que incremente cantidad
                    if (cartItem.Product.UnitsInStock > 0)
                    {
                        cartItem.Quantity++;

                    }
                    //si no hay stock, error
                    else
                    {
                        //TODO: NO HAY STOCK
                    }
                }

                //guardamos cambios del contexto de datos


                _cartItemManager.Context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, cartItem);



            }
            catch (Exception ex)
            {
                var message = string.Format("No se ha podido anadir al carrito");
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);

            }
        }

        /// <summary>
        /// metodo para eliminar un elemento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Delete(string id)
        {
            try
            {
                var cartItemToDelete = _cartItemManager.GetCartItemById(id).FirstOrDefault();
                if (cartItemToDelete == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "El cart item " + id + " no existe");
                }
                else
                {
                    _cartItemManager.Remove(cartItemToDelete);
                    _cartItemManager.Context.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK);

                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }


    }


}
