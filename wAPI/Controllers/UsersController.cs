﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace wAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class UsersController : ApiController
    {
        IMyUserManager _userManager = null;


        public UsersController(IMyUserManager userManager)
        {
            this._userManager = userManager;
        }


        /// <summary>
        /// metodo que devuelve listado de usuarios
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public IEnumerable<ApplicationUser> Get()
        {
            //TODO: esto antes era GetProducts2
            var listado = _userManager.GetUsers().ToList();
            return listado;
        }


        /// <summary>
        /// metodo que devuelve info de un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public HttpResponseMessage Get(String email)
        {
            var myUser = _userManager.GetUsersByEmail(email).FirstOrDefault();
            

            if (myUser != null)
            {

                return Request.CreateResponse(HttpStatusCode.OK, myUser);

            }
            else
            {
                var message = string.Format("El producto {0} no existe", email);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }

        }


    }
}
