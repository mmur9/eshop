﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using Eshop.IFR.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace wAPI.Controllers
{

    [EnableCors("*", "*", "*")]
    public class OrderController : ApiController
    {

        IOrderManager _orderManager = null;
        ILogEvent _logManager = null;


        /// <summary>
        /// constructor con order manager y log
        /// </summary>
        /// <param name="orderManager"></param>
        /// <param name="logManager"></param>
        public OrderController(IOrderManager orderManager, ILogEvent logManager)
        {

            this._orderManager = orderManager;
            this._logManager = logManager;
        }

        /// <summary>
        /// permite consultar los orders de un usuario
        /// </summary>
        /// <param name="orderManager"></param>
        /// <param name="logManager"></param>
        /// 
        [HttpGet] 
        [Authorize]
        public IEnumerable<Order> Get()
        {
            //si no esta logueado que deje en blanco y pille todos
            String username = "";

            if (User.Identity.IsAuthenticated)
            {

                username = User.Identity.Name; 
            }

            var listado = _orderManager.GetByUsername(username).ToList();
            return listado;
        }


        [HttpGet]
        [Authorize]
        public HttpResponseMessage Get(int id)
        {
            var myOrder = _orderManager.GetByIdIncOrderDet(id);

            if(myOrder != null)
            {
                if (myOrder.Email.Equals(User.Identity.Name))
                {

                    return Request.CreateResponse(HttpStatusCode.OK, myOrder);

                }
                else
                {

                    var message = string.Format("El pedido {0} no le pertenece", id);
                    HttpError err = new HttpError(message);
                    return Request.CreateResponse(HttpStatusCode.NotFound, err);
                }
            }
            else
            {
                var message = string.Format("El pedido {0} no existe", id);
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }



        



        }
    }
}
