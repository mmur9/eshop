﻿using Eshop.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wAPI.Models
{
    public class CartItemModel
    {
        public string Id { get; set; }

        public string CartId { get; set; }

        public int Quantity { get; set; }

        public System.DateTime DateCreated { get; set; }

        public int ProductId { get; set; }

        public virtual Product Product { get; set; }

        public String Imagen { get; set; }
    }
}