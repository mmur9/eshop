using System;

using Unity;
using Unity.Lifetime;

namespace wAPI
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();

           // container.RegisterType(Type.GetType("Eshop.CORE.Contracts.IApplicationDbContext, Eshop.CORE"),
           //Type.GetType("Eshop.DAL.ApplicationDbContext, Eshop.DAL"),
           //new HierarchicalLifetimeManager());


           // container.RegisterType(Type.GetType("Eshop.CORE.Contracts.IProductManager, Eshop.CORE"),
           //                        Type.GetType("Eshop.Application.ProductManager, Eshop.Application"));

           // container.RegisterType(Type.GetType("Eshop.CORE.Contracts.ICategoryManager, Eshop.CORE"),
           //                        Type.GetType("Eshop.Application.CategoryManager, Eshop.Application"));

           // container.RegisterType(Type.GetType("Eshop.CORE.Contracts.IImageManager, Eshop.CORE"),
           //                        Type.GetType("Eshop.Application.ImageManager, Eshop.Application"));

           // container.RegisterType(Type.GetType("Eshop.CORE.Contracts.ICartItemManager, Eshop.CORE"),
           //                        Type.GetType("Eshop.Application.CartItemManager, Eshop.Application"));

           // container.RegisterType(Type.GetType("Eshop.CORE.Contracts.IMyUserManager, Eshop.CORE"),
           //                        Type.GetType("Eshop.Application.MyUserManager, Eshop.Application"));

           // container.RegisterType(Type.GetType("Eshop.CORE.Contracts.IOrderManager, Eshop.CORE"),
           //                        Type.GetType("Eshop.Application.OrderManager, Eshop.Application"));

           // container.RegisterType(Type.GetType("Eshop.CORE.Contracts.IApplicationUser, Eshop.CORE"),
           //                        Type.GetType("Eshop.CORE.ApplicationUser, Eshop.CORE"));

           // //no hace falta crear tipo ya que esta en el ensamblado y tenemos la referencia
           // container.RegisterType(Type.GetType("Eshop.IFR.Log.ILogEvent, Eshop.IFR"),
           //            Type.GetType("Eshop.IFR.Log.Log4NetManager, Eshop.IFR"));

           // container.RegisterType(Type.GetType("Eshop.IFR.Email.IEmailManager, Eshop.IFR"),
           //             Type.GetType("Eshop.IFR.Email.EmailManager, Eshop.IFR"));

           // container.RegisterType(Type.GetType("Eshop.IFR.Paypal.IPaypalManager, Eshop.IFR"),
           //             Type.GetType("Eshop.IFR.Paypal.PaypalManager, Eshop.IFR"));



        }
    }
}