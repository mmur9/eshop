using System.Web.Http;

using Unity.AspNet.WebApi;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(wAPI.UnityWebApiActivator), nameof(wAPI.UnityWebApiActivator.Start))]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(wAPI.UnityWebApiActivator), nameof(wAPI.UnityWebApiActivator.Shutdown))]

namespace wAPI
{
    /// <summary>
    /// Provides the bootstrapping for integrating Unity with WebApi when it is hosted in ASP.NET.
    /// </summary>
    public static class UnityWebApiActivator
    {
        /// <summary>
        /// Integrates Unity when the application starts.
        /// </summary>
        //public static void Start() 
        //{
        //    // Use UnityHierarchicalDependencyResolver if you want to use
        //    // a new child container for each IHttpController resolution.
        //    // var resolver = new UnityHierarchicalDependencyResolver(UnityConfig.Container);

        //    //var resolver = new UnityDependencyResolver(UnityConfig.Container);
        //    var resolver = new UnityHierarchicalDependencyResolver(UnityConfig.Container);

        //    GlobalConfiguration.Configuration.DependencyResolver = resolver;
        //}
        public static void Start()
        {
            // Use UnityHierarchicalDependencyResolver if you want to use a new child container for each IHttpController resolution.
            //var resolver = new UnityHierarchicalDependencyResolver(UnityConfig.GetConfiguredContainer());
            var resolver = new UnityDependencyResolver(Eshop.IFR.IoC.UnityConfig.Container);

            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
        /// <summary>
        /// Disposes the Unity container when the application is shut down.
        /// </summary>
        public static void Shutdown()
        {
            UnityConfig.Container.Dispose();
        }
    }
}