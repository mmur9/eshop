﻿namespace Eshop.CORE
{
    /// <summary>
    /// Entidad de dominio de categoria
    /// </summary>
    //public enum Category : int
    //{
    //    Comedia = 0,
    //    Terror = 1,
    //    Suspense = 2,
    //    Documental = 3,
    //    Accion = 4,
    //    Animacion =5
    //}

    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    //public class Category
    //{
    //    /// <summary>
    //    /// Id de la Categoria
    //    /// </summary>
    //    public int Id { get; set; }


    //    public string Name { get; set; }



    //}
}