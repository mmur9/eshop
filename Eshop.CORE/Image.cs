﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.CORE
{
    /// <summary>
    /// Entidad de dominio de Imagen
    /// </summary>
    public class Image
    {
        /// <summary>
        /// Id de la imagen
        /// </summary>
        public int Id { get; set; }

                       
        ///<summary>
        /// Url de la imagen
        ///</summary>
        public String urlImage { get; set; }

        /// <summary>
        /// Cada imagen pertenece a un producto
        /// </summary>
        public virtual Product Product { get; set; }


        /// <summary>
        /// Imagen
        /// </summary>
        //public byte[] ImageData { get; set; }

        //public string ImageTitle { get; set; }

        ///// <summary>
        ///// FK Product
        ///// </summary>
        //[ForeignKey("Product")]
        //public int ProductId { get; set; }

    }
}
