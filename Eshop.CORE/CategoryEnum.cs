﻿namespace Eshop.CORE
{
    /// <summary>
    /// Entidad de dominio de enumerado categoria
    /// </summary>
    public enum CategoryEnum : int
    {
        Comedia = 0,
        Terror = 1,
        Suspense = 2,
        Documental = 3
    }
}