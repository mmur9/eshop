﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.CORE
{
    /// <summary>
    /// Enumerado de los posibles estados de un pedido
    /// </summary>
    public enum  OrderStatus : int
    {
        Nuevo = 0,

        Preparando = 1,

        Enviado = 2,

        Recibido = 3, 

        Completado = 4,

        Cancelado = 5

    }
}
