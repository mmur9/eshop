﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eshop.CORE
{
    /// <summary>
    /// Entidad de dominio de producto
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Id del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del producto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Unidades en Stock
        /// </summary>
        public int UnitsInStock { get; set; }

        /// <summary>
        /// Duración - ? es nullable
        /// </summary>
        public int? Duration { get; set; }

        /// <summary>
        /// Precio por unidad
        /// </summary>

        public decimal Price { get; set; }

        /// <summary>
        /// Un producto puede tener muchas imagenes
        /// </summary>

        public List<Image> Imagenes { get; set; }
               

        /// <summary>
        /// Url trailer youtube pelicula
        /// </summary>
        public string YtURL { get; set; }

        /// <summary>
        /// Categoria producto
        /// </summary>
        //public Category ProductCategory { get; set; }
        public Category Category { get; set; }



        /// <summary>
        /// Imagen
        /// </summary>
        //public string Image { get; set; }

        //PK 


        /// <summary>
        /// Categoria del producto
        /// </summary>
        /// 

        //[ForeignKey("Category")]
        //public int ProductCategory { get; set; }

        //public virtual Category Category { get; set; }


    }
}
