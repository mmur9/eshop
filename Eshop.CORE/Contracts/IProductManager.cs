﻿using Eshop.CORE;
using System.Linq;

namespace Eshop.CORE.Contracts
{
    public interface IProductManager : IGenericManager<Product>
    {
        //IQueryable<Product> GetProducts2();
        IQueryable<Product> GetProductByIdWithImages(int id);

    }
}