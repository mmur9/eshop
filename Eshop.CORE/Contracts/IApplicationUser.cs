﻿using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Eshop.CORE.Contracts
{

    public interface IApplicationUser
    {

        string Address { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string PostalCode { get; set; }
        string State { get; set; }


        ClaimsIdentity GenerateUserIdentity(UserManager<ApplicationUser> manager);
        Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager);
    }
}