﻿using Eshop.CORE;
using System.Linq;

namespace Eshop.CORE.Contracts
{
    public interface IOrderManager : IGenericManager<Order>
    {
        //Order GetByIdAndTotalValue(int id);
        IQueryable<Order> GetByUserId(string userId);

        IQueryable<Order> GetByUsername(string userName);

        Order GetByIdIncOrderDet(int id);

    }
}