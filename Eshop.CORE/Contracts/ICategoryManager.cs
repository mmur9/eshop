﻿using Eshop.CORE;
using System.Linq;

namespace Eshop.CORE.Contracts
{
    public interface ICategoryManager : IGenericManager<Category>
    {
        IQueryable<Category> GetCategoriesTest(int id);
        IQueryable<Category> GetAllCategories();

    }
}