﻿using Eshop.CORE;
using System;
using System.Linq;

namespace Eshop.CORE.Contracts
{
    public interface ICartItemManager : IGenericManager<CartItem>
    {
        IQueryable<CartItem> GetCartItemByUserAndProduct(String userCart, int productCart);
        IQueryable<CartItem> GetCartItemByUser(String userCart);
        int GetCartAmount(string userCart);
        void CleanCart(string userCart);

        IQueryable<CartItem> GetCartItemById(String idCart);



    }
}