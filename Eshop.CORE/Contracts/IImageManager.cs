﻿using Eshop.CORE;
using System.Linq;

namespace Eshop.CORE.Contracts
{
    public interface IImageManager : IGenericManager<Image>
    {
        IQueryable<Image> GetDataImageByProductId(int Id);
    }
}