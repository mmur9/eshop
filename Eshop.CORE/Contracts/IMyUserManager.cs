﻿using Eshop.CORE;
using Eshop.CORE.Contracts;
using System.Linq;

namespace Eshop.CORE.Contracts
{
    public interface IMyUserManager
    {
        IQueryable<ApplicationUser> GetUsers();
        IQueryable<ApplicationUser> GetUsersByEmail(string email);
        IQueryable<ApplicationUser> GetUsersById(string id);
    }
}