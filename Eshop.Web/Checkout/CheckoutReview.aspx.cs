﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Eshop;
using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using Eshop.Web.Client.Logica;

namespace Eshop.Web.Checkout
{
    public partial class CheckoutReview : System.Web.UI.Page
    {
        ApplicationDbContext context = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                NVPAPICaller payPalCaller = new NVPAPICaller();

                string retMsg = "";
                string token = "";
                string PayerID = "";
                NVPCodec decoder = new NVPCodec();
                token = Session["token"].ToString();


                //Capturamos el pedido realizado por el usuario
                OrderManager orderManager = new OrderManager(context);
                ShoppingCartActions shoppingCartActions = new ShoppingCartActions();

                int createdOrderId = orderManager.GetByUserId(User.Identity.Name).ToList().Last().Id;
               

                bool ret = payPalCaller.GetCheckoutDetails(token, ref PayerID, ref decoder, ref retMsg);
                if (ret)
                {
                    Session["payerId"] = PayerID;


                    //VACIAMOS CARRITO
                    shoppingCartActions.EmptyCart();

                    //Redireccionamos a ver el pedido
                    Response.Redirect("/Client/Orders/OrderDetails?id=" + createdOrderId);


                }
                else
                {
                    Response.Redirect("CheckoutError.aspx?" + retMsg);
                }
            }
        }


    }
}