﻿<%@ Page Title="Contacto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Eshop.Web.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>&nbsp;</h3>
    <address>
        E-Shop MMUR<br />
        Redmond, WA 98052-6399<br />
        <abbr title="Phone">P:</abbr>
        425.555.0100
    </address>

        <div class="form-group row">
  <div class="col-xs-6">
    <label for="ex2">Asunto</label>
      <asp:TextBox ID="txtAsunto" runat="server" CssClass="form-control" rows="5"></asp:TextBox>
  </div><br />

</div>
  <div class="form-group">
  <label for="comment">Mensaje:</label>
      <asp:TextBox ID="txtMensaje" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
</div>

        <asp:Button ID="Button1" runat="server" Text="Enviar"  CssClass="btn btn-primary" OnClick="Button1_Click" />
</asp:Content>
