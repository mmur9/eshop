﻿using Eshop.Web.Admin.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                SendMail sendMail = new SendMail();
                sendMail.SendMyMail("adm-eshop-mmur@outlook.com", txtAsunto.Text, txtMensaje.Text);
                Response.Redirect(Page.Request.RawUrl);

            }
            catch (Exception ex)
            {
                //TODO: escribir error en log
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al mandar el mail",
                    IsValid = false
                };
                Page.Validators.Add(err);
            }
        }
    }
}