﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index1.aspx.cs" Inherits="Eshop.Web.Test.Index1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <header>
        <style>
            html,
            body {
                margin: 0;
                font-family: "Helvetica Neue", Helvetica, sans-serif;
                color: white;
                background-color: black;
                font-size: 16px;
            }

            * {
                box-sizing: border-box;
            }

            header {
                background: linear-gradient(to right, rgba(0, 0, 0, 0.9), rgba(0, 0, 0, 0.5)), url("https://github.com/jmtalarn/ironhack_css_clones/blob/master/netflix/img/background-netflix-header.jpg?raw=true");
                height: 100vh;
                width: 100%;
            }

            nav img {
                width: 167px;
                height: 45px;
                vertical-align: middle;
            }

            nav a.logo {
                display: inline-block;
                line-height: 90px;
                margin-left: 3%;
            }

            header nav {
                height: 90px;
            }

            nav .signin {
                color: #fff;
                float: right;
                background-color: #e50914;
                line-height: normal;
                margin-top: 18px;
                margin-right: 3%;
                padding: 7px 17px;
                font-weight: 400;
                border-radius: 3px;
                font-size: 16px;
                text-decoration: none;
            }

            .pitch {
                margin: 0 3%;
                position: absolute;
                top: 35%;
                font-size: 1.8vw;
            }

            .pitch-title {
                font-size: 3em;
                margin: 0 0 0.2em;
                font-weight: 700;
            }

            .pitch-subtitle {
                margin: 0 0 0.5em;
            }

            .btn {
                font-size: 14px;
                letter-spacing: 1.9px;
                font-weight: 400;
                margin: 0.5em 0.5em 0.5em 0;
                padding: 12px 2em;
                color: #fff;
                background-color: #e50914;
                cursor: pointer;
                text-decoration: none;
                vertical-align: middle;
                font-family: Arial, sans-serif;
                border-radius: 2px;
                user-select: none;
                text-align: center;
                border: none;
            }

                nav .signin:hover,
                .btn:hover {
                    background: #f40612;
                }

            #features > nav {
                padding-top: 35px;
                background-color: #141414;
                border-bottom: 2px solid #3d3d3d;
            }

                #features > nav a {
                    text-decoration: none;
                    text-align: center;
                    vertical-align: top;
                    font-weight: bold;
                    color: #777;
                    height: 125px;
                    line-height: 20px;
                }

                    #features > nav a:hover {
                        color: white;
                    }

                    #features > nav a img {
                        height: 49px;
                        width: 49px;
                    }

                    #features > nav a h2 {
                        margin: 10px 0 0;
                        font-size: 1em;
                    }

            .columns {
                display: flex;
            }

            .column {
                display: block;
                flex-basis: 0;
                flex-grow: 1;
                flex-shrink: 1;
            }

            .center {
                margin: 0 auto;
            }

            .size-80 {
                width: 80%;
            }

            #features > nav a.is-selected {
                border-bottom: 5px solid #e50915;
                color: white;
            }

            #features > article > section {
                display: none;
                padding: 40px 0;
            }

                #features > article > section.is-selected {
                }
        </style>
        
    </header>

    <section class="pitch">
        <h1 class="pitch-title">E-SHOP M.</h1>
        <p class="pitch-subtitle">Bienvenido, ¿Qué te apetece?</p>
        <asp:Button ID="btnMovies" runat="server" Text="EXPLORA NUESTRO CATÁLOGO" CssClass="btn" OnClick="btnMovies_Click"/>

    </section>
    <div id="features">
        <nav>
            <div class="center columns size-80">
                <a  class="column is-selected" data-id="cancelanytime">
                    <img alt="Cancel anytime" src="http://i67.tinypic.com/2qvw9ro.jpg" />
                    <h2>                        Sin permanencia

					 <br />
                        Cancela tu cuenta cuando desees
                    </h2>
                </a>
                <a  class="column is-selected" data-id="watchanywhere">
                    <img alt="Watch anywhere" src="http://i67.tinypic.com/wv78z7.jpg" />
                    <h2>Productos para todo tipo de plataformas
                    </h2>
                </a>
                <a  class="column is-selected" data-id="pickprice">
                    <img alt="Pick your price" src="http://i65.tinypic.com/2zf8xed.jpg" />
                    <h2>Te devolvemos tu dinero
                    </h2>
                </a>
            </div>



        </nav>
    </div>
</asp:Content>
