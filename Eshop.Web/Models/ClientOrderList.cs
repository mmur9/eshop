﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eshop.Web.Models
{
    public class ClientOrderList
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Total { get; set; }
        public DateTime OrderDate { get; set; }
        public string Status { get; set; }


    }
}