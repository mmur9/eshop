﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eshop.Web.Models
{
    public class UserListModel
    {
        public string Email { get; set; }
        public string Username { get; set; }
    }
}