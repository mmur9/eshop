﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eshop.Web.Models
{
    public class AdminProductList
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
    }
}