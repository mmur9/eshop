﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Eshop.Web.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Create a new account</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">Email</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="The email field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="The password field is required." />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">Confirm password</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
            </div>
        </div>
        <br />

        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Nombre" CssClass="col-md-2 control-label" AssociatedControlID="txtName"></asp:Label>
            <div class="col-md10">
                <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El nombre es obligatorio" ControlToValidate="txtName" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>


        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Apellido" CssClass="col-md-2 control-label" AssociatedControlID="txtLastName"></asp:Label>
            <div class="col-md10">
                <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="El apellido es obligatorio" ControlToValidate="txtLastName" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Teléfono" CssClass="col-md-2 control-label" AssociatedControlID="txtTelephone"></asp:Label>
            <div class="col-md10">
                <asp:TextBox ID="txtTelephone" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="El teléfono es obligatorio" ControlToValidate="txtTelephone" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Dirección" CssClass="col-md-2 control-label" AssociatedControlID="txtMyAddress"></asp:Label>
            <div class="col-md10">
                <asp:TextBox ID="txtMyAddress" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="La dirección es obligatoria" ControlToValidate="txtMyAddress" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label8" runat="server" Text="CP" CssClass="col-md-2 control-label" AssociatedControlID="txtMyZip"></asp:Label>
            <div class="col-md10">
                <asp:TextBox ID="txtMyZip" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="El CP es obligatorio" ControlToValidate="txtMyZip" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Ciudad" CssClass="col-md-2 control-label" AssociatedControlID="txtMyCity"></asp:Label>
            <div class="col-md10">
                <asp:TextBox ID="txtMyCity" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="La ciudad es obligatoria" ControlToValidate="txtMyCity" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
                <div class="form-group">
            <asp:Label ID="Label6" runat="server" Text="Estado" CssClass="col-md-2 control-label" AssociatedControlID="txtMyState"></asp:Label>
            <div class="col-md10">
                <asp:TextBox ID="txtMyState" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="La ciudad es obligatoria" ControlToValidate="txtMyState" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="País" CssClass="col-md-2 control-label" AssociatedControlID="txtMyCountry"></asp:Label>
            <div class="col-md10">
                <asp:TextBox ID="txtMyCountry" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="El país es obligatorio" ControlToValidate="txtMyCountry" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</asp:Content>
