﻿<%@ Page Title="Mi Cuenta" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditAccount.aspx.cs" Inherits="Eshop.Web.Client.EditAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal">
        <h4>Mi Cuenta</h4>
        <hr />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Nombre" CssClass="col-md3" AssociatedControlID="txtName"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El nombre es obligatorio" ControlToValidate="txtName" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Apellido" CssClass="col-md3" AssociatedControlID="txtLastName"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="El apellido es obligatorio" ControlToValidate="txtLastName" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Teléfono" CssClass="col-md3" AssociatedControlID="txtTelephone"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtTelephone" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="El teléfono es obligatorio" ControlToValidate="txtTelephone" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Dirección" CssClass="col-md3" AssociatedControlID="txtMyAddress"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtMyAddress" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="La dirección es obligatoria" ControlToValidate="txtMyAddress" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label8" runat="server" Text="CP" CssClass="col-md3" AssociatedControlID="txtMyZip"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtMyZip" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="El CP es obligatorio" ControlToValidate="txtMyZip" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Ciudad" CssClass="col-md3" AssociatedControlID="txtMyCity"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtMyCity" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="La ciudad es obligatoria" ControlToValidate="txtMyCity" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="País" CssClass="col-md3" AssociatedControlID="txtMyCountry"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtMyCountry" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="El país es obligatorio" ControlToValidate="txtMyCountry" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
                <asp:Button ID="btnChangePassword" runat="server" Text="Cambiar contraseña" CssClass="btn btn-secondary" OnClick="btnChangePassword_Click" />
          
        </div>

        <div class="form-group">
                <asp:Button ID="btnVerifyPhoneNumber" runat="server" Text="Verificar Teléfono" CssClass="btn btn-secondary" OnClick="btnVerifyPhoneNumber_Click" />
            
        </div>

        <div class="form-group">
                <asp:Button ID="btnMoreActions" runat="server" Text="Más Acciones" CssClass="btn btn-secondary" OnClick="btnMoreActions_Click" />
            
        </div>


        <div class="form-group">
                <asp:Button ID="Button1" runat="server" Text="Actualizar Datos" OnClick="Button1_Click" CssClass="btn btn-primary btn-lg btn-block"  />
            
        </div>
    </div>
</asp:Content>

