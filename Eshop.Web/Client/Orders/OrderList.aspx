﻿<%@ Page Title="Mis Pedidos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderList.aspx.cs" Inherits="Eshop.Web.Client.Orders.OrderList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
    <h1>Mis Pedidos</h1>
    <br />
    <table id="Orders">
        <thead>
            <tr>
                <th>Id</th>
                <th>Email</th>
                <th>Importe</th>
                <th>Estado</th>
                <th>Fecha</th>

            </tr>
        </thead>
    </table>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Orders").DataTable({
                'bProcessing': true,
                'bServerSide': true,
                'sAjaxSource': '/Client/Orders/OrderServiceList.ashx',
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
                },
                "columns": [
                    { "data": "Id", "Name": "Id", "autoWidth": true },
                    { "data": "Email", "Name": "Email", "autoWidth": true },
                    { "data": "Total", "Name": "Total", "autoWidth": true },
                    { "data": "Status", "Name": "Status", "autoWidth": true },
                    { "data": "OrderDate", "Name": "OrderDate", "autoWidth": true }

                    //{ "data": "Image", "Name": "Image", "autoWidth": true },

                ],
                "columnDefs": [
                    {
                        "render": function (data, type, row) {
                            return "<a href='/Client/Orders/OrderDetails?id=" + row.Id + "' class='btn btn-pink'>" + data + "</a>";
                        },
                        "targets": 0
                    },
                    {
                        "render": function (data, type, row) {
                            var dateString = data.substr(6);
                            var currentTime = new Date(parseInt(dateString));
                            var month = currentTime.getMonth() + 1;
                            var day = currentTime.getDate();
                            var year = currentTime.getFullYear();
                            var date = day + "/" + month + "/" + year;
                            return date;
                        },
                        "targets": 4
                    },

                                        {
                        "render": function (data, type, row) {
                            if (type === 'display') {

                                var num = $.fn.dataTable.render.number(',', '.', 2).display(data);
                                return num + '€';
                            } else {
                                return data;
                            }
                        },
                        "targets": 2
                    }
                ]
            });
        });
    </script>
</asp:Content>


