﻿using Eshop.Application;
using Eshop.DAL;
using Eshop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;
using System.Security.Principal;


namespace Eshop.Web.Client.Orders
{
    /// <summary>
    /// Summary description for OrderServiceList
    /// </summary>
    public class OrderServiceList : IHttpHandler
    {
               
        public void ProcessRequest(HttpContext context)
        {
            var iDisplayLength = int.Parse(context.Request["iDisplaylength"]);
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            var sSearch = context.Request["sSearch"];
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];

            ApplicationDbContext contextdb = new ApplicationDbContext();
            OrderManager orderManager = new OrderManager(contextdb);

            #region select

            //var allProducts = productManager.GetByUserId(context.User.Identity.GetUserId());

            // Recogemos todos los pedidos existentes por el usuario de la sesión

            var currentUser = HttpContext.Current.User.Identity.Name.ToString();
            var allOrders = orderManager.GetByUserId(currentUser);

            // recogemos los valores del modelo definido en la variable orders
            var orders = allOrders
                   .Select(p => new ClientOrderList
                   {
                       Id = p.Id,
                       Email = p.Email.ToString(),
                       Total = p.Total.ToString(),
                       OrderDate = p.OrderDate,
                       Status = p.Status.ToString()





                   });

            #endregion

            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Id.ToString().Contains(@0) || 
                               Email.ToString().Contains(@0) ||
                               Total.ToString().Contains(@0) ||
                               OrderDate.ToString().Contains(@0) ||
                               Status.ToString().Contains(@0)";
                orders = orders.Where(where, sSearch);

            }
            #endregion

            #region Paginate
            orders = orders
                .OrderBy(sortColum + " " + iSortDir)
                .Skip(iDisplayStart)
                .Take(iDisplayLength);

            #endregion

            var result = new
            {
                iTotalRecords = allOrders.Count(),
                iTotalDisplayRecords = allOrders.Count(),
                aaData = orders
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}