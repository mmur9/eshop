﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Client.Orders
{
    public partial class OrderDetails : System.Web.UI.Page
    {
        //Creamos managers
        ApplicationDbContext context = null;
        OrderManager orderManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Creamos contexto de datos e instancias de managers pasandoles el contexto
            context = new ApplicationDbContext();
            orderManager = new OrderManager(context);

            //Inicializacion para tratar la variable id
            int id = 0;

            // Si hay un parametro llamado id
            if (Request.QueryString["id"] != null)
            {
                // intentamos convertir lo que nos viene en el parametro a int, y lo pasamos a id
                // en caso de ser true, el param es numero y se consigue pasar a entero
                if (int.TryParse(Request.QueryString["id"], out id))
                {
                    //intentamos coger el producto del contexto
                    var order = orderManager.GetById(id);



                    // comprobamos que exista un pedido con ese id
                    if (order != null)
                    {



                        //comprobamos si el id corresponde al usuario con el que estamos logueados
                        if(order.User_Id == User.Identity.GetUserId()){
                           LoadOrder(order);
                            // Cargamos la tabla con el ID del pedido como parametro
                            GetDetails(order.Id.ToString());
                        }
                        else
                        {
                            //Response.Write(User.Identity.GetUserId().ToString());
                            Response.Redirect("/Forbidden.html");
                        }
                        }
                    }
            }
        }
        /// <summary>
        /// Metodo que carga un pedido
        /// </summary>
        /// <param name="order"></param>
        private void LoadOrder(Order order)
        {
            txtId.Text = order.Id.ToString();
            txtUser.Text = order.Email.ToString();
            txtName.Text = order.FirstName.ToString();
            txtLastName.Text = order.LastName.ToString();
            txtAddress.Text = order.Address.ToString();
            txtPhone.Text = order.Phone.ToString();
            txtPrice.Text = order.Total.ToString()+" €";
            txtDate.Text = order.OrderDate.ToString();
            txtStatus.Text = order.Status.ToString();



        }

        /// <summary>
        /// Metodo que saca los detalles de un pedido
        /// </summary>
        /// <param name="orderId"></param>
        public void GetDetails(string orderId)
        {
            string DbConnectionString = "Data Source=(LocalDb)" + @"\" + "MSSQLLocalDB;AttachDbFilename=|DataDirectory|" + @"\" + "aspnet-Eshop.Web-20190624050241.mdf;Initial Catalog=aspnet-Eshop.Web-20190624050241;Integrated Security=True";
            SqlConnection connection = new SqlConnection(DbConnectionString);
            //especificamos consulta para sacar la info que deseamos
            string sqlQuery = "select DISTINCT Products.Name as Producto, UnitPrice as Precio, Quantity as Cantidad from OrderDetails inner join Products on OrderDetails.ProductId = Products.Id where OrderId = @orderId";


            SqlCommand cmd = new SqlCommand(sqlQuery, connection);

            //añadimos el parametro de la consulta, en este caso el id del pedido
            cmd.Parameters.Add(new SqlParameter("@orderId", System.Data.SqlDbType.VarChar, 50) { Value = orderId });

            //rellenamos el gridView
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            OrderDetailsGrid.DataSource = ds;
            OrderDetailsGrid.DataBind();

            

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }
    }
}