﻿<%@ Page Title="Mi Pedido" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderDetails.aspx.cs" Inherits="Eshop.Web.Client.Orders.OrderDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        function GetURLParameter(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }
    </script>
    <div class="form-horizontal">
        <h4>Mi Pedido</h4>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />

        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="ID" CssClass="col-md-3" AssociatedControlID="txtId"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtId" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Usuario" CssClass="col-md-3" AssociatedControlID="txtUser"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtUser" runat="server" Text="" CssClass="form-control"></asp:Label>

            </div>
        </div>
        <%-- <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Estado" CssClass="col-md-3" AssociatedControlID="ddlStatus"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="ddlStatus" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>--%>



        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Nombre" CssClass="col-md-3" AssociatedControlID="txtName"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtName" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Apellido" CssClass="col-md-3" AssociatedControlID="txtLastName"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtLastName" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label6" runat="server" Text="Dirección" CssClass="col-md-3" AssociatedControlID="txtAddress"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtAddress" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Teléfono" CssClass="col-md-3" AssociatedControlID="txtPhone"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtPhone" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label8" runat="server" Text="Importe" CssClass="col-md-3" AssociatedControlID="txtPrice"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtPrice" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label9" runat="server" Text="Fecha" CssClass="col-md-3" AssociatedControlID="txtDate"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtDate" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>
                <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Estado" CssClass="col-md-3" AssociatedControlID="txtStatus"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtStatus" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <br />
        <h4>Elementos del pedido</h4><br />
        <asp:GridView ID="OrderDetailsGrid" runat="server" CssClass="table table-striped"></asp:GridView>
      

        <br />





    </div>
</asp:Content>


