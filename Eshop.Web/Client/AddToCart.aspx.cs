﻿using Eshop.Web.Client.Logica;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Client
{
    /// <summary>
    /// Permite que los productos se añadan en el carrito en funcion del Id del producto
    /// </summary>
    public partial class AddToCart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Cuando carga se recoge el Id del producto
            string rawId = Request.QueryString["ProductID"];
            int productId;
            if (!String.IsNullOrEmpty(rawId) && int.TryParse(rawId, out productId))
            {
                using (ShoppingCartActions usersShoppingCart = new ShoppingCartActions())
                {
                    // Crea una instancia de AddToCart pasandole el valor del productId recibido
                    // MODIFICADO DE INT16
                    usersShoppingCart.AddToCart(Convert.ToInt32(rawId));
                }

            }
           
            //Redireccionamos al carrito cuando se añaden productos
            Response.Redirect("ShoppingCart.aspx");
        }
    }
}