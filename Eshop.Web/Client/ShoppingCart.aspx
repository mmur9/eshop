﻿<%@ Page Title="Mi Carrito" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="Eshop.Web.Client.ShoppingCart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <header>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
    </header>
    <div id="ShoppingCartTitle" runat="server" class="ContentHead"><h1>Su carrito</h1></div>

            <%--        PAGINA QUE MUESTRA  LOS PRODUCTOS AGREGADOS A LA CESTA 
                CON UN GRIDVIEW, CONTORLANDO LOS ELEMENTOS DEL TYPE SELECCIONADO
                ESCOGIENDO EL METODO GetShopppingCartItems QUE DEVUELVE UNA LISTA DE
                CartItem
            --%>
     <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />

    <asp:GridView ID="CartList" runat="server" AutoGenerateColumns="False" ShowFooter="True" GridLines="Vertical" CellPadding="4"
        
        ItemType="Eshop.CORE.CartItem" SelectMethod="GetShoppingCartItems" 
        CssClass="table table-striped table-bordered" > 

        <Columns>
        <asp:BoundField DataField="Product.Id" HeaderText="ID" SortExpression="ProductId" />        
        <asp:BoundField DataField="Product.Name" HeaderText="Nombre" />        
        <asp:BoundField DataField="Product.Price" HeaderText="Precio por unidad" DataFormatString="{0:c}"/>     
        <asp:TemplateField   HeaderText="Unidades">            
                <ItemTemplate>
                    <asp:TextBox ID="PurchaseQuantity" Width="40" runat="server" Text="<%#: Item.Quantity %>"></asp:TextBox> 
                </ItemTemplate>        
        </asp:TemplateField>    
        <asp:TemplateField HeaderText="Precio Total Artículo">            
                <ItemTemplate>
                    <%#: String.Format("{0:c}", ((Convert.ToDouble(Item.Quantity)) *  Convert.ToDouble(Item.Product.Price)))%>
                </ItemTemplate>        
        </asp:TemplateField> 
        <asp:TemplateField HeaderText="Quitar artículo">            
                <ItemTemplate>
                    <asp:CheckBox id="Remove" runat="server"></asp:CheckBox>
                </ItemTemplate>        
        </asp:TemplateField>    
        </Columns>    
    </asp:GridView>
    <div>
        <p></p>
        <strong>
            <asp:Label ID="LabelTotalText" runat="server" Text="Importe Total: "></asp:Label>
            <asp:Label ID="lblTotal" runat="server" EnableViewState="false"></asp:Label>
        </strong> 
    </div>
    <br />
    <table> 
    <tr>
      <td>
        <asp:Button ID="UpdateBtn" runat="server" Text="Actualizar Carrito" OnClick="UpdateBtn_Click" class="btn btn-primary"/>
      </td>
      <td>
        <!--Checkout Placeholder -->
          <asp:ImageButton ID="CheckoutImageBtn" runat="server" 
                      ImageUrl="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" 
                      Width="145" AlternateText="Check out with PayPal" 
                      OnClick="CheckoutBtn_Click" 
                      BackColor="Transparent" BorderWidth="0" />
          
      </td>
    </tr>
    </table>

    <br />
        <div id="errorDiv" runat="server">
    </div>
          

</asp:Content>
