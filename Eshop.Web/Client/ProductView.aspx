﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductView.aspx.cs" Inherits="Eshop.Web.Client.ProductView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        #DIV_1 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 1111px;
    width: 1200px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 600px 555.5px;
    transform-origin: 600px 555.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px -15px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_1*/

#DIV_1:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_1:after*/

#DIV_1:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_1:before*/

#DIV_2 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: left;
    height: 1091px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 1200px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 600px 545.5px;
    transform-origin: 600px 545.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px 0px 20px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_2*/

#SCRIPT_3, #BR_122, #BR_124, #SCRIPT_177 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#SCRIPT_3, #BR_122, #BR_124, #SCRIPT_177*/

#HEADER_4 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 77px;
    max-height: 500px;
    width: 1200px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 600px 38.5px;
    transform-origin: 600px 38.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 20px 0px 0px;
    outline: rgb(80, 80, 80) none 0px;
    overflow: hidden;
}/*#HEADER_4*/

#DIV_5 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 77px;
    width: 1170px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 585px 38.5px;
    transform-origin: 585px 38.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px 15px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_5*/

#DIV_5:after {
    background-position: 0px 0px;
    bottom: -1px;
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    left: 15px;
    position: relative;
    right: -15px;
    top: 1px;
    z-index: 2;
    column-rule-color: rgb(80, 80, 80);
    background: rgba(0, 0, 0, 0) none repeat scroll 0px 0px / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_5:after*/

#DIV_5:before {
    background-position: 0px 0px;
    bottom: -10.7656px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    left: 351px;
    position: relative;
    right: -351px;
    top: 10.78px;
    z-index: 1;
    column-rule-color: rgb(80, 80, 80);
    transform: matrix(1, 0, 0, 1, 0, 0);
    background: rgba(0, 0, 0, 0) none repeat scroll 0px 0px / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_5:before*/

#DIV_6 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 77px;
    width: 1200px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 600px 38.5px;
    transform-origin: 600px 38.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px -15px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_6*/

#DIV_6:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_6:after*/

#DIV_6:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_6:before*/

#DIV_7 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: left;
    height: 77px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 700px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 350px 38.5px;
    transform-origin: 350px 38.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px 15px;
}/*#DIV_7*/

#H1_8 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 30px;
    width: 670px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 335px 15px;
    transform-origin: 335px 15px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 30px / 30px FSAlbertWeb-Bold, Arial, sans-serif;
    margin: 20px 0px 7px;
    outline: rgb(80, 80, 80) none 0px;
}/*#H1_8*/

#A_9, #A_20 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    text-decoration: none;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#A_9, #A_20*/

#DIV_10 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    width: 1200px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 600px 0px;
    transform-origin: 600px 0px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_10*/

#DIV_11 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 964px;
    width: 1230px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 615px 482px;
    transform-origin: 615px 482px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 30px -15px 0px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_11*/

#DIV_11:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_11:after*/

#DIV_11:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_11:before*/

#DIV_12 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: left;
    height: 964px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 1230px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 615px 482px;
    transform-origin: 615px 482px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px 15px;
}/*#DIV_12*/

#FORM_13 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 964px;
    width: 1200px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 600px 482px;
    transform-origin: 600px 482px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px;
    outline: rgb(80, 80, 80) none 0px;
}/*#FORM_13*/

#INPUT_14, #INPUT_16 {
    color: rgb(80, 80, 80);
    display: none;
    height: auto;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    background: rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px;
}/*#INPUT_14, #INPUT_16*/

#DIV_15 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: none;
    height: auto;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_15*/

#INPUT_17 {
    color: rgb(80, 80, 80);
    display: none;
    height: auto;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    background: rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px;
}/*#INPUT_17*/

#DIV_18 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: left;
    height: 460px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 600px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 300px 230px;
    transform-origin: 300px 230px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px 15px;
}/*#DIV_18*/

#DIV_19 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 360px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 180px;
    transform-origin: 285px 180px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px 0px 10px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_19*/

#IMG_21 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: inline-block;
    height: 360px;
    max-width: 100%;
    vertical-align: middle;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 180px;
    transform-origin: 285px 180px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(221, 221, 221);
    border-radius: 4px 4px 4px 4px;
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}/*#IMG_21*/

#DIV_22 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 80px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 40px;
    transform-origin: 285px 40px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 10px 0px 0px;
}/*#DIV_22*/

#UL_23 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 70px;
    width: 600px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 300px 35px;
    transform-origin: 300px 35px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    margin: 0px -15px 10px;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px;
}/*#UL_23*/

#UL_23:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
}/*#UL_23:after*/

#UL_23:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
}/*#UL_23:before*/

#LI_24 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: left;
    height: 70px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 150px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 75px 35px;
    transform-origin: 75px 35px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px 15px;
}/*#LI_24*/

#A_25 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    text-align: left;
    text-decoration: none;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
}/*#A_25*/

#IMG_26 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: inline-block;
    height: 70px;
    max-width: 100%;
    text-align: left;
    vertical-align: middle;
    width: 105px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 52.5px 35px;
    transform-origin: 52.5px 35px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(221, 221, 221);
    border-radius: 4px 4px 4px 4px;
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}/*#IMG_26*/

#DIV_27 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: none;
    height: auto;
    left: 0px;
    opacity: 0;
    position: fixed;
    right: 0px;
    top: 0px;
    width: auto;
    z-index: 1050;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    overflow: hidden;
    transition: opacity 0.15s linear 0s;
}/*#DIV_27*/

#DIV_28 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    position: relative;
    width: 940px;
    z-index: 0;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 30px auto;
    outline: rgb(80, 80, 80) none 0px;
    transition: transform 0.3s ease-out 0s;
}/*#DIV_28*/

#DIV_29 {
    box-shadow: rgba(0, 0, 0, 0.498039) 0px 5px 15px 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    position: relative;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box padding-box;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 6px 6px 6px 6px;
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_29*/

#DIV_30 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border-top: 0px none rgb(80, 80, 80);
    border-right: 0px none rgb(80, 80, 80);
    border-bottom: 1px solid rgb(229, 229, 229);
    border-left: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 15px;
}/*#DIV_30*/

#DIV_30:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    height: auto;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_30:after*/

#DIV_30:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    height: auto;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_30:before*/

#BUTTON_31 {
    background-position: 0px 0px;
    cursor: pointer;
    display: block;
    float: right;
    height: auto;
    opacity: 0.2;
    text-shadow: rgb(255, 255, 255) 0px 1px 0px;
    width: auto;
    z-index: 0;
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    background: rgba(0, 0, 0, 0) none repeat scroll 0px 0px / auto padding-box border-box;
    border: 0px none rgb(0, 0, 0);
    font: normal normal bold normal 21px / 21px FSAlbertWeb, Arial, sans-serif;
    margin: -2px 0px 0px;
    padding: 0px;
}/*#BUTTON_31*/

#H4_32 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 18px / 25.7143px FSAlbertWeb-Bold, Arial, sans-serif;
    margin: 0px;
    outline: rgb(80, 80, 80) none 0px;
}/*#H4_32*/

#DIV_33 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    position: relative;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 15px;
}/*#DIV_33*/

#DIV_34 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    position: relative;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_34*/

#OL_35 {
    bottom: 20px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    left: 50%;
    position: absolute;
    text-align: center;
    width: 60%;
    z-index: 15;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    margin: 0px 0px 10px -30%;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px;
}/*#OL_35*/

#LI_36 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    cursor: pointer;
    display: inline-block;
    height: 12px;
    text-align: center;
    text-indent: -999px;
    width: 12px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(255, 255, 255);
    border-radius: 10px 10px 10px 10px;
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
}/*#LI_36*/

#LI_37 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    cursor: pointer;
    display: inline-block;
    height: 10px;
    text-align: center;
    text-indent: -999px;
    width: 10px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 1px solid rgb(255, 255, 255);
    border-radius: 10px 10px 10px 10px;
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    margin: 1px;
    outline: rgb(80, 80, 80) none 0px;
}/*#LI_37*/

#DIV_38 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    position: relative;
    width: 100%;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    overflow: hidden;
}/*#DIV_38*/

#DIV_39 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    left: 0px;
    position: relative;
    width: auto;
    z-index: 0;
    backface-visibility: hidden;
    column-rule-color: rgb(80, 80, 80);
    perspective: 1000px;
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    transition: transform 0.6s ease-in-out 0s;
}/*#DIV_39*/

#IMG_40, #IMG_42 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: inline-block;
    max-width: 100%;
    vertical-align: middle;
    column-rule-color: rgb(80, 80, 80);
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(221, 221, 221);
    border-radius: 4px 4px 4px 4px;
    font: normal normal normal normal 14px / 14px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}/*#IMG_40, #IMG_42*/

#DIV_41 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: none;
    height: auto;
    position: relative;
    width: auto;
    z-index: 0;
    backface-visibility: hidden;
    column-rule-color: rgb(80, 80, 80);
    perspective: 1000px;
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    transition: transform 0.6s ease-in-out 0s;
}/*#DIV_41*/

#A_43 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    display: block;
    left: 0px;
    opacity: 0.5;
    position: absolute;
    text-align: center;
    text-decoration: none;
    text-shadow: rgba(0, 0, 0, 0.6) 0px 1px 2px;
    top: 0px;
    width: 15%;
    z-index: 0;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    background: rgba(0, 0, 0, 0) linear-gradient(to right, rgba(0, 0, 0, 0.498039) 0px, rgba(0, 0, 0, 0) 100%) repeat-x scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(255, 255, 255);
    font: normal normal normal normal 20px / 28.5714px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#A_43*/

#SPAN_44 {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    display: block;
    height: 30px;
    left: 50%;
    position: absolute;
    text-align: center;
    text-shadow: rgba(0, 0, 0, 0.6) 0px 1px 2px;
    top: 50%;
    width: 30px;
    z-index: 5;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(255, 255, 255);
    font: normal normal normal normal 30px / 30px serif;
    margin: -10px 0px 0px -10px;
    outline: rgb(255, 255, 255) none 0px;
}/*#SPAN_44*/

#SPAN_44:before {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    content: '"‹"';
    text-align: center;
    text-shadow: rgba(0, 0, 0, 0.6) 0px 1px 2px;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(255, 255, 255);
    font: normal normal normal normal 30px / 30px serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#SPAN_44:before*/

#A_45 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    display: block;
    opacity: 0.5;
    position: absolute;
    right: 0px;
    text-align: center;
    text-decoration: none;
    text-shadow: rgba(0, 0, 0, 0.6) 0px 1px 2px;
    top: 0px;
    width: 15%;
    z-index: 0;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    background: rgba(0, 0, 0, 0) linear-gradient(to right, rgba(0, 0, 0, 0) 0px, rgba(0, 0, 0, 0.498039) 100%) repeat-x scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(255, 255, 255);
    font: normal normal normal normal 20px / 28.5714px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#A_45*/

#SPAN_46 {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    display: block;
    height: 30px;
    position: absolute;
    right: 50%;
    text-align: center;
    text-shadow: rgba(0, 0, 0, 0.6) 0px 1px 2px;
    top: 50%;
    width: 30px;
    z-index: 5;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(255, 255, 255);
    font: normal normal normal normal 30px / 30px serif;
    margin: -10px -10px 0px 0px;
    outline: rgb(255, 255, 255) none 0px;
}/*#SPAN_46*/

#SPAN_46:before {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    content: '"›"';
    text-align: center;
    text-shadow: rgba(0, 0, 0, 0.6) 0px 1px 2px;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(255, 255, 255);
    font: normal normal normal normal 30px / 30px serif;
    outline: rgb(255, 255, 255) none 0px;
}/*#SPAN_46:before*/

#DIV_47 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: auto;
    text-align: right;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border-top: 1px solid rgb(229, 229, 229);
    border-right: 0px none rgb(80, 80, 80);
    border-bottom: 0px none rgb(80, 80, 80);
    border-left: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 15px;
}/*#DIV_47*/

#DIV_47:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    height: auto;
    text-align: right;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_47:after*/

#DIV_47:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    height: auto;
    text-align: right;
    width: auto;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_47:before*/

#BUTTON_48 {
    color: rgb(51, 51, 51);
    cursor: pointer;
    height: auto;
    touch-action: manipulation;
    vertical-align: middle;
    white-space: nowrap;
    width: auto;
    column-rule-color: rgb(51, 51, 51);
    perspective-origin: 50% 50%;
    transform-origin: 50% 50%;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(204, 204, 204);
    border-radius: 4px 4px 4px 4px;
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(51, 51, 51) none 0px;
    padding: 7px 10px;
}/*#BUTTON_48*/

#DIV_49 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: right;
    height: 712px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 600px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 300px 356px;
    transform-origin: 300px 356px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px 0px 30px;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px 15px;
}/*#DIV_49*/

#DIV_50 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 80px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 40px;
    transform-origin: 285px 40px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px 0px 20px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_50*/

#DIV_51 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: inline-block;
    height: 60px;
    width: 399px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 199.5px 30px;
    transform-origin: 199.5px 30px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px 0px 20px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_51*/

#DIV_52, #DIV_53 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 20px;
    width: 399px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 199.5px 10px;
    transform-origin: 199.5px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_52, #DIV_53*/

#DIV_54 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 20px;
    width: 399px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 199.5px 10px;
    transform-origin: 199.5px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_54*/

#DIV_55 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: right;
    height: 100px;
    width: 171px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 85.5px 50px;
    transform-origin: 85.5px 50px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_55*/

#IMG_56 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: block;
    height: 100px;
    max-width: 100%;
    vertical-align: middle;
    width: 165px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 82.5px 50px;
    transform-origin: 82.5px 50px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#IMG_56*/

#DIV_57 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 108px;
    text-align: left;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 54px;
    transform-origin: 285px 54px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px 15px;
}/*#DIV_57*/

#DIV_57:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    text-align: left;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_57:after*/

#DIV_57:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    text-align: left;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_57:before*/

#DIV_58 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 98px;
    text-align: left;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 49px;
    transform-origin: 285px 49px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px -15px 10px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_58*/

#DIV_59 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 34px;
    text-align: left;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 17px;
    transform-origin: 285px 17px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px 0px 20px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_59*/

#LABEL_60 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: inline-block;
    height: 20px;
    max-width: 100%;
    text-align: left;
    width: 25.4531px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 12.7188px 10px;
    transform-origin: 12.7188px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal bold normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px 7px 5px 0px;
    outline: rgb(80, 80, 80) none 0px;
}/*#LABEL_60*/

#INPUT_61 {
    box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset;
    color: rgb(128, 123, 116);
    height: 34px;
    text-align: center;
    width: 45px;
    column-rule-color: rgb(128, 123, 116);
    perspective-origin: 22.5px 17px;
    transform-origin: 22.5px 17px;
    border: 1px solid rgb(206, 205, 204);
    border-radius: 4px 4px 4px 4px;
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 0px 30px 0px 0px;
    outline: rgb(128, 123, 116) none 0px;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
}/*#INPUT_61*/

#SPAN_62 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    text-align: left;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#SPAN_62*/

#DIV_63 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 44px;
    text-align: left;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 22px;
    transform-origin: 285px 22px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_63*/

#BUTTON_64 {
    color: rgb(255, 255, 255);
    cursor: pointer;
    height: 44px;
    touch-action: manipulation;
    vertical-align: middle;
    white-space: nowrap;
    width: 179.531px;
    column-rule-color: rgb(255, 255, 255);
    perspective-origin: 89.7656px 22px;
    transform-origin: 89.7656px 22px;
    background: rgb(0, 138, 137) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(0, 113, 112);
    border-radius: 4px 4px 4px 4px;
    font: normal normal normal normal 20px / 28.5714px FSAlbertWeb-Bold, Arial, sans-serif;
    outline: rgb(255, 255, 255) none 0px;
    padding: 7px 20px;
}/*#BUTTON_64*/

#I_65 {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    display: inline-block;
    height: 20px;
    text-align: center;
    vertical-align: -15%;
    white-space: nowrap;
    width: 24.7656px;
    column-rule-color: rgb(255, 255, 255);
    align-self: flex-start;
    perspective-origin: 12.375px 10px;
    transform-origin: 12.375px 10px;
    border: 0px none rgb(255, 255, 255);
    font: normal normal normal normal 26.6667px / 20px FontAwesome;
    margin: 0px 7px 0px 0px;
    outline: rgb(255, 255, 255) none 0px;
}/*#I_65*/

#I_65:before {
    box-sizing: border-box;
    color: rgb(255, 255, 255);
    content: '""';
    cursor: pointer;
    text-align: center;
    white-space: nowrap;
    column-rule-color: rgb(255, 255, 255);
    border: 0px none rgb(255, 255, 255);
    font: normal normal normal normal 26.6667px / 20px FontAwesome;
    outline: rgb(255, 255, 255) none 0px;
}/*#I_65:before*/

#DIV_66 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: left;
    height: 20px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 10px;
    transform-origin: 285px 10px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 20px 0px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_66*/

#EM_67 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: italic normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#EM_67*/

#DIV_68 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: left;
    height: 444px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 222px;
    transform-origin: 285px 222px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_68*/

#DIV_69 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 444px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 222px;
    transform-origin: 285px 222px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_69*/

#DIV_69:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_69:after*/

#DIV_69:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_69:before*/

#H2_70, #H2_127 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 19px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 9.5px;
    transform-origin: 285px 9.5px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    border-radius: 3px 3px 0 0;
    font: normal normal normal normal 16px / 19.2px FSAlbertWeb-Bold, Arial, sans-serif;
    margin: 0px;
    outline: rgb(80, 80, 80) none 0px;
}/*#H2_70, #H2_127*/

#DIV_71 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 410px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 205px;
    transform-origin: 285px 205px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_71*/

#DIV_72 {
    background-position: 0px 0px;
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: table;
    height: 320px;
    left: 0px;
    min-height: 20px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 276px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 138px 160px;
    transform-origin: 138px 160px;
    background: rgba(0, 0, 0, 0) none repeat scroll 0px 0px / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    border-radius: 3px 3px 3px 3px;
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 15px 0px 10px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_72*/

#DIV_73, #DIV_76, #DIV_79, #DIV_82, #DIV_85, #DIV_88, #DIV_91, #DIV_94, #DIV_97, #DIV_100, #DIV_103, #DIV_106, #DIV_109, #DIV_112, #DIV_115, #DIV_118 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: table-row;
    height: 20px;
    width: 276px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 138px 10px;
    transform-origin: 138px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_73, #DIV_76, #DIV_79, #DIV_82, #DIV_85, #DIV_88, #DIV_91, #DIV_94, #DIV_97, #DIV_100, #DIV_103, #DIV_106, #DIV_109, #DIV_112, #DIV_115, #DIV_118*/

#SPAN_74, #SPAN_77, #SPAN_80, #SPAN_83, #SPAN_86, #SPAN_89, #SPAN_92, #SPAN_95, #SPAN_98, #SPAN_101, #SPAN_104, #SPAN_107, #SPAN_110, #SPAN_113, #SPAN_116, #SPAN_119 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: table-cell;
    height: 20px;
    width: 207px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 103.5px 10px;
    transform-origin: 103.5px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#SPAN_74, #SPAN_77, #SPAN_80, #SPAN_83, #SPAN_86, #SPAN_89, #SPAN_92, #SPAN_95, #SPAN_98, #SPAN_101, #SPAN_104, #SPAN_107, #SPAN_110, #SPAN_113, #SPAN_116, #SPAN_119*/

#SPAN_74:after, #SPAN_77:after, #SPAN_80:after, #SPAN_83:after, #SPAN_86:after, #SPAN_89:after, #SPAN_92:after, #SPAN_95:after, #SPAN_98:after, #SPAN_101:after, #SPAN_104:after, #SPAN_107:after, #SPAN_110:after, #SPAN_113:after, #SPAN_116:after, #SPAN_119:after {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    content: '":' '"';
    display: inline-block;
    height: 20px;
    width: 9.875px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 4.9375px 10px;
    transform-origin: 4.9375px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px 7px 0px 0px;
}/*#SPAN_74:after, #SPAN_77:after, #SPAN_80:after, #SPAN_83:after, #SPAN_86:after, #SPAN_89:after, #SPAN_92:after, #SPAN_95:after, #SPAN_98:after, #SPAN_101:after, #SPAN_104:after, #SPAN_107:after, #SPAN_110:after, #SPAN_113:after, #SPAN_116:after, #SPAN_119:after*/

#SPAN_75, #SPAN_78, #SPAN_84, #SPAN_90, #SPAN_96, #SPAN_102, #SPAN_108, #SPAN_114, #SPAN_120 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: table-cell;
    height: 20px;
    width: 69px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 34.5px 10px;
    transform-origin: 34.5px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#SPAN_75, #SPAN_78, #SPAN_84, #SPAN_90, #SPAN_96, #SPAN_102, #SPAN_108, #SPAN_114, #SPAN_120*/

#SPAN_81, #SPAN_87, #SPAN_99, #SPAN_111 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: table-cell;
    height: 20px;
    width: 69px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 34.5px 10px;
    transform-origin: 34.5px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#SPAN_81, #SPAN_87, #SPAN_99, #SPAN_111*/

#SPAN_93, #SPAN_105 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: table-cell;
    height: 20px;
    width: 69px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 34.5px 10px;
    transform-origin: 34.5px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#SPAN_93, #SPAN_105*/

#SPAN_117 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: table-cell;
    height: 20px;
    width: 69px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 34.5px 10px;
    transform-origin: 34.5px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#SPAN_117*/

#DIV_121 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 80px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 40px;
    transform-origin: 285px 40px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_121*/

#BR_123 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#BR_123*/

#DIV_125 {
    bottom: 0px;
    box-sizing: border-box;
    clear: left;
    color: rgb(80, 80, 80);
    float: left;
    height: 504px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 600px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 300px 252px;
    transform-origin: 300px 252px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px 15px;
}/*#DIV_125*/

#DIV_126 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 49px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 24.5px;
    transform-origin: 285px 24.5px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    margin: 20px 0px;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_126*/

#DIV_126:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_126:after*/

#DIV_126:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_126:before*/

#UL_128 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 20px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 10px;
    transform-origin: 285px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    margin: 0px 0px 10px;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px;
}/*#UL_128*/

#LI_129 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 20px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 10px;
    transform-origin: 285px 10px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
}/*#LI_129*/

#A_130 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    text-align: left;
    text-decoration: none;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    margin: 10px 0px 0px;
    outline: rgb(80, 80, 80) none 0px;
}/*#A_130*/

#A_130:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    content: '"' '"';
    display: inline-block;
    height: 14px;
    text-align: left;
    width: 15.5px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 7.75px 7px;
    transform-origin: 7.75px 7px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 14px FontAwesome;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
}/*#A_130:before*/

#DIV_131 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 415px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 207.5px;
    transform-origin: 285px 207.5px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_131*/

#DIV_131:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_131:after*/

#DIV_131:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_131:before*/

#DIV_132 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 19px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 9.5px;
    transform-origin: 285px 9.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_132*/

#H2_133 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 19px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 9.5px;
    transform-origin: 285px 9.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 16px / 19.2px FSAlbertWeb-Bold, Arial, sans-serif;
    margin: 20px 0px 10px;
    outline: rgb(80, 80, 80) none 0px;
}/*#H2_133*/

#UL_134, #UL_155 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 183px;
    width: 570px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 285px 91.5px;
    transform-origin: 285px 91.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    margin: 0px -15px;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px;
}/*#UL_134, #UL_155*/

#UL_134:after, #UL_155:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
}/*#UL_134:after, #UL_155:after*/

#UL_134:before, #UL_155:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 0px 0px;
    transform-origin: 0px 0px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
}/*#UL_134:before, #UL_155:before*/

#LI_135, #LI_140, #LI_145, #LI_150, #LI_156, #LI_161, #LI_166, #LI_171 {
    bottom: 0px;
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    float: left;
    height: 183px;
    left: 0px;
    min-height: 1px;
    position: relative;
    right: 0px;
    top: 0px;
    width: 142.5px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 71.25px 91.5px;
    transform-origin: 71.25px 91.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
    padding: 0px 15px;
}/*#LI_135, #LI_140, #LI_145, #LI_150, #LI_156, #LI_161, #LI_166, #LI_171*/

#A_136, #A_141, #A_146, #A_151, #A_157, #A_162, #A_167, #A_172 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: block;
    height: 113px;
    text-align: left;
    text-decoration: none;
    width: 112.5px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 56.25px 56.5px;
    transform-origin: 56.25px 56.5px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    margin: 0px 0px 10px;
    outline: rgb(80, 80, 80) none 0px;
}/*#A_136, #A_141, #A_146, #A_151, #A_157, #A_162, #A_167, #A_172*/

#IMG_137, #IMG_142, #IMG_147, #IMG_152, #IMG_158, #IMG_163, #IMG_168, #IMG_173 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    display: inline-block;
    height: 112.5px;
    max-width: 100%;
    text-align: left;
    vertical-align: middle;
    width: 112.5px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 56.25px 56.25px;
    transform-origin: 56.25px 56.25px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(221, 221, 221);
    border-radius: 4px 4px 4px 4px;
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(80, 80, 80) none 0px;
    padding: 4px;
    transition: all 0.2s ease-in-out 0s;
}/*#IMG_137, #IMG_142, #IMG_147, #IMG_152, #IMG_158, #IMG_163, #IMG_168, #IMG_173*/

#P_138, #P_143, #P_148, #P_153, #P_159, #P_164, #P_169, #P_174 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 60px;
    min-height: 60px;
    text-align: left;
    width: 112.5px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 56.25px 30px;
    transform-origin: 56.25px 30px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    margin: 0px;
    outline: rgb(80, 80, 80) none 0px;
}/*#P_138, #P_143, #P_148, #P_153, #P_159, #P_164, #P_169, #P_174*/

#A_139, #A_144, #A_154, #A_165, #A_175 {
    box-sizing: border-box;
    color: rgb(0, 138, 137);
    text-align: left;
    text-decoration: none;
    column-rule-color: rgb(0, 138, 137);
    border: 0px none rgb(0, 138, 137);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(0, 138, 137) none 0px;
}/*#A_139, #A_144, #A_154, #A_165, #A_175*/

#A_149, #A_160 {
    box-sizing: border-box;
    color: rgb(0, 138, 137);
    text-align: left;
    text-decoration: none;
    column-rule-color: rgb(0, 138, 137);
    border: 0px none rgb(0, 138, 137);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(0, 138, 137) none 0px;
}/*#A_149, #A_160*/

#A_170 {
    box-sizing: border-box;
    color: rgb(0, 138, 137);
    text-align: left;
    text-decoration: none;
    column-rule-color: rgb(0, 138, 137);
    border: 0px none rgb(0, 138, 137);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    list-style: none outside none;
    outline: rgb(0, 138, 137) none 0px;
}/*#A_170*/

#DIV_176 {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    height: 964px;
    width: 1200px;
    column-rule-color: rgb(80, 80, 80);
    perspective-origin: 600px 482px;
    transform-origin: 600px 482px;
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_176*/

#DIV_176:after {
    box-sizing: border-box;
    clear: both;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_176:after*/

#DIV_176:before {
    box-sizing: border-box;
    color: rgb(80, 80, 80);
    
    display: table;
    column-rule-color: rgb(80, 80, 80);
    border: 0px none rgb(80, 80, 80);
    font: normal normal normal normal 14px / 20px FSAlbertWeb, Arial, sans-serif;
    outline: rgb(80, 80, 80) none 0px;
}/*#DIV_176:before*/


    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <div class="layout layout-1-col" id="DIV_1">
            <div role="main" id="DIV_2">
                                <script type="text/javascript" id="SCRIPT_3">
    var optionsPrice = new Product.OptionsPrice([]);
</script>

<header class="stage  stage--no-arc" id="HEADER_4">
    <div class="container" id="DIV_5">
        <div class="section-wrap  section-wrap--row" id="DIV_6">
            <div class="stage__copy  stage__copy--text-color" id="DIV_7">
                <h1 class="stage__headline" id="H1_8">DINT FCY 14W 825 220-240V E27</h1>
                                    <a href="http://www.multi-lite.com/de_en/products.html/?manufacturer=49" id="A_9">
                        Osram</a>
                            </div>

        </div>
    </div>
</header>

<div id="DIV_10"></div>
<div class="product-view" id="DIV_11">
    <div class="product-essential" id="DIV_12">
    <form action="http://www.multi-lite.com/de_en/checkout/cart/add/uenc/aHR0cDovL3d3dy5tdWx0aS1saXRlLmNvbS9kZV9lbi9kaW50LWZjeS0xNHctODI1LTIyMC0yNDB2LWUyNy5odG1sP19fX1NJRD1V/product/12371/form_key/fccVhb1XO4qNNPs6/" method="post" id="FORM_13">
        <input name="form_key" type="hidden" value="fccVhb1XO4qNNPs6" id="INPUT_14">
        <div class="no-display" id="DIV_15">
            <input type="hidden" name="product" value="12371" id="INPUT_16">
            <input type="hidden" name="related_product" id="INPUT_17" value="">
        </div>

        <div class="product-img-box" id="DIV_18">
                <div class="product-image product-image-zoom" id="DIV_19">
        <a href="#product-media-modal" data-toggle="media" data-index="0" id="A_20">
            <img src="http://www.multi-lite.com/media/catalog/product/cache/1/image/736x460/9df78eab33525d08d6e5fb8d27136e95/1/5/15142_1.jpg" alt="DINT FCY 14W 825 220-240V E27" id="IMG_21">
        </a>
    </div>

    <div class="more-views" id="DIV_22">
        <ul id="UL_23">
                            <li id="LI_24">
                    <a href="#product-media-modal" data-toggle="media" data-index="1" id="A_25">
                        <img src="http://www.multi-lite.com/media/catalog/product/cache/1/image/95x60/9df78eab33525d08d6e5fb8d27136e95/1/5/15142_2.jpg" alt="" id="IMG_26">
                    </a>
                </li>
                    </ul>
    </div>


    <div class="modal modal-media fade" id="DIV_27">
        <div class="modal-dialog" id="DIV_28">
            <div class="modal-content" id="DIV_29">
                <div class="modal-header" id="DIV_30">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="BUTTON_31">×</button>
                    <h4 class="modal-title" id="H4_32">DINT FCY 14W 825 220-240V E27</h4>
                </div>
                <div class="modal-body" id="DIV_33">

                    <div id="DIV_34" class="carousel slide">
                        <ol class="carousel-indicators" id="OL_35">
                                                                                        <li data-target="#product-media-carousel" class="active" data-slide-to="0" id="LI_36"></li>
                                                                                                                    <li data-target="#product-media-carousel" class="" data-slide-to="1" id="LI_37"></li>
                                                                                    </ol>

                        <div class="carousel-inner" id="DIV_38">
                                                                                        <div class="item active" id="DIV_39">
                                    <img src="http://www.multi-lite.com/media/catalog/product/cache/1/image/940x587/9df78eab33525d08d6e5fb8d27136e95/1/5/15142_1.jpg" alt="DINT FCY 14W 825 220-240V E27" id="IMG_40">
                                </div>
                                                                                        <div class="item " id="DIV_41">
                                    <img src="http://www.multi-lite.com/media/catalog/product/cache/1/image/940x587/9df78eab33525d08d6e5fb8d27136e95/1/5/15142_2.jpg" alt="" id="IMG_42">
                                </div>
                                                    </div>

                                                    <a class="left carousel-control" href="#product-media-carousel" data-slide="prev" id="A_43">
                                <span class="icon-prev" id="SPAN_44"></span>
                            </a>
                            <a class="right carousel-control" href="#product-media-carousel" data-slide="next" id="A_45">
                                <span class="icon-next" id="SPAN_46"></span>
                            </a>
                                            </div>

                </div>
                <div class="modal-footer" id="DIV_47">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="BUTTON_48">Close</button>
                </div>
            </div>
        </div>
    </div>

        </div>

        <div class="product-shop" id="DIV_49">


            <div class="short-description" id="DIV_50">
                <div class="price-box" id="DIV_51">
                    <div class="info-item" id="DIV_52">Price upon request</div>
                    <div class="info-item" id="DIV_53">You will receive our written offer with your order enquiry</div>
                                            <div class="info-item" id="DIV_54">Product available</div>
                                    </div>

                                    <div class="manufacturer-image" id="DIV_55">
                        <img src="http://www.multi-lite.com/media/catalog/category/navigation/osram.jpg" alt="" id="IMG_56">
                    </div>
                            </div>

            
                        
                        
                            <div class="add-to-box" id="DIV_57">
                                                <div class="add-to-cart" id="DIV_58">
                    <div class="add-to-cart--qty" id="DIV_59">
                <label for="qty" id="LABEL_60">Qty:</label>
                <input type="text" name="qty" id="INPUT_61" maxlength="12" value="1" title="Qty" class="input-text form-control qty">
                <span class="add-to-cart--packaging-unit" id="SPAN_62">(
                    10 items per package                )</span>
            </div>
            <div class="add-to-cart--button" id="DIV_63">
                <button type="button" title="Add to Cart" class="button btn-cart btn--cta btn--icon-left" onclick="productAddToCartForm.submit(this)" id="BUTTON_64">
                    <i class="fa fa-lg fa-cart-plus" id="I_65"></i>
                    Add to Cart                </button>
            </div>
            </div>
                                    </div>
                            
            
            
            <div class="product-collateral shipping-message-block" id="DIV_66">
                <em id="EM_67">Express delivery on request</em>            </div>

            <div class="product-collateral" id="DIV_68">
                                    <div class="box-collateral box-description" id="DIV_69">
                                                    <h2 id="H2_70">Details</h2>
                                                <div class="std" id="DIV_71">
    <div class="product-options" id="DIV_72"> <div class="product-option" id="DIV_73"><span class="product-option-name" id="SPAN_74">Manufacturer</span><span class="product-option-value" id="SPAN_75">Osram</span></div> <div class="product-option" id="DIV_76"><span class="product-option-name" id="SPAN_77">Product Code</span><span class="product-option-value" id="SPAN_78">15142</span></div> <div class="product-option" id="DIV_79"><span class="product-option-name" id="SPAN_80">Wattage (W)</span><span class="product-option-value" id="SPAN_81">14</span></div> <div class="product-option" id="DIV_82"><span class="product-option-name" id="SPAN_83">Voltage (V)</span><span class="product-option-value" id="SPAN_84">220-240</span></div> <div class="product-option" id="DIV_85"><span class="product-option-name" id="SPAN_86">Base</span><span class="product-option-value" id="SPAN_87">E27</span></div> <div class="product-option" id="DIV_88"><span class="product-option-name" id="SPAN_89">Lumen (lm)</span><span class="product-option-value" id="SPAN_90">800</span></div> <div class="product-option" id="DIV_91"><span class="product-option-name" id="SPAN_92">Colour Temperature (K)</span><span class="product-option-value" id="SPAN_93">2500</span></div> <div class="product-option" id="DIV_94"><span class="product-option-name" id="SPAN_95">Colour Rendering Index (Ra)</span><span class="product-option-value" id="SPAN_96">80</span></div> <div class="product-option" id="DIV_97"><span class="product-option-name" id="SPAN_98">Colour</span><span class="product-option-value" id="SPAN_99">Warm white</span></div> <div class="product-option" id="DIV_100"><span class="product-option-name" id="SPAN_101">Energy Consumption (kWh/1000h)</span><span class="product-option-value" id="SPAN_102">14</span></div> <div class="product-option" id="DIV_103"><span class="product-option-name" id="SPAN_104">Life Time (h)</span><span class="product-option-value" id="SPAN_105">20000</span></div> <div class="product-option" id="DIV_106"><span class="product-option-name" id="SPAN_107">Dimmable</span><span class="product-option-value" id="SPAN_108">no</span></div> <div class="product-option" id="DIV_109"><span class="product-option-name" id="SPAN_110">Switching Frequency</span><span class="product-option-value" id="SPAN_111">1000000</span></div> <div class="product-option" id="DIV_112"><span class="product-option-name" id="SPAN_113">EEC</span><span class="product-option-value" id="SPAN_114">A</span></div> <div class="product-option" id="DIV_115"><span class="product-option-name" id="SPAN_116">Diameter (mm)</span><span class="product-option-value" id="SPAN_117">45</span></div> <div class="product-option" id="DIV_118"><span class="product-option-name" id="SPAN_119">Dimensions  Product LxWxH (cm)</span><span class="product-option-value" id="SPAN_120">12.3</span></div></div><div class="product-catalog-text" id="DIV_121">• Extremely durable due to extremly high switching resistance <br id="BR_122">• Much longer relamping intervals (compared with the standard reference product) <br id="BR_123">• Variable use at Direct Current and Alternating Current <br id="BR_124">• Extremely fast lumen ramp-up thanks to Quick Light Technology</div></div>
                    </div>
                            </div>

        </div>

        <div class="product-collateral-left" id="DIV_125">

                            <div class="box-collateral downloads" id="DIV_126">
                    <h2 id="H2_127">Downloads</h2>
                    <ul class="file-downloads" id="UL_128"><li id="LI_129"><a class="file-download file-download-pdf" href="/media/catalog/downloads/15142_data-sheet-EN.pdf" target="_blank" id="A_130">data-sheet</a></li></ul>                </div>
            
                <div class="box-collateral box-up-sell" id="DIV_131">
        <div class="box-title" id="DIV_132">
            <h2 id="H2_133">Additional products</h2>
        </div>
        <ul class="products-grid" id="UL_134">
                                        <li class="item" id="LI_135">
                    <a href="http://www.multi-lite.com/de_en/master-ple-c-11w-827-e27-230-240v-1ct-6.html?___SID=U" class="product-image" id="A_136"><img src="http://www.multi-lite.com/media/catalog/product/cache/1/small_image/135x/9df78eab33525d08d6e5fb8d27136e95/2/2/22178_1.jpg" width="135" height="135" alt="MASTER PLE-C 11W/827 E27 230-240V 1CT/6" title="MASTER PLE-C 11W/827 E27 230-240V 1CT/6" id="IMG_137"></a>
    <p class="product-name" id="P_138"><a href="http://www.multi-lite.com/de_en/master-ple-c-11w-827-e27-230-240v-1ct-6.html?___SID=U" id="A_139">MASTER PLE-C 11W/827 E27 230-240V 1CT/6</a></p>
            

            </li>
                                            <li class="item" id="LI_140">
                    <a href="http://www.multi-lite.com/de_en/master-stairway-15w-827-e27-1ch-6.html?___SID=U" class="product-image" id="A_141"><img src="http://www.multi-lite.com/media/catalog/product/cache/1/small_image/135x/9df78eab33525d08d6e5fb8d27136e95/2/2/22000_1.jpg" width="135" height="135" alt="MASTER Stairway 15W/827 E27 1CH/6" title="MASTER Stairway 15W/827 E27 1CH/6" id="IMG_142"></a>
    <p class="product-name" id="P_143"><a href="http://www.multi-lite.com/de_en/master-stairway-15w-827-e27-1ch-6.html?___SID=U" id="A_144">MASTER Stairway 15W/827 E27 1CH/6</a></p>
            

            </li>
                                            <li class="item" id="LI_145">
                    <a href="http://www.multi-lite.com/de_en/master-ple-c-8w-827-e27-230-240v-1ct-6.html?___SID=U" class="product-image" id="A_146"><img src="http://www.multi-lite.com/media/catalog/product/cache/1/small_image/135x/9df78eab33525d08d6e5fb8d27136e95/2/2/22008_1.jpg" width="135" height="135" alt="MASTER PLE-C 8W/827 E27 230-240V 1CT/6" title="MASTER PLE-C 8W/827 E27 230-240V 1CT/6" id="IMG_147"></a>
    <p class="product-name" id="P_148"><a href="http://www.multi-lite.com/de_en/master-ple-c-8w-827-e27-230-240v-1ct-6.html?___SID=U" id="A_149">MASTER PLE-C 8W/827 E27 230-240V 1CT/6</a></p>
            

            </li>
                                            <li class="item" id="LI_150">
                    <a href="http://www.multi-lite.com/de_en/master-ple-r-33w-865-e27-220-240v-1ct-6.html?___SID=U" class="product-image" id="A_151"><img src="http://www.multi-lite.com/media/catalog/product/cache/1/small_image/135x/9df78eab33525d08d6e5fb8d27136e95/2/2/22007_1.jpg" width="135" height="135" alt="MASTER PLE-R 33W/865 E27 220-240V 1CT/6" title="MASTER PLE-R 33W/865 E27 220-240V 1CT/6" id="IMG_152"></a>
    <p class="product-name" id="P_153"><a href="http://www.multi-lite.com/de_en/master-ple-r-33w-865-e27-220-240v-1ct-6.html?___SID=U" id="A_154">MASTER PLE-R 33W/865 E27 220-240V 1CT/6</a></p>
            

            </li>
                                                </ul><ul class="products-grid" id="UL_155">
                        <li class="item" id="LI_156">
                    <a href="http://www.multi-lite.com/de_en/master-ple-r-15w-827-e27-220-240v-1ct-6.html?___SID=U" class="product-image" id="A_157"><img src="http://www.multi-lite.com/media/catalog/product/cache/1/small_image/135x/9df78eab33525d08d6e5fb8d27136e95/2/1/21998_1.jpg" width="135" height="135" alt="MASTER PLE-R 15W/827 E27 220-240V 1CT/6" title="MASTER PLE-R 15W/827 E27 220-240V 1CT/6" id="IMG_158"></a>
    <p class="product-name" id="P_159"><a href="http://www.multi-lite.com/de_en/master-ple-r-15w-827-e27-220-240v-1ct-6.html?___SID=U" id="A_160">MASTER PLE-R 15W/827 E27 220-240V 1CT/6</a></p>
            

            </li>
                                            <li class="item" id="LI_161">
                    <a href="http://www.multi-lite.com/de_en/dint-dim-stick-18-w-825-e27.html?___SID=U" class="product-image" id="A_162"><img src="http://www.multi-lite.com/media/catalog/product/cache/1/small_image/135x/9df78eab33525d08d6e5fb8d27136e95/1/7/17684_1.jpg" width="135" height="135" alt="DINT DIM STICK 18 W 825 E27" title="DINT DIM STICK 18 W 825 E27" id="IMG_163"></a>
    <p class="product-name" id="P_164"><a href="http://www.multi-lite.com/de_en/dint-dim-stick-18-w-825-e27.html?___SID=U" id="A_165">DINT DIM STICK 18 W 825 E27</a></p>
            

            </li>
                                            <li class="item" id="LI_166">
                    <a href="http://www.multi-lite.com/de_en/master-ple-r-20w-827-e27-220-240v-1ct-6.html?___SID=U" class="product-image" id="A_167"><img src="http://www.multi-lite.com/media/catalog/product/cache/1/small_image/135x/9df78eab33525d08d6e5fb8d27136e95/2/2/22182_1.jpg" width="135" height="135" alt="MASTER PLE-R 20W/827 E27 220-240V 1CT/6" title="MASTER PLE-R 20W/827 E27 220-240V 1CT/6" id="IMG_168"></a>
    <p class="product-name" id="P_169"><a href="http://www.multi-lite.com/de_en/master-ple-r-20w-827-e27-220-240v-1ct-6.html?___SID=U" id="A_170">MASTER PLE-R 20W/827 E27 220-240V 1CT/6</a></p>
            

            </li>
                                            <li class="item" id="LI_171">
                    <a href="http://www.multi-lite.com/de_en/master-ple-r-33w-827-e27-220-240v-1ct-6.html?___SID=U" class="product-image" id="A_172"><img src="http://www.multi-lite.com/media/catalog/product/cache/1/small_image/135x/9df78eab33525d08d6e5fb8d27136e95/2/2/22006_1.jpg" width="135" height="135" alt="MASTER PLE-R 33W/827 E27 220-240V 1CT/6" title="MASTER PLE-R 33W/827 E27 220-240V 1CT/6" id="IMG_173"></a>
    <p class="product-name" id="P_174"><a href="http://www.multi-lite.com/de_en/master-ple-r-33w-827-e27-220-240v-1ct-6.html?___SID=U" id="A_175">MASTER PLE-R 33W/827 E27 220-240V 1CT/6</a></p>
            

            </li>
                            </ul>
    </div>
                    </div>

        <div class="clearer" id="DIV_176"></div>
            </form>
    <script type="text/javascript" id="SCRIPT_177">
    //<![CDATA[
        var productAddToCartForm = new VarienForm('product_addtocart_form');
        productAddToCartForm.submit = function(button, url) {
            if (this.validator.validate()) {
                var form = this.form;
                var oldUrl = form.action;

                if (url) {
                   form.action = url;
                }
                var e = null;
                try {
                    this.form.submit();
                } catch (e) {
                }
                this.form.action = oldUrl;
                if (e) {
                    throw e;
                }

                if (button && button != 'undefined') {
                    button.disabled = true;
                }
            }
        }.bind(productAddToCartForm);

        productAddToCartForm.submitLight = function(button, url){
            if(this.validator) {
                var nv = Validation.methods;
                delete Validation.methods['required-entry'];
                delete Validation.methods['validate-one-required'];
                delete Validation.methods['validate-one-required-by-name'];
                // Remove custom datetime validators
                for (var methodName in Validation.methods) {
                    if (methodName.match(/^validate-datetime-.*/i)) {
                        delete Validation.methods[methodName];
                    }
                }

                if (this.validator.validate()) {
                    if (url) {
                        this.form.action = url;
                    }
                    this.form.submit();
                }
                Object.extend(Validation.methods, nv);
            }
        }.bind(productAddToCartForm);
    //]]>
    </script>
    </div>
</div>
            </div>
        </div>
    </form>
</body>
</html>
