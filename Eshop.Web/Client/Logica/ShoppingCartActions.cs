﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Client.Logica
{
    /// <summary>
    /// Clase que contiene los metodos para las acciones del carrito
    /// </summary>
    public class ShoppingCartActions : IDisposable
    {
        public string ShoppingCartId { get; set; }

        private ApplicationDbContext context = new ApplicationDbContext();

        public const string CartSessionId = "CartId";

        /// <summary>
        /// Metodo que crea carrito
        /// </summary>
        /// <param name="id"></param>
        public void AddToCart(int id)
        {
            // Sacamos el producto de la bd
            ShoppingCartId = GetCartId();

            var cartItem = context.ShoppingCartItems.SingleOrDefault(
                c => c.CartId == ShoppingCartId
                && c.ProductId == id);

            int stock = context.Products.SingleOrDefault(
              c => c.Id == id).UnitsInStock;
                
            //Si hay stock
            if(stock > 0)
            {
                if (cartItem == null)
                {
                    // Si no hay productos de ese tipo en el carrito, lo creamos
                    cartItem = new CartItem
                    {
                        Id = Guid.NewGuid().ToString(),
                        ProductId = id,
                        CartId = ShoppingCartId,
                        Product = context.Products.SingleOrDefault(
                       p => p.Id == id),
                        Quantity = 1,
                        DateCreated = DateTime.Now
                    };
                    //añadimos el cartItem
                    context.ShoppingCartItems.Add(cartItem);
                }
                else
                {
                    // Si el producto añadido ya existe en el carrito, le aumentamos la cantidad

                    // Mientras la cantidad sea menor al stock se podrá añadir
                    if(stock >= cartItem.Quantity++)
                    {
                        cartItem.Quantity++;

                    }
                }
                context.SaveChanges();
            }



        }

        /// <summary>
        /// Metodo que oculta el carrito
        /// </summary>
        public void Dispose()
        {
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
        }

        public string GetCartId()
        {
            // Comprobamos que la sesión  tiene un valor nulo
            if (HttpContext.Current.Session[CartSessionId] == null)
            {
                // Comprobamos si el nombre del usuario de la sesión de Identity es null
                if (!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
                {
                    //Guardamos la info en el CartSessionId
                    HttpContext.Current.Session[CartSessionId] = HttpContext.Current.User.Identity.Name;
                }
                else
                {
                    // En caso contrario creamos una sesión temporal   
                    Guid tempCartId = Guid.NewGuid();
                    HttpContext.Current.Session[CartSessionId] = tempCartId.ToString();
                }
            }
            //El método devuelve la sesión
            return HttpContext.Current.Session[CartSessionId].ToString();
        }
        /// <summary>
        /// Listado de elementos del carrito
        /// </summary>
        /// <returns>Devuelve los objetos del carrito</returns>
        public List<CartItem> GetCartItems()
        {
            ShoppingCartId = GetCartId();
            
            
            return context.ShoppingCartItems.Where(
                c => c.CartId == ShoppingCartId).ToList();
        }

        /// <summary>
        /// Método que multiplica el precio del producto por la cantidad.
        /// Suma el precio de cada uno de los tipos de producto para realizar el total
        /// </summary>
        /// <returns></returns>
        public decimal GetTotal()
        {
            ShoppingCartId = GetCartId();
 
            decimal? totalPrice = decimal.Zero;
            totalPrice = (decimal?)(from CartItems in context.ShoppingCartItems
                               where CartItems.CartId == ShoppingCartId
                               select (int?)CartItems.Quantity *
                               CartItems.Product.Price).Sum();
            return totalPrice ?? decimal.Zero;
        }



        /// <summary>
        /// Método que obtiene el id del carrito
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Elemento Cart</returns>
        public ShoppingCartActions GetCart(HttpContext context)
        {
            using (var cart = new ShoppingCartActions())
            {
                cart.ShoppingCartId = cart.GetCartId();
                return cart;
            }
        }

        /// <summary>
        /// Método que actualiza en la BD los cambios realizados
        /// </summary>
        /// <param name="cartId">Le pasamos el Id del carrito</param>
        /// <param name="CartItemUpdates">Los cambios realizados, modificacion de cantidades y eliminacion de articulos</param>
        public void UpdateShoppingCartDatabase(String cartId, ShoppingCartUpdates[] CartItemUpdates)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    // Contamos los elementos
                    int CartItemCount = CartItemUpdates.Count();
                    // Generamos lista de los elementos del carrito
                    List<CartItem> myCart = GetCartItems();
                    // Bucle para cada uno de los elementos del carrito
                    foreach (var cartItem in myCart)
                    {
                        for (int i = 0; i < CartItemCount; i++)
                        {
                            Console.WriteLine("ATENCION!!!! El cartItem.Product.Id " + cartItem.Product.Id + " == " + CartItemUpdates[i].ProductId);
                            if (cartItem.Product.Id == CartItemUpdates[i].ProductId)
                            {
                                // En caso de que este marcado para eliminar
                                if (CartItemUpdates[i].PurchaseQuantity < 1 || CartItemUpdates[i].RemoveItem == true)
                                {
                                    RemoveItem(cartId, cartItem.ProductId);
                                }
                                else
                                {
                                // EN caso de que se modifique la cantidad
                                    UpdateItem(cartId, cartItem.ProductId, CartItemUpdates[i].PurchaseQuantity);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("No se ha podido actualizar en la base de datos - " + ex.Message.ToString(), ex);
                }
            }
        }

        /// <summary>
        /// Metodo que elimina elementos del carrito
        /// </summary>
        /// <param name="removeCartID">Id del carrito donde realizar la accion</param>
        /// <param name="removeProductID">Id del producto a eliminar</param>
        public void RemoveItem(string removeCartID, int removeProductID)
        {
            using (var context = new ApplicationDbContext())
            {
                try
                {
                    //Recogemos el producto a eliminar
                    var myItem = (from c in context.ShoppingCartItems where c.CartId == removeCartID && c.Product.Id == removeProductID select c).FirstOrDefault();
                    if (myItem != null)
                    {
                        // Eliminamos el producto
                        context.ShoppingCartItems.Remove(myItem);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("No se ha podido eliminar el artículo del carrito - " + ex.Message.ToString(), ex);
                }
            }
        }

        /// <summary>
        /// Metodo que actualiza los artículos del carrito
        /// </summary>
        /// <param name="updateCartID">ID del carrito a modificar</param>
        /// <param name="updateProductID">Id del producto a modificar</param>
        /// <param name="quantity">Cantidad del producto modificado</param>
        public void UpdateItem(string updateCartID, int updateProductID, int quantity)
        {
            using (var context = new ApplicationDbContext())
            {
                try
                {
                    //Recogemos el producto a modificar
                    var myItem = (from c in context.ShoppingCartItems where c.CartId == updateCartID && c.Product.Id == updateProductID select c).FirstOrDefault();
                    if (myItem != null)
                    {
                        //Cambiamos la cantidad con el parámetro recibido
                        myItem.Quantity = quantity;
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("No se ha podido actualizar el producto - " + ex.Message.ToString(), ex);
                }
            }
        }

        /// <summary>
        /// Metodo que vacia el carrito
        /// </summary>
        public void EmptyCart()
        {
            ShoppingCartId = GetCartId();
            var cartItems = context.ShoppingCartItems.Where(
                c => c.CartId == ShoppingCartId);
            //recorremos productos y los eliminamos
            foreach (var cartItem in cartItems)
            {
                context.ShoppingCartItems.Remove(cartItem);
            }
            // Save changes.             
            context.SaveChanges();
        }

        /// <summary>
        /// Metodo que cuenta la cantidad de cada tipo de articulo que hay en el carrito
        /// </summary>
        /// <returns>Cantidad de productos de la condición</returns>
        public int GetCount()
        {
            ShoppingCartId = GetCartId();

            int? count = (from cartItems in context.ShoppingCartItems
                          where cartItems.CartId == ShoppingCartId
                          select (int?)cartItems.Quantity).Sum();
            //Devuelve 0 si todas son null
            return count ?? 0;
        }

        /// <summary>
        /// struct con los atributos de un cambio
        /// </summary>
        public struct ShoppingCartUpdates
        {
            public int ProductId;
            public int PurchaseQuantity;
            public bool RemoveItem;
        }

        /// <summary>
        /// Método que cambia el carrito de usuario propietario
        /// </summary>
        /// <param name="cartId">Id del carrito a tratar</param>
        /// <param name="userName">Usuario propietario del carrito</param>
        public void MigrateCart(string cartId, string userName)
        {
            //Recoge el carrito
            var shoppingCart = context.ShoppingCartItems.Where(c => c.CartId == cartId);
            foreach (CartItem item in shoppingCart)
            {
                item.CartId = userName;
            }
            HttpContext.Current.Session[CartSessionId] = userName;
            context.SaveChanges();
        }




    }
}