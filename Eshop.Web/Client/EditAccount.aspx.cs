﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Client
{
    public partial class EditAccount : System.Web.UI.Page
    {           
        //Creamos managers
        ApplicationDbContext context = null;
        MyUserManager myUserManager = null;
        ApplicationUser applicationUser = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                context = new ApplicationDbContext();
                myUserManager = new MyUserManager(context);
                applicationUser = new ApplicationUser();

                //Capturamos el usuario de la sesion
                var currentUser = myUserManager.GetUsersByEmail(HttpContext.Current.User.Identity.Name.ToString());

                //Si lo consigue capturar cargamos los datos, en caso contrario no
                try
                {
                    LoadUser(currentUser.FirstOrDefault());

                }
                catch (Exception ex)
                {
                    Response.Redirect("/Forbidden.html");
                }
            }
        }

        /// <summary>
        /// Metodo que saca a los campos los datos del usuario
        /// </summary>
        /// <param name="user"></param>
        private void LoadUser(ApplicationUser user)
        {
            txtName.Text = user.FirstName.ToString();
            txtLastName.Text = user.LastName.ToString();
            txtMyAddress.Text = user.Address.ToString();
            txtMyCity.Text = user.City.ToString();
            txtMyCountry.Text = user.Country.ToString();
            txtMyZip.Text = user.PostalCode.ToString();
            txtTelephone.Text = user.PhoneNumber.ToString();
            
 

            if (user.PhoneNumber != null)
            {
                txtTelephone.Text = user.PhoneNumber.ToString();
            }
            else
            {
                txtTelephone.Text = "Sin Telefono";
            }
            

            }

        /// <summary>
        /// Metodo que captura el usuario de la sesion actual 
        /// </summary>
        private void UpdateUser()
        {
            //Abrimos contexto
            context = new ApplicationDbContext();



            //Recogemos el usuario buscandolo por su email
            var result = context.Users.SingleOrDefault(b => b.Email == HttpContext.Current.User.Identity.Name.ToString());

            if (result != null)
            {

                
                //Actualizamos el todos los campos del usuario
                //Gracias a los validadores no se ejecutara si hay algun campo vacio o similar
                result.FirstName = txtName.Text.ToString();
                result.LastName = txtLastName.Text.ToString();
                result.Address = txtMyAddress.Text.ToString();
                result.City = txtMyCity.Text.ToString();
                result.Country = txtMyCountry.Text.ToString();
                result.PhoneNumber = txtTelephone.Text.ToString();
                result.PostalCode = txtMyZip.Text.ToString();


                //Guardamos cambios
                context.SaveChanges();
                Response.Redirect("/Client/EditAccount");
            }
        }


        
        /// <summary>
        /// Al pulsar en el boton se actualizan los datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            UpdateUser();
        }

        /// <summary>
        /// Redirecciona a la pagina de account de cambiar password
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/ManagePassword.aspx");

        }

        protected void btnVerifyPhoneNumber_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/VerifyPhoneNumber.aspx");

        }

        protected void btnMoreActions_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/Manage.aspx");

        }
    }
}