﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using Eshop.Web.Admin.Mail;
using Eshop.Web.Client.Logica;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Client
{

    public partial class ShoppingCart : System.Web.UI.Page
    {
        //En la carga, se carga el objeto del carrito y recupera el carro llamando a GetTotal() de ShoppingCart
        protected void Page_Load(object sender, EventArgs e)
        {

            using (ShoppingCartActions usersShoppingCart = new ShoppingCartActions())
            {
                decimal cartTotal = 0;
                cartTotal = usersShoppingCart.GetTotal();
                if (cartTotal > 0)
                {
                    //Se muestra el total.
                    lblTotal.Text = String.Format("{0:c}", cartTotal);
                }
                // Si esta vacio, se mostrará un mensaje
                else
                {
                    LabelTotalText.Text = "";
                    lblTotal.Text = "";
                    ShoppingCartTitle.InnerText = "El carrito está vacío";
                    UpdateBtn.Visible = false;
                    CheckoutImageBtn.Visible = false;


                }
            }
        }

        // Devolvemos los elementos del carrito llamando a GetCartItems

        public List<CartItem> GetShoppingCartItems()
        {
            ShoppingCartActions actions = new ShoppingCartActions();
            return actions.GetCartItems();
        }

        /// <summary>
        /// Metodo que comprueba el stock
        /// </summary>
        public void CheckStock()
        {
            ShoppingCartActions actions = new ShoppingCartActions();
            var allItems = actions.GetCartItems();

            using (var context = new ApplicationDbContext())
            {
                try
                {
                    ProductManager productManager = new ProductManager(context);

                    // Contamos los elementos
                    int CartItemCount = allItems.Count();
                    // Generamos lista de los elementos del carrito
                    List<CartItem> myCart = actions.GetCartItems();
                    // Bucle para cada uno de los elementos del carrito
                    foreach (var cartItem in myCart)
                    {
                        for (int i = 0; i < CartItemCount; i++)
                        {
                            //Si el id del producto del carrito, es el mismo que el que se esta tratando, 
                            if (cartItem.Product.Id == myCart[i].ProductId)
                            {
                                //Se le resta la cantidad del stock
                                int newStock = cartItem.Product.UnitsInStock = cartItem.Product.UnitsInStock - myCart[i].Quantity;
                                if (newStock >= 0)
                                {
                                    //Sacamos el producto en cuestion
                                    int productId = myCart[i].ProductId;
                                    var result = context.Products.SingleOrDefault(b => b.Id == productId);
                                    //Actualizamos contexto de producto
                                    result.UnitsInStock = newStock;


                                    context.SaveChanges();
                                }
                                else
                                {
                                    

                                }



                            }


                        }

                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("No se ha podido actualizar en la base de datos - " + ex.Message.ToString(), ex);
                }
            }
        }

        /// <summary>
        /// Metodo que actualiza el stock
        /// </summary>
        public void UpdateStock()
        {
            ShoppingCartActions actions = new ShoppingCartActions();
            var allItems = actions.GetCartItems();

            using (var context = new ApplicationDbContext())
            {
                try
                {
                    ProductManager productManager = new ProductManager(context);

                    // Contamos los elementos
                    int CartItemCount = allItems.Count();
                    // Generamos lista de los elementos del carrito
                    List<CartItem> myCart = actions.GetCartItems();
                    // Bucle para cada uno de los elementos del carrito
                    foreach (var cartItem in myCart)
                    {
                        for (int i = 0; i < CartItemCount; i++)
                        {
                            //Si el id del producto del carrito, es el mismo que el que se esta tratando, 
                            if (cartItem.Product.Id == myCart[i].ProductId)
                            {
                                //Se le resta la cantidad del stock
                                int newStock = cartItem.Product.UnitsInStock = cartItem.Product.UnitsInStock - myCart[i].Quantity;
                                if(newStock >= 0)
                                {
                                    //Sacamos el producto en cuestion
                                    int productId = myCart[i].ProductId;
                                    var result = context.Products.SingleOrDefault(b => b.Id == productId);
                                    //Actualizamos contexto de producto
                                    result.UnitsInStock = newStock;


                                    context.SaveChanges();
                                }
                                //Si el stock es 0 se manda mail al admin
                                if(newStock == 0)
                                {
                                    SendMail sendMail = new SendMail();
                                    sendMail.SendMyMail("miguelparaweb6@gmail.com", "Alerta Stock", "El stock del articulo esta a 0");
                                }

                                                               
                            }


                        }

                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("No se ha podido actualizar en la base de datos - " + ex.Message.ToString(), ex);
                }
            }
        }
        // Sacamos una lista de productos utilizando el metodo UpdateCartItems
        public List<CartItem> UpdateCartItems()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager productManager = new ProductManager(context);

            using (ShoppingCartActions usersShoppingCart = new ShoppingCartActions())
            {
                String cartId = usersShoppingCart.GetCartId();

                ShoppingCartActions.ShoppingCartUpdates[] cartUpdates = new ShoppingCartActions.ShoppingCartUpdates[CartList.Rows.Count];
                for (int i = 0; i < CartList.Rows.Count; i++)
                {
                    IOrderedDictionary rowValues = new OrderedDictionary();
                    rowValues = GetValues(CartList.Rows[i]);
                    cartUpdates[i].ProductId = Convert.ToInt32(rowValues[0]);

                    CheckBox cbRemove = new CheckBox();
                    cbRemove = (CheckBox)CartList.Rows[i].FindControl("Remove");
                    cartUpdates[i].RemoveItem = cbRemove.Checked;

                    //COMPROBAMOS EL STOCK
                    TextBox quantityTextBox = new TextBox();
                    quantityTextBox = (TextBox)CartList.Rows[i].FindControl("PurchaseQuantity");
                    //capturamos stock viejo, por lo tanto maximo
                    int oldStock = productManager.GetById(cartUpdates[i].ProductId).UnitsInStock;
                    //stock pretendido
                    int changedStock = (Convert.ToInt16(quantityTextBox.Text));
                    //stock resultado nuevo, si es menor de 0 no cambia y muestra un mensaje
                    int newStock =  (oldStock - changedStock);
                    if (newStock >= 0)
                    {
                        cartUpdates[i].PurchaseQuantity = Convert.ToInt16(quantityTextBox.Text.ToString());
                    }
                    else
                    {
                        cartUpdates[i].PurchaseQuantity = oldStock;
                         this.errorDiv.InnerHtml += "<div class='alert alert-danger' role='alert'>Lo sentimos, no hay suficientes unidades de "+productManager.GetById(cartUpdates[i].ProductId).Name+" en stock, hemos actualizado su carrito con el número de unidades disponibles</ div ></br> ";
                        
                    }



                }
                usersShoppingCart.UpdateShoppingCartDatabase(cartId, cartUpdates);
                CartList.DataBind();
                lblTotal.Text = String.Format("{0:c}", usersShoppingCart.GetTotal());
                return usersShoppingCart.GetCartItems();
            }
        }

        public static IOrderedDictionary GetValues(GridViewRow row)
        {
            IOrderedDictionary values = new OrderedDictionary();
            foreach (DataControlFieldCell cell in row.Cells)
            {
                if (cell.Visible)
                {
                    // Sacamos valores de las celdas
                    cell.ContainingField.ExtractValuesFromCell(values, cell, row.RowState, true);

                }
            }
            return values;
        }

        /// <summary>
        /// Metodo que crea el pedido en la BD
        /// </summary>
        public void CreateOrder()
        {
            // Creamos managers
            OrderManager orderManager = null;
            MyUserManager myUserManager = null;
            ApplicationDbContext context = new ApplicationDbContext();
            //Instanciamos Managers
            orderManager = new OrderManager(context);
            myUserManager = new MyUserManager(context);
            SendMail sendMail = new SendMail();


            using (ShoppingCartActions usersShoppingCart = new ShoppingCartActions())
            {

                //Recogemos el carrito con el metodo getcartid y lo pasamos a una variable
                String cartId = usersShoppingCart.GetCartId();
                //Recogemos el precio del pedido, ya que lo necesitamos para su creacion
                decimal cartTotal = usersShoppingCart.GetTotal();
                //Recogemos el usuario del pedido gracias al metodo que creamos getusersbyemail y lo guardamos en una variable
                var orderUser = myUserManager.GetUsersByEmail(cartId);


                try
                {
                    // Almacenamos en variables  los valores de las propiedades que ya tenemos
                    //Guardamos la fecha actual y el mail del cliente
                    DateTime orderDate = DateTime.Now;
                    string email = cartId;

  
                    // Creamos un metodo que carga los datos de un usuario y crea un pedido con los mismos
                    Order LoadUser(ApplicationUser user)
                    {

                        Order createdOrder = new Order()
                        {
                            //Añadimos Propiedades de pedido desde los controles correspondientes
                            OrderDate = orderDate,
                            Email = email,
                            Total = cartTotal,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Address = user.Address,
                            City = user.City,
                            State = user.State,
                            PostalCode = user.PostalCode,
                            Country = user.Country,
                            Phone = user.PhoneNumber,
                            PaymentTransactionId = "737377373",
                            HasBeenShipped = true,
                            User_Id = user.Id,
                            Status = 0
                            

                        };
                        return createdOrder;


                    }



                    // Actualizamos stock
                    UpdateStock();

                    //Añadimos el order al order manager, pasandole por parametro el usuario recogido

                    orderManager.Add(LoadUser(orderUser.FirstOrDefault()));
                    
                    orderManager.Context.SaveChanges();
                    //Capturamos el pedido realizado por el usuario
                    int createdOrderId = orderManager.GetByUserId(cartId).ToList().Last().Id;

                    //Mandamos mail al usuario confirmando el pedido
                    string orderLink = "https://localhost:44383/Client/Orders/OrderDetails?id=" + createdOrderId;
                    sendMail.SendMyMail(cartId, "Se ha confimado su pedido " + createdOrderId + " Eshop" ,"Se ha confimado su pedido " + createdOrderId + " Eshop con un importe total de: " + orderManager.GetById(createdOrderId).Total + " €. Para ver más detalles <a href="+orderLink+">Click</a>"); 


                    //Guardamos los datos del pedido en orderDetail
                    CreateOrderDetails(createdOrderId);


                }
                catch (Exception ex)
                {
                    Response.Redirect("/Forbidden.html");
                    //PARA DEBUG
                    //foreach (var eve in e.EntityValidationErrors)
                    //{
                    //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    //    foreach (var ve in eve.ValidationErrors)
                    //    {
                    //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                    //            ve.PropertyName, ve.ErrorMessage);
                    //    }
                    //}
                    //throw;
                }

            }



            //Recoge el carrito

            //var shoppingCart = context.ShoppingCartItems.Where(c => c.CartId == cartId);
            //foreach (CartItem item in shoppingCart)
            //{
            //    item.CartId = userName;
            //    //context.Orders.;
            //}
            //HttpContext.Current.Session[CartSessionId] = userName;
            //context.SaveChanges();
        }

        /// <summary>
        /// Metodo que crea registros en order detail
        /// </summary>
        /// <param name="order"></param>
        public void CreateOrderDetails(int order)
        {
            ShoppingCartActions actions = new ShoppingCartActions();
            var allItems = actions.GetCartItems();

            using (var context = new ApplicationDbContext())
            {
                try
                {
                    ProductManager productManager = new ProductManager(context);

                    // Contamos los elementos
                    int CartItemCount = allItems.Count();
                    // Generamos lista de los elementos del carrito
                    List<CartItem> myCart = actions.GetCartItems();
                    // Bucle para cada uno de los elementos del carrito
                    foreach (var cartItem in myCart)
                    {
                        for (int i = 0; i < CartItemCount; i++)
                        {
                            
                            int quantity = myCart[i].Quantity;
                            //Sacamos el producto en cuestion
                            int productId = myCart[i].ProductId;
                            string username = myCart[i].CartId;
                            decimal unitPrice = myCart[i].Product.Price;
                            var result = context.Products.SingleOrDefault(b => b.Id == productId);
                            decimal unitPrice2 = result.Price;

                            context.OrderDetails.Add(new OrderDetail
                            {
                                OrderId = order,
                                Username = username,
                                ProductId = productId,
                                Quantity = quantity,
                                UnitPrice = unitPrice2

                            });
                            //Actualizamos contexto de producto


                            context.SaveChanges();





                        }

                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("No se ha podido actualizar en la base de datos - " + ex.Message.ToString(), ex);
                }
            }

            }
            /// <summary>
            /// Al pulsar el boton, se llama al metodo UpdateCartItems
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void UpdateBtn_Click(object sender, EventArgs e)
        {

            UpdateCartItems();

        }

        /// <summary>
        /// Al pulsar en el botón de checkout redireccionamos a la página de checkout, pasandole al payment el total del carrito y creando pedido
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckoutBtn_Click(object sender, ImageClickEventArgs e)
        {
            //Creamos el pedido
            CreateOrder();


            using (ShoppingCartActions usersShoppingCart = new ShoppingCartActions())
            {
                var cartTotal = usersShoppingCart.GetTotal();
                Session["payment_amt"] = cartTotal;

            }
            Response.Redirect("../Checkout/CheckoutStart.aspx");



        }


    }
}