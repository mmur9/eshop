﻿using Eshop.CORE;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Client.Products
{
    public partial class ProductDetails3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public IQueryable<Product> GetProduct([QueryString("productID")] int? productId)
        {
            var context = new ApplicationDbContext();
            IQueryable<Product> myquery = context.Products;
            // comprueba que no es null y que es mayor de 0
            if (productId.HasValue && productId > 0)
            {
                myquery = myquery.Where(p => p.Id == productId);




            }
            else
            {
                myquery = null;
            }
            return myquery;
        }
    }
}