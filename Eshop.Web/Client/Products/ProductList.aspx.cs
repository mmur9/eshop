﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Client
{
    public partial class ProductList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();


        }

        //Iqueryable que saca los productos de una categoria concreta
        public IQueryable<Product> GetAll([QueryString("id")] string myProductCategory)
        {
            var context = new ApplicationDbContext();
            IQueryable<Product> query = context.Products;

            //si no la categoria no es null
            if(myProductCategory != null)
            {
                //saca los productos con una categoria
                query = query.Where(p => p.Category.ToString() == myProductCategory.ToString());
                
                Response.Write("Categoria: "+myProductCategory);
            }
            return query;


        }

    }
}