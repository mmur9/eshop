﻿<%@ Page Title="Producto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="Eshop.Web.Client.Products.ProductDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <header>
        <style>
            body {
                background-image: url("/Images/whiteback.jpg");
                background-size: cover;
            }
        </style>
    </header>
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
    <asp:FormView ID="productDetail" runat="server" ItemType="Eshop.CORE.Product" SelectMethod="GetProduct" RenderOuterTable="false">
        <ItemTemplate>

            <div>
                <h1><%#:Item.Name %></h1>
            </div>
            <br />
            <table>
                <tr>
                    <td></td>
                    <td>&nbsp;</td>
                    <td style="vertical-align: top; text-align: left;">
                        <b style="font-size:large">Sinopsis:</b><br /><br />
                       <p style="font-size: medium"><%#:Item.Description %><br /></p> 
                        <h4><span class="label label-info"><b>Precio:</b>&nbsp;<%#: string.Format("{0:#.00} €", Convert.ToDecimal(Item.Price)) %></span></h4>
                        <h4><span class="label label-info"><b>Categoría:</b>&nbsp;<%#:Item.ProductCategory %></span></h4></td>




                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    <br />
    <div id="addToCartDiv" runat="server">
    </div>
    <br />
    <div>
            <asp:Image ID="Image1" runat="server" Width="300px"/>

    </div>

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 

</asp:Content>
