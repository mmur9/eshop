﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Image = Eshop.CORE.Image;

namespace Eshop.Web.Client.Products
{
    public partial class ProductDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //En el pageload lanzamos el metodo checkstock para condicionar la carga del html
            string qsValue = Request.QueryString["productID"];
            CheckStock(qsValue);
        }

        
        /// <summary>
        /// Iqueryable  que recoge el producto pasado por la url
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public IQueryable<Product> GetProduct([QueryString("productID")] int? productId)
        {
            var context = new ApplicationDbContext();
            IQueryable<Product> myquery = context.Products;
            // comprueba que no es null y que es mayor de 0
            if (productId.HasValue && productId > 0)
            {
                myquery = myquery.Where(p => p.Id == productId);
                Image1.ImageUrl = "/Client/Products/ShowImage.ashx?id=" + productId;
                

            }
            else
            {
                myquery = null;
            }
            return myquery;
        }

        /// <summary>
        /// Metodo que comprueba si hay stock para permitir o no comprar
        /// </summary>
        /// <param name="productId"></param>
        public void CheckStock(string productId)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ProductManager productManager = new ProductManager(context);

            try
            {
                int intId = int.Parse(productId);
                //Capturamos el producto que se esta tratando
                int avaliableStock = productManager.GetById(intId).UnitsInStock;
                 // Si el stock es mayor que 0 muestra el boton de comprar
                if (avaliableStock > 0)
                {
                    //mostramos boton de comprar que llama a AddToCart para agregar al carrito el producto
                    this.addToCartDiv.InnerHtml = " <a href='../AddToCart.aspx?productID=" + productId + " '><img border = '0' src='../../Images/addtocartbutton.png' width='150' /></a>";

                }                
                //Sino muestra un mensaje como que no hay stock

                else
                {
                    //this.addToCartDiv.InnerHtml = "<h4>No hay unidades en stock</h4>";
                    this.addToCartDiv.InnerHtml = "<div class='alert alert-danger' role='alert'>Lo sentimos, no hay unidades en stock</ div > ";
                }
            }
            catch
            {
                //En caso de que no se cargue ningun prodcuto, redireccionamos a la lista de productos, ya que en caso contrario no debe ser accesible
                Response.Redirect("/Client/Products/ProductList");
                

            }

        }






    }
}