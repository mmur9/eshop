﻿<%@ Page Title="Productos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProductList.aspx.cs" Inherits="Eshop.Web.Client.ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <header>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <style>
            .list-group-horizontal .list-group-item {
                display: inline-block;
            }

            .list-group-horizontal .list-group-item {
                margin-bottom: 0;
                margin-left: -4px;
                margin-right: 0;
            }

                .list-group-horizontal .list-group-item:first-child {
                    border-top-right-radius: 0;
                    border-bottom-left-radius: 4px;
                }

                .list-group-horizontal .list-group-item:last-child {
                    border-top-right-radius: 4px;
                    border-bottom-left-radius: 0;
                }
        </style>
    </header>
    <section>
        <div>
            <hgroup>
                <h2><%: Page.Title %></h2>
            </hgroup>

            <div class="container">
                <div class="row" style="padding-top: 50px">

                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-12 text-center">

                        <div class="list-group list-group-horizontal">
                            <a runat="server" class="list-group-item active" href="/Client/Products/ProductIndex">Índice</a>

                            <a runat="server" class="list-group-item" href="/Client/Products/ProductList">Todas</a>

                            <a runat="server" class="list-group-item" href="/Client/Products/ProductList?id=accion">Acción</a>

                            <a runat="server" class="list-group-item" href="/Client/Products/ProductList?id=animacion">Animación</a>


                            <a runat="server" class="list-group-item" href="/Client/Products/ProductList?id=comedia">Comedia</a>
                            <a runat="server" class="list-group-item" href="/Client/Products/ProductList?id=documental">Documental</a>

                            <a runat="server" class="list-group-item" href="/Client/Products/ProductList?id=suspense">Suspense</a>
                            <a runat="server" class="list-group-item" href="/Client/Products/ProductList?id=terror">Terror</a>




                        </div>
                    </div>



                </div>
            </div>
            <br /><br />


            <asp:ListView ID="productList" runat="server"
                DataKeyNames="Id" GroupItemCount="4"
                ItemType="Eshop.CORE.Product" SelectMethod="GetAll">
                <EmptyDataTemplate>
                    <table>
                        <tr>
                            <td>No hay datos disponibles.</td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <EmptyItemTemplate>
                    <td />
                </EmptyItemTemplate>
                <GroupTemplate>
                    <tr id="itemPlaceholderContainer" runat="server">
                        <td id="itemPlaceholder" runat="server"></td>
                    </tr>
                </GroupTemplate>
                <ItemTemplate>
                    <td runat="server">
                        <table>
                            <tr>
                                <td>
                                    <a href="ProductDetails.aspx?productID=<%#:Item.Id%>">
                                        <%--                                         <img src="/Images/<%#:Item.Name+".png"%>"--%>
                                        <img src="/Client/Products/ShowImage.ashx?id=<%#:Item.Id%>"
                                            width="100" height="75" style="border: solid" />
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="/Client/Products/ProductDetails.aspx?productID=<%#:Item.Id%>">
                                        <span>
                                            <%#:Item.Name%>
                                        </span>
                                    </a>
                                    <br />
                                    <span>
                                        <b>Precio: </b><%#:String.Format("{0:c}", Item.Price)%>
                                    </span>
                                    <br />
                                    <%--                                    Anadimos enlace a AddToCart--%>
                                    <a href="ProductDetails.aspx?productID=<%#:Item.Id %>">
                                        <span class="ProductListItem">
                                            <b>Ver Producto<b>
                                        </span>
                                    </a>
                                    <br />
                                    <span>
                                        <b>Categoría: <%#:Item.ProductCategory.ToString() %></b>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        </p>
                    </td>
                </ItemTemplate>
                <LayoutTemplate>
                    <table style="width: 100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <table id="groupPlaceholderContainer" runat="server" style="width: 100%">
                                        <tr id="groupPlaceholder"></tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr></tr>
                        </tbody>
                    </table>
                </LayoutTemplate>
            </asp:ListView>

        </div>
    </section>

</asp:Content>
