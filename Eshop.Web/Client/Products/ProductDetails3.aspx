﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails3.aspx.cs" Inherits="Eshop.Web.Client.Products.ProductDetails3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <header>
        <link href="/Content/ProductDetails.css" type="text/css" rel="stylesheet" />
        <script src="/Scripts/ProductDetails.js" type="text/javascript"></script>
    </header>

    <asp:FormView ID="productDetail" runat="server" ItemType="Eshop.CORE.Product" SelectMethod="GetProduct" RenderOuterTable="false">
        <ItemTemplate>
            
            <div class="product">
                <div class="product-photo">
                    <img src="/Client/Products/ShowImage.ashx?id=<%#:Item.Id%>" width="330" height="400" />
                </div>
                <div class="product-content">
                    <div class="infos">
                        <h3><%# Item.Name %></h3>
                        <p><%# Item.Description %></p>
                    </div>
                    <div class="price"><%#: string.Format("{0:#.00} €", Convert.ToDecimal(Item.Price)) %> </div>
                    <div class="actions">
                        <div class="cart">AÑADIR AL CARRITO</div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>
</asp:Content>
