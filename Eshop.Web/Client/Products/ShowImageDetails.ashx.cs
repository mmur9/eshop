﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eshop.Web.Client.Products
{
    /// <summary>
    /// Summary description for ShowImageDetails
    /// </summary>
    public class ShowImageDetails : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}