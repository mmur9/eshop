﻿<%@ Page Title="Editar Producto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductEdit.aspx.cs" Inherits="Eshop.Web.Admin.Products.ProductEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal">
        <h4>Editar Producto</h4>
        <asp:HiddenField ID="txtId" runat="server" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />

        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="Nombre" CssClass="col-md3" AssociatedControlID="mytxtName"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="mytxtName" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El Nombre es obligatorio" ControlToValidate="mytxtName" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Categoría" CssClass="col-md-0" AssociatedControlID="DropDownListCategory"></asp:Label><br />
            <div class="col-md-2">
                <asp:DropDownList ID="DropDownListCategory" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>

        <%--       <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Imagen" CssClass="col-md-3" AssociatedControlID="imgPreview" ></asp:Label>
            <div class="col-md-7">
                <asp:Image ID="imgPreview" width="300px" runat="server" BorderStyle="None" />   

            </div>
        </div>--%>

        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Precio" CssClass="col-md3" AssociatedControlID="mytxtPrice"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="mytxtPrice" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="El precio es obligatorio" ControlToValidate="mytxtPrice" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Stock" CssClass="col-md3" AssociatedControlID="mytxtStock"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="mytxtStock" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="El Stock es obligatorio" ControlToValidate="mytxtStock" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>
                <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Descripción" CssClass="col-md3" AssociatedControlID="mytxtDescription"></asp:Label>
            <div class="col-md16">
                <asp:TextBox ID="mytxtDescription" runat="server" CssClass="form-control"  TextMode="MultiLine" Rows="5"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="La descripción es obligatoria" ControlToValidate="mytxtDescription" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>



                <asp:Button ID="btnSubmit" runat="server" Text="Actualizar" CssClass="btn btn-primary btn-lg btn-block" OnClick="btnSubmit_Click" />
                  <asp:Button ID="btnDelete" runat="server" Text="Eliminar" CssClass="btn btn-danger btn-lg btn-block" OnClick="btnOpenModal_Click"/>

                



    </div>
              <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
   
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
<%--                  <h4 class="modal-title">Modal Header</h4>--%>
                </div>
                <div class="modal-body">
    <div class="form-group">
        <asp:Label ID="Label5" runat="server" Text="¿Está seguro de eliminar el producto? " CssClass="col-md3"></asp:Label>
        <div class="col-md9">

            <br />
            <asp:Button ID="Button1" runat="server" OnClick="btnDelete_Click" Text="Eliminar" CssClass="btn btn-danger" />

        </div>
    </div>                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
<%--                  <asp:Button ID="btnCloseModal" CssClass="btn btn-default"  runat="server" Text="Close & Reopen" onclick="btnCloseModal_Click" />--%>
                </div>
              </div>
     
            </div>
          </div>
</asp:Content>
