﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = Eshop.CORE.Image;

namespace Eshop.Web.Admin.Products
{
    public partial class ProductEdit : System.Web.UI.Page
    {
        //Creamos managers
        ApplicationDbContext context = null;
        ProductManager productManager = null;
        ImageManager imageManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Creamos contexto de datos e instancias de managers pasandoles el contexto
                context = new ApplicationDbContext();
                productManager = new ProductManager(context);
                imageManager = new ImageManager(context);

                //Inicializacion para tratar la variable id
                int id = 0;

                // Si hay un parametro llamado id
                if (Request.QueryString["id"] != null)
                {
                    // intentamos convertir lo que nos viene en el parametro a int, y lo pasamos a id
                    // en caso de ser true, el param es numero y se consigue pasar a entero
                    if (int.TryParse(Request.QueryString["id"], out id))
                    {
                        //intentamos coger el producto del contexto
                        var product = productManager.GetById(id);



                        // comprobamos que exista un producto con ese id
                        if (product != null)
                        {
                            //Cargamos producto
                            LoadProduct(product);

                            //rellenamos ddl

                            DropDownListCategory.DataSource = Enum.GetValues(typeof(Category));
                            DropDownListCategory.DataBind();
                            //Variable imagen --> devuelve imagen pasando el id del producto
                            var image = imageManager.GetById(product.Id);

                            //carga la imagen recibida 
                            //LoadImage(image);

                        //Aqui no hace falta, es admin:
                            //comprobamos si el producto id corresponde al usuario con el que estamos logueados
                            //if(incidence.User_Id == User.Identity.GetUserId()){
                            //LoadIncidence(indicence);
                            //}
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Metodo para cargar producto
        /// </summary>
        /// <param name="product"></param>
        private void LoadProduct(Product product)
        {
            txtId.Value = product.Id.ToString();
            mytxtName.Text = product.Name;
            mytxtStock.Text = product.UnitsInStock.ToString();
            mytxtPrice.Text = product.Price.ToString();
            //DropDownListCategory.Text = product.ProductCategory.ToString();
            mytxtDescription.Text = product.Description.ToString();
        }

        //private void GetImageData()
        //{
        //    string DbConnectionString = "Data Source=(LocalDb)" + @"\" + "MSSQLLocalDB;AttachDbFilename=|DataDirectory|" + @"\" + "aspnet-Eshop.Web-20190624050241.mdf;Initial Catalog=aspnet-Eshop.Web-20190624050241;Integrated Security=True";
        //    //verificamos cadena
        //    //Response.Write(DbConnectionString);


        //    try
        //    {
        //        //Creamos conexion
        //        SqlConnection connection = new SqlConnection(DbConnectionString);
        //        connection.Open();

        //        SqlCommand cmd = new SqlCommand("select ImageData, ProductImages.ProductId from Images where ProductId = 1;", connection);

        //        SqlDataReader dr = cmd.ExecuteReader();

                
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Write("<br>Ha habido un error al Acceder la imagen");
        //    }
        //}

        /// <summary>
        /// Metodo para cargar imagen
        /// </summary>
        /// <param name="image"></param>
        //private void LoadImage(Image image)
        //{
        //    try
        //    {
        //        byte[] bytes = (byte[])image.ImageData;

        //        // Convertimos la salida de bytes a strBase64
        //        string strBase64 = Convert.ToBase64String(bytes);

        //        //Insertamos en el elemento Image1 el data de la imagen

        //        imgPreview.ImageUrl = "data:Image/png;base64," + strBase64;

        //    }catch(Exception e)
        //    {
        //        Response.Write("<br>Ha habido un error al cargar la imagen");
        //    }

        //}


       //Metodo que actualiza un producto en funcion a los campos de la pagina
        public void UpdateProduct()
        {
            //Abrimos contexto
            context = new ApplicationDbContext();

            int productId = 0;
            //Recogemos de la url del producto
            int.TryParse(Request.QueryString["id"], out productId);

            //Recogemos el producto buscandolo por su ID
            var result = context.Products.SingleOrDefault(b => b.Id == productId);

            if (result != null)
            {
                //Recogemos el indice y lo parseamos a enum
                int selectedValueInt = DropDownListCategory.SelectedIndex;
                Category selectedValueEnum = (Category)Enum.ToObject(typeof(Category), selectedValueInt);

                //Actualizamos los valores del producto con los valores de los txt
                //result.ProductCategory = selectedValueEnum;
                result.UnitsInStock = Int32.Parse(mytxtStock.Text);
                result.Name = mytxtName.Text;
                result.Price = Int32.Parse(mytxtPrice.Text);

                //Guardamos cambios
                context.SaveChanges();
                //Redirigimos a la misma pagina para actualizar
                Response.Redirect("/Admin/Products/ProductEdit?id="+productId);
            }
        }

        /// <summary>
        /// Metodo que elimina el producto actual
        /// </summary>
        public void DeleteProduct()
        {
            //Abrimos contexto
            context = new ApplicationDbContext();
            productManager = new ProductManager(context);

            int productId = 0;
            //Recogemos de la url del producto
            int.TryParse(Request.QueryString["id"], out productId);
            //sacamos producto
            int myProductID = context.Products.SingleOrDefault(b => b.Id == productId).Id;

            Product myProduct = productManager.GetById(productId);
            //Eliminamos producto
            productManager.Remove(myProduct);
            context.SaveChanges();

            Response.Redirect("/Admin/Products/ProductList");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //Actualizamos producto 
                UpdateProduct();                

            }catch(Exception ex)
            {
                //TODO: escribir error en un log
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al modificar",
                    IsValid = false
                };
                Page.Validators.Add(err);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteProduct();
        }

        protected void btnOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        /// <summary>
        /// boton que cierra el modal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCloseModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalAgainShow", "$('#myModal').modal();", true);
        }
    }
}