﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Eshop.Web.Admin.Products
{
    public partial class ProductCreate : System.Web.UI.Page
    {
        ProductManager productManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
    
                // Creamos contexto de datos
                ApplicationDbContext context = new ApplicationDbContext();
                //Instanciamos productManager
                productManager = new ProductManager(context);

                //Sacamos a un ddl las categorias
                ddlType.DataSource = Enum.GetValues(typeof(Category));
                ddlType.DataBind();

            }
        
        /// <summary>
        /// Metodo al clicar el boton de crear producto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                // Almacenamos en variables int los valores de las propiedades int
                int myDuration = Int32.Parse(txtDuration.Text);
                decimal myPrice = decimal.Parse(txtPrice.Text);
                int myStock = Int32.Parse(txtStock.Text);
                int selectedValueInt = ddlType.SelectedIndex;
                Category selectedValueEnum = (Category)Enum.ToObject(typeof(Category), selectedValueInt);

                //Creamos product
                Product product = new Product
                {
                    //Añadimos Propiedades de producto desde los controles correspondientes
                    Name = txtProductName.Text,
                    Description = txtDescription.Text,
                    Duration = myDuration,
                    Price = myPrice,
                    UnitsInStock = myStock
                    //ProductCategory = selectedValueEnum


            };
                //Añadimos el product al product manager
                productManager.Add(product);
                productManager.Context.SaveChanges();
                //Pasamos el Id del producto cargado al metodo AddImage despues de crear el producto, asi podemos insertar esa info en la bd
                AddImage(product.Id);


                //Redireccionamos a la pagina de Edit del producto que se acaba de crear
                Response.Redirect("ProductEdit?Id=" + product.Id);
            }
            catch (Exception ex)
            {
                //TODO: escribir error en log
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al guardar el producto",
                    IsValid = false
                };
                Page.Validators.Add(err);
            }

        }


        /// <summary>
        /// Metodo para subir imagen a la BD
        /// </summary>
        /// <param name="productId">Id del producto </param>
        public void AddImage(int productId)
        {
            //Creamos cadena de conexion

            string DbConnectionString = "Data Source=(LocalDb)" + @"\" + "MSSQLLocalDB;AttachDbFilename=|DataDirectory|" + @"\" + "aspnet-Eshop.Web-20190624050241.mdf;Initial Catalog=aspnet-Eshop.Web-20190624050241;Integrated Security=True";
            //Response.Write(DbConnectionString + "<br/>");

            //Obtener info de la imagen
            int Size = fUploadImage.PostedFile.ContentLength;

            //Pasamos imagen a Byte
            byte[] OriginalImage = new byte[Size];

            fUploadImage.PostedFile.InputStream.Read(OriginalImage, 0, Size);

            //Pasamos la imagen a Bitmap
            Bitmap OriginalBinaryImage = new Bitmap(fUploadImage.PostedFile.InputStream);

            //Insertar en la BD

            //Abrimos conexion
            SqlConnection connection = new SqlConnection(DbConnectionString);
            SqlCommand cmd = new SqlCommand();

            //Creamos la sentencia SQL que se va a ejecutar
            cmd.CommandText = "INSERT INTO Images(ImageData, ImageTitle, ProductId) VALUES(@Imagen, @Titulo, @productId)";
            //cmd.CommandText = "INSERT INTO Images(ImageData, ImageTitle) VALUES(@Imagen, @Titulo);INSERT INTO ProductImages(ImageId, ProductId) VALUES(@imageId, @productId)";

            //Pasamos los parametros de la consulta
            cmd.Parameters.Add("@Imagen", SqlDbType.Image).Value = OriginalImage;
            cmd.Parameters.Add("@Titulo", SqlDbType.Text).Value = txtTitle.Text;
            //cmd.Parameters.Add("@imageId", SqlDbType.Int).Value = 9;
            cmd.Parameters.Add("@productId", SqlDbType.Int).Value = productId; //TODO PASAR PRODUCTO 

            //Response.Write(OriginalImage);


            cmd.CommandType = CommandType.Text;
            cmd.Connection = connection;

            //Bloque trycatch tratando de abrir la conexión y lanzar el comando
            try
            {
                connection.Open();
                cmd.BeginExecuteNonQuery();



            }
            catch (Exception ex)
            {
                Response.Write("Ha habido un error");
            }

            //Pasamos la imagen a string para mostrarla en el control imgPreview
            string ImageDataUrl64 = "data:image/jpg;base64," + Convert.ToBase64String(OriginalImage);
            imgPreview.ImageUrl = ImageDataUrl64;
        }


    }
}