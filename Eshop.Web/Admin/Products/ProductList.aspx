﻿<%@ Page Title="Listado Productos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="Eshop.Web.Admin.Products.ProductList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">    
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
    <br />
    <table id="Products">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Category</th>
                <th>Price</th>
                <th>Image</th>
            </tr>
        </thead>
    </table>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Products").DataTable({
                'bProcessing': true,
                'bServerSide': true,
                'sAjaxSource': '/Admin/Products/ProductServiceList.ashx',
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
                },
                "columns": [
                    { "data": "Id", "Name": "Id", "autoWidth": true },
                    { "data": "Title", "Name": "Title", "autoWidth": true },
                    { "data": "Category", "Name": "Category", "autoWidth": true },
                    { "data": "Price", "Name": "Price", "autoWidth": true }
                    //{ "data": "Image", "Name": "Image", "autoWidth": true },

                ],
                "columnDefs": [
                    {
                        "render": function (data, type, row) {
                            return "<a href='/Admin/Products/ProductEdit?id=" + row.Id + "' class='btn btn-pink'>" + data + "</a>";
                        },
                        "targets": 0
                    },
                    //{
                    //    "render": function (data, type, row) {
                    //        var dateString = data.substr(6);
                    //        var currentTime = new Date(parseInt(dateString));
                    //        var month = currentTime.getMonth() + 1;
                    //        var day = currentTime.getDate();
                    //        var year = currentTime.getFullYear();
                    //        var date = day + "/" + month + "/" + year;
                    //        return date;
                    //    },
                    //    "targets": 1
                    //}
                ]
            });
        });
</script>
</asp:Content>

