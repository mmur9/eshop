﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Admin.Products
{
    public partial class ImageView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Cadena de conexion db
            string DbConnectionString = "Data Source=(LocalDb)" + @"\" + "MSSQLLocalDB;AttachDbFilename=|DataDirectory|" + @"\" + "aspnet-Eshop.Web-20190624050241.mdf;Initial Catalog=aspnet-Eshop.Web-20190624050241;Integrated Security=True";
            //verificamos cadena
            //Response.Write(DbConnectionString);

                       
            try
            {
                //Creamos conexion
                SqlConnection connection = new SqlConnection(DbConnectionString);

                // Escogemos el procedimiento almacenado para recoger la imagen de la db
                SqlCommand cmd = new SqlCommand("spGetImageById", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                // Definimos los parametros del SP
                SqlParameter paramId = new SqlParameter()
                {
                    ParameterName = "@Id",
                    Value = Request.QueryString["Id"]
                };
                //Lo anadimos y abrimos conexion
                cmd.Parameters.Add(paramId);
                connection.Open();
                
                //Casteamos la salida a bytes
                byte[] bytes = (byte[])cmd.ExecuteScalar();

                // Convertimos la salida de bytes a strBase64
                string strBase64 = Convert.ToBase64String(bytes);

                //Insertamos en el elemento Image1 el data de la imagen
                Image1.ImageUrl = "data:Image/png;base64," + strBase64;


            }
            catch (Exception ex)
            {
                Response.Write("<br>Ha habido un error al cargar la imagen");
            }

        }
    }
}