﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UploadImage.aspx.cs" Inherits="Eshop.Web.Admin.Products.UploadImage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <div class="form-horizontal">
        <h4>Subir Imagen</h4>
        <hr />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />
      

       <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Seleccione la categoría" CssClass="col-md3" AssociatedControlID="ddlProductId"></asp:Label>
            <div class="col-md9">
                <asp:DropDownList ID="ddlProductId" runat="server" CssClass="form-control " AppendDataBoundItems="True"  Selected="True"> 
                </asp:DropDownList>
                

            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-md4 col-md-offset-4">
                    Imagen Agregada:
                    <br />
                    <br />
                    <asp:Image ID="imgPreview" runat="server" />
                    Archivo:
                    <asp:FileUpload ID="fUploadImage" accept=".jpg" runat="server" CssClass="form-control" />
                    <br />
                    <br />
                    Titulo de la Imagen:
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
<%--                    <asp:Button ID="btnUploadImage" runat="server" Text="Subir Imagen" CssClass="btn btn-success" OnClick="AddImage()"  />--%>
                </div>
            </div>
            <div class="row">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>

                    </ItemTemplate>
                </asp:Repeater>

            </div>

        </div>

        <div class="form-group">
            <div class="col-md-1 col-md-offset-3">
                <asp:Button ID="btnSubmit" runat="server" Text="Crear" CssClass="btn btn-default" OnClick="btnSubmit_Click"  />
            </div>
        </div>
    </div>
</asp:Content>
