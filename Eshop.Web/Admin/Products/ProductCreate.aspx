﻿<%@ Page Title="Crear Producto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductCreate.aspx.cs" Inherits="Eshop.Web.Admin.Products.ProductCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal">
        <h4>Crear producto</h4>
        <hr />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />

        <%--        NOMBRE--%>
        <div class="form-row">

            <div class="form-group shadow-textarea">
                <asp:Label ID="Label1" runat="server" Text="Introduzca el nombre del producto" CssClass="col-md3" AssociatedControlID="txtProductName"></asp:Label>
                <div class="col-md9">
                    <asp:TextBox ID="txtProductName" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El nombre del producto es obligatorio" ControlToValidate="txtProductName" Text="*"></asp:RequiredFieldValidator>
                </div>
            </div>

        </div>

        <%--        DESCRIPCION--%>
        <div class="form-group shadow-textarea">
            <asp:Label ID="Label2" runat="server" Text="Introduzca la descripción del producto" CssClass="col-md3" AssociatedControlID="txtDescription"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="4"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="El texto de la descripción es obligatorio" ControlToValidate="txtDescription" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>

        <%--DURACION--%>
        <div class="form-group shadow-textarea">
            <asp:Label ID="Label4" runat="server" Text="Introduzca la duración" CssClass="col-md3" AssociatedControlID="txtDuration"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtDuration" runat="server" CssClass="form-control"></asp:TextBox>

            </div>
        </div>

        <%--        PRECIO--%>
        <div class="form-group shadow-textarea">
            <asp:Label ID="Label5" runat="server" Text="Introduzca el precio" CssClass="col-md3" AssociatedControlID="txtPrice"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="El precio es obligatorio" ControlToValidate="txtPrice" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>

        <%--        CATEGORIA--%>
        <div class="form-group shadow-textarea">
            <asp:Label ID="Label3" runat="server" Text="Seleccione la categoría" CssClass="col-md2" AssociatedControlID="ddlType"></asp:Label>
            <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control " AppendDataBoundItems="True">
            </asp:DropDownList>

        </div>

        <%--        STOCK--%>
        <div class="form-group shadow-textarea">
            <asp:Label ID="Label6" runat="server" Text="Introduzca las unidades en Stock" CssClass="col-md3" AssociatedControlID="txtStock"></asp:Label>
            <div class="col-md9">
                <asp:TextBox ID="txtStock" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="El precio es obligatorio" ControlToValidate="txtPrice" Text="*"></asp:RequiredFieldValidator>
            </div>
        </div>

        <%--IMAGEN--%>
        <div class="container">
            <div class="col-md4 col-md-offset-4">
                Imagen Agregada:
                    <br />
                <br />
                <asp:Image ID="imgPreview" runat="server" />
                Archivo:
                    <asp:FileUpload ID="fUploadImage" accept=".jpg" runat="server" CssClass="form-control-file" />
                <br />
                <br />

                <%--                    <asp:Button ID="btnUploadImage" runat="server" Text="Subir Imagen" CssClass="btn btn-success" OnClick="AddImage()"  />--%>
            </div>
            <div class="form-group shadow-textarea">
            <asp:Label ID="Label7" runat="server" Text="Título de la imagen" CssClass="col-md3" AssociatedControlID="txtTitle"></asp:Label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                    </ItemTemplate>
                </asp:Repeater>
            </div>



        </div>
        <br />

                <asp:Button ID="btnSubmit" runat="server" Text="Crear" CssClass="btn btn-primary btn-lg btn-block" OnClick="btnSubmit_Click" />

    </div>
</asp:Content>
