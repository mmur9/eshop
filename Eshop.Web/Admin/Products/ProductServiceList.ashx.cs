﻿using Eshop.Application;
using Eshop.DAL;
using Eshop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;

namespace Eshop.Web.Admin.Products
{
    /// <summary>
    /// Summary description for ProductServiceList
    /// </summary>
    public class ProductServiceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var iDisplayLength = int.Parse(context.Request["iDisplaylength"]);
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            var sSearch = context.Request["sSearch"];
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];

            ApplicationDbContext contextdb = new ApplicationDbContext();
            ProductManager productManager = new ProductManager(contextdb);

            #region select

            //var allProducts = productManager.GetByUserId(context.User.Identity.GetUserId());

            // Recogemos todos los productos existentes
            var allProducts = productManager.GetProducts2();

            // recogemos los valores del modelo definido en la variable products
             var products = allProducts
                    .Select(p => new AdminProductList
                    {
                        Id = p.Id,
                        Title = p.Name.ToString(),
                        Category = p.Category.ToString(),
                        Price = p.Price
                        


                    });

            #endregion

            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Id.ToString().Contains(@0) || 
                               Title.ToString().Contains(@0) ||
                               Category.ToString().Contains(@0) ||
                               Price.ToString().Contains(@0)";
                products = products.Where(where, sSearch);

            }
            #endregion

            #region Paginate
            products = products
                .OrderBy(sortColum + " " + iSortDir)
                .Skip(iDisplayStart)
                .Take(iDisplayLength);

            #endregion

            var result = new
            {
                iTotalRecords = allProducts.Count(),
                iTotalDisplayRecords = allProducts.Count(),
                aaData = products
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}