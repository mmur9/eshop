﻿<%@ Page Title="Enviar Mail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SendMailForm.aspx.cs" Inherits="Eshop.Web.Admin.Mail.SendMailForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   
    <h1>Enviar Mail </h1><br />
    <div class="form-group row">
  <div class="col-xs-2">
    <label for="ex1">Para</label>
<%--      <asp:TextBox ID="txtRemitente" runat="server" CssClass="form-control" rows="5"></asp:TextBox>--%>
      <asp:DropDownList ID="ddlUsers" runat="server" CssClass="form-control" rows="5" Width="190px"></asp:DropDownList>
  </div>
  <div class="col-xs-6">
    <label for="ex2">Asunto</label>
      <asp:TextBox ID="txtAsunto" runat="server" CssClass="form-control" rows="5"></asp:TextBox>
  </div><br />

</div>
  <div class="form-group">
  <label for="comment">Mensaje:</label>
      <asp:TextBox ID="txtMensaje" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
</div>

        <asp:Button ID="Button1" runat="server" Text="Enviar" OnClick="Button1_Click" CssClass="btn btn-primary" />


</asp:Content>
