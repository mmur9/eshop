﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Admin.Mail
{
    public class SendMail
    {

        /// <summary>
        /// Clase que manda email
        /// </summary>
        /// <param name="destinatario">destinatario del mensaje</param>
        /// <param name="asunto">asunto del mensaje</param>
        /// <param name="mensaje">cuerpo del mensaje</param>
        public void SendMyMail(string destinatario, string asunto, string mensaje)
        {
            //Creamos instancia de MailMessage
            MailMessage mailMessage = new MailMessage();

            //Variables del correo
            mailMessage.Subject = asunto;
            mailMessage.To.Add(destinatario);
            mailMessage.Body = mensaje;
            mailMessage.Bcc.Add("adm-eshop-mmur@outlook.com");


            //Estaticas
            mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
            mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
            mailMessage.IsBodyHtml = true;
            //mail desde el que se mandan los correos
            mailMessage.From = new MailAddress("adm-eshop-mmur@outlook.com");

            //Cliente de correo
            //especificamos datos de conexion, servidor, puerto...
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("adm-eshop-mmur@outlook.com", "Pruebaingenieria!1");
            client.Port = 587;
            client.EnableSsl = true;
            client.Host = "SMTP.Office365.com";

            try
            {
                //Intenta mandar mensaje
                client.Send(mailMessage);

            }
            catch (Exception ex)
            {
                //TODO: escribir error en log
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al enviar el correo",
                    IsValid = false
                    
                };
            }




        }
    }
}