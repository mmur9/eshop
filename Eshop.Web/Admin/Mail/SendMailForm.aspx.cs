﻿using Eshop.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Admin.Mail
{
    public partial class SendMailForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //rellenamos ddl
                fillDDL();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            try
            {
                //Creamos nuevo mail
                SendMail sendMail = new SendMail();
                //Llamamos al metodo sendmymail con los parametros indicados de cuerpo, asunto, y destinatario desde el ddl
                sendMail.SendMyMail(ddlUsers.SelectedItem.ToString(), txtAsunto.Text, txtMensaje.Text);
                //redirigimos a la misma pagina
                Response.Redirect(Page.Request.RawUrl);

            }catch(Exception ex)
            {
                //TODO: escribir error en log
                var err = new CustomValidator
                {
                    ErrorMessage = "Se ha producido un error al mandar el mail",
                    IsValid = false
                };
                Page.Validators.Add(err);
            }

          
        }

        /// <summary>
        /// metodo que rellena el ddl
        /// </summary>
        public void fillDDL()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            //Consulta para sacar los productos y meterlos en un DDL
            string DbConnectionString = "Data Source=(LocalDb)" + @"\" + "MSSQLLocalDB;AttachDbFilename=|DataDirectory|" + @"\" + "aspnet-Eshop.Web-20190624050241.mdf;Initial Catalog=aspnet-Eshop.Web-20190624050241;Integrated Security=True";
            SqlConnection connection = new SqlConnection(DbConnectionString);
            string sqlQuery = "select * from [dbo].[AspNetUsers]";
            SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, connection);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            ddlUsers.DataSource = dt;
            ddlUsers.DataValueField = "Id";
            ddlUsers.DataTextField = "Email";


            //ddlProductId.DataSource = (from p in context.Products select new { p.Name }).ToList();

            ddlUsers.DataBind();
        }
    }
}