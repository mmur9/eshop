﻿<%@ Page Title="Roles" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageRoles.aspx.cs" Inherits="Eshop.Web.Admin.Users.ManageRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <header>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </header>
    <h4>Roles:</h4>
    <br />
    <asp:GridView ID="GridView1" CssClass="table table-striped" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSourceRoles">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
        </Columns>
    </asp:GridView>
    <h4>Usuarios - Roles</h4>
    <br />
    <asp:GridView ID="GridView2" CssClass="table table-striped" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceUserRoles">
        <Columns>
            <asp:BoundField DataField="Email" HeaderText="Usuario" SortExpression="Email" />
            <asp:BoundField DataField="Name" HeaderText="Rol" SortExpression="Name" />
        </Columns>
    </asp:GridView>
    <br />
    <%--    <div class="form-group">
        <asp:Label ID="Label4" runat="server" Text="Nuevo Rol" CssClass="col-md3" AssociatedControlID="txtNewRole"></asp:Label>
        <div class="col-md9">
            <asp:TextBox ID="txtNewRole" runat="server" CssClass="form-control">
            </asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Crear" CssClass="btn btn-primary" />

        </div>
    </div>--%>
          

      <h2></h2>
          <!-- Trigger the modal with a button -->
          <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Crear Rol</button>
          <!--ASP.NET Button -->
<%--            <asp:Button ID="btnOpenModal" runat="server" CssClass="btn btn-info btn-lg" Text="Open with ASP Button" onclick="btnOpenModal_Click" />--%>
      
          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
   
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
<%--                  <h4 class="modal-title">Modal Header</h4>--%>
                </div>
                <div class="modal-body">
    <div class="form-group">
        <asp:Label ID="Label4" runat="server" Text="Crear nuevo Rol" CssClass="col-md3" AssociatedControlID="txtNewRole"></asp:Label>
        <div class="col-md9">
            <asp:TextBox ID="txtNewRole" runat="server" CssClass="form-control">
            </asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Crear" CssClass="btn btn-primary" />

        </div>
    </div>                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<%--                  <asp:Button ID="btnCloseModal" CssClass="btn btn-default"  runat="server" Text="Close & Reopen" onclick="btnCloseModal_Click" />--%>
                </div>
              </div>
     
            </div>
          </div>
    <asp:SqlDataSource ID="SqlDataSourceUserRoles" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT  [AspNetUsers].Email , [AspNetRoles].Name FROM [AspNetUserRoles] inner join [AspNetRoles] on [AspNetUserRoles].RoleId = [AspNetRoles].Id inner join [AspNetUsers] on [AspNetUserRoles].UserId = [AspNetUsers].Id"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSourceRoles" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT Name, Id FROM [AspNetRoles]"></asp:SqlDataSource>
</asp:Content>
