﻿using Eshop.CORE;
using Eshop.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Admin.Users
{
    public partial class ManageRoles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

           

            
        }
        /// <summary>
        /// Metodo que crea un nuevo rol
        /// </summary>
        public void CreateRole()
        {
            //Abrimos contexto
            ApplicationDbContext context = null;
            context = new ApplicationDbContext();

            //Inicializamos rolemanager de identity
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));


            IdentityResult roleResult;

            //Captamos el nombre del rol desde el label
            string newRole = txtNewRole.Text.ToString();

            //Comprobamos si existe un rol con ese nombre
            if (!RoleManager.RoleExists(newRole))
            {
                roleResult = RoleManager.Create(new IdentityRole(newRole));
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Al pulsar en el boton se crea y recarga la pagina par ver los cambios en la tabla
            CreateRole();
            Response.Redirect(Request.RawUrl);

        }
        /// <summary>
        /// Metodo que abre modal para crear rol
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        /// <summary>
        /// boton que cierra el modal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCloseModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalAgainShow", "$('#myModal').modal();", true);
        }
    }
}