﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Admin.Users
{
    public partial class UserEdit : System.Web.UI.Page
    {
  
            //Creamos managers
            ApplicationDbContext context = null;
            MyUserManager myUserManager = null;
            ApplicationUser applicationUser = null;


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                //Creamos contexto de datos e instancias de managers pasandoles el contexto
                context = new ApplicationDbContext();
                myUserManager = new MyUserManager(context);
                applicationUser = new ApplicationUser();
                

                //Inicializacion para tratar la variable email
                string email = Request.QueryString["Email"];


                // Si hay un parametro llamado email
                if (Request.QueryString["Email"] != null)
                {


                    //intentamos coger el usuario que contenga ese mail
                    var myUser = myUserManager.GetUsersByEmail(email);
                    //Response.Write(email);




                    // comprobamos que exista un usuario con ese email
                    if (myUser != null)
                    {

                        try
                        {

                            // Cargamos el DDL con los roles existentes
                            string DbConnectionString = "Data Source=(LocalDb)" + @"\" + "MSSQLLocalDB;AttachDbFilename=|DataDirectory|" + @"\" + "aspnet-Eshop.Web-20190624050241.mdf;Initial Catalog=aspnet-Eshop.Web-20190624050241;Integrated Security=True";
                            SqlConnection connection = new SqlConnection(DbConnectionString);
                            string sqlQuery = "select Name from [dbo].[AspNetRoles]";
                            SqlDataAdapter sda = new SqlDataAdapter(sqlQuery, connection);
                            DataTable dt = new DataTable();
                            sda.Fill(dt);
                            DropDownListRole.DataSource = dt;
                            //DropDownListRole.DataValueField = "Id";
                            DropDownListRole.DataTextField = "Name";

                            DropDownListRole.DataBind();


                            //Cargamos usuario
                            LoadUser(myUser.FirstOrDefault());



                        }
                        catch (Exception ex)
                        {
                            //TODO: escribir error en un log
                            var err = new CustomValidator
                            {
                                ErrorMessage = "Se ha producido un error al visualizar el usuario",
                                IsValid = false
                            };
                            Page.Validators.Add(err);
                        }



                    }

                }

            }
        }


        /// <summary>
        /// Metodo para cargar los datos del usuario en cuestión en el formulario
        /// </summary>
        /// <param name="user"></param>
            private void LoadUser(ApplicationUser user)
             {
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));


            txtId.Value = user.Id.ToString();
            txtEmail.Text = user.Email.ToString();
            txtPhoneConfirmed.Text = user.PhoneNumberConfirmed.ToString();
            txtTwoFactor.Text = user.TwoFactorEnabled.ToString();
            txtAccessFail.Text = user.TwoFactorEnabled.ToString();
            txtMailConfirmed.Text = user.EmailConfirmed.ToString();
            //DropDownListRole.Text = applicationUserManager.GetRolesAsync(user.Id.ToString()).ToString();

            // Si no tiene tlfn ponemos sin tlfn
            if (user.PhoneNumber != null) {
                txtPhone.Text = user.PhoneNumber.ToString();
            }
            else
            {
                txtPhone.Text = "Sin Telefono";
            }
            //si tiene rol, mostramos el rol actual, sino "sin rol"
            if (user.Roles.Count == 1)
            {
                string oldRoleId = user.Roles.FirstOrDefault().RoleId.ToString();

                string oldRoleName = RoleManager.FindById(oldRoleId).Name.ToString();

                txtRole.Text = oldRoleName;

            }
            else
            {
                txtRole.Text = "Sin Rol";
            }

        }

        /// <summary>
        /// Metodo que actualiza el rol de un usuario
        /// </summary>
        public void UpdateRole()
        {
            //Abrimos contexto

            context = new ApplicationDbContext();
            //applicationUserManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));



            string userId = "";

            //Recogemos el email del usuario
            userId = Request.QueryString["Email"].ToString();

            //Recogemos el usuario buscandolo por su ID
            var result = context.Users.SingleOrDefault(b => b.Email == userId);

            if (result != null)
            {
                //Recogemos el nº de roles
                int numRoles = result.Roles.Count();

                //recogemos usuario
                var user = UserManager.FindByEmail(userId);
                string newRole = "none";


                //Actualizamos el rol del usuario
                if (result.Roles.Count() > 0)
                {
                    //variable que recoge el rol viejo 
                    string oldRoleId = result.Roles.FirstOrDefault().RoleId.ToString();
                    
                    //recogemos el rol nuevo del ddl
                    newRole = DropDownListRole.SelectedValue.ToString();

                    string oldRoleName = RoleManager.FindById(oldRoleId).Name.ToString();

                   //Eliminamos rol viejo
                    UserManager.RemoveFromRole(user.Id, oldRoleName );
                    //Añadimos rol nuevo, ya que segun la estructura, no es compatible ser multirol, ya que el admin tambien puede comprar
                    UserManager.AddToRole(user.Id, newRole);

                    context.SaveChanges();

                }
                else
                {
                    try
                    {
                        //variable que selecciona el nuevo rol
                        newRole = DropDownListRole.SelectedValue.ToString();
                        UserManager.AddToRole(user.Id, newRole);
                        context.SaveChanges();
                    }
                    catch
                    {
                        throw;
                    }
                }

                //Guardamos cambios
                //context.SaveChanges();
                //Recargamos página
                Response.Redirect("/Admin/Users/UserEdit?Email=" + userId);



            }
        }




        protected void btnSubmit_Click(object sender, EventArgs e)
            {
                try
                {
                UpdateRole();

                }
                catch (Exception ex)
                {
                    //TODO: escribir error en un log
                    var err = new CustomValidator
                    {
                        ErrorMessage = "Se ha producido un error al modificar",
                        IsValid = false
                    };
                    Page.Validators.Add(err);
                }


            }
 
        /// <summary>
        /// Si pulsamos en el boton de cambiar contraseña, nos redireccionará a la pagina correspondiente,
        /// de la propia sesion, un usuario no puede cambiar contraseñas de otros usuarios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/ManagePassword.aspx");

        }
    }
    }
    
