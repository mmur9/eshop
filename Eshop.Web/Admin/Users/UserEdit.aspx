﻿<%@ Page Title="Editar usuario" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserEdit.aspx.cs" Inherits="Eshop.Web.Admin.Users.UserEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal">
        <h4>Editar Producto</h4>
        <asp:HiddenField ID="txtId" runat="server" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />

        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="E-mail" CssClass="col-md-3" AssociatedControlID="txtEmail"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtEmail" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>


        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="E-mail Confirmado" CssClass="col-md-3" AssociatedControlID="txtMailConfirmed"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtMailConfirmed" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Telefono" CssClass="col-md-3" AssociatedControlID="txtPhone"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtPhone" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Telefono confirmado" CssClass="col-md-3" AssociatedControlID="txtPhoneConfirmed"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtPhoneConfirmed" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Two Factor Enabled" CssClass="col-md-3" AssociatedControlID="txtTwoFactor"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtTwoFactor" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label6" runat="server" Text="Accesos Fallidos" CssClass="col-md-3" AssociatedControlID="txtAccessFail"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtAccessFail" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label8" runat="server" Text="Rol" CssClass="col-md-3" AssociatedControlID="txtRole"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtRole" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Cambiar Rol" CssClass="col-md-3" AssociatedControlID="DropDownListRole"></asp:Label>
            <div class="col-md-9">
                <asp:DropDownList ID="DropDownListRole" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>


        <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" Text="Actualizar" CssClass="btn btn-primary btn-lg btn-block" OnClick="btnSubmit_Click" />
        </div>



    </div>
</asp:Content>

