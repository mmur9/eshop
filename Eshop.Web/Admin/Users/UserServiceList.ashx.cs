﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using Eshop.Web.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Script.Serialization;

namespace Eshop.Web.Admin.Users
{
    /// <summary>
    /// Summary description for UserServiceList
    /// </summary>
    public class UserServiceList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var iDisplayLength = int.Parse(context.Request["iDisplaylength"]);
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            var sSearch = context.Request["sSearch"];
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];

            ///Creamos managers
            ApplicationDbContext contextdb = new ApplicationDbContext();
            MyUserManager myUserManager = new MyUserManager(contextdb);

            #region select
            // Creamos una variable que recoge todos los usuarios
            var allUsers = myUserManager.GetUsers();
            // Almacenamos en la variable users el resultado de los campos del model sobre los usuarios de allusers
            var users = allUsers
                   .Select(p => new UserListModel
                   {
                       Email = p.Email.ToString(),
                       Username = p.UserName.ToString()



                   });

            #endregion

            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Email.ToString().Contains(@0) ||
                               Username.ToString().Contains(@0)";
                users = users.Where(where, sSearch);

            }
            #endregion

            #region Paginate
            users = users
                .OrderBy(sortColum + " " + iSortDir)
                .Skip(iDisplayStart)
                .Take(iDisplayLength);

            #endregion

            var result = new
            {
                iTotalRecords = allUsers.Count(),
                iTotalDisplayRecords = allUsers.Count(),
                aaData = users
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}