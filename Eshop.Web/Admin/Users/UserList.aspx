﻿<%@ Page Title="Listado Usuarios" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="Eshop.Web.Admin.Users.UserList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
    <br />
    <table id="Users">
        <thead>
            <tr>
                <th>Email</th>
                <th>Username</th>

            </tr>
        </thead>
    </table>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Users").DataTable({
                'bProcessing': true,
                'bServerSide': true,
                'sAjaxSource': '/Admin/Users/UserServiceList.ashx',
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
                },
                "columns": [
                    { "data": "Email", "Name": "Email", "autoWidth": true },
                    { "data": "Username", "Name": "Username", "autoWidth": true }

                ],
                "columnDefs": [
                    {
                        "render": function (data, type, row) {
                            return "<a href='/Admin/Users/UserEdit?Email=" + row.Email + "' class='btn btn-pink'>" + data + "</a>";
                        },
                        "targets": 0
                    },
                  
                ]
            });
        });
</script>
</asp:Content>
