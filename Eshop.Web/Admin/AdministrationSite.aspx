﻿<%@ Page Title="Panel Administración" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrationSite.aspx.cs" Inherits="Eshop.Web.Admin.AdministrationSite" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>

    <div>
        <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
            <p class="text-success">Exito</p>
        </asp:PlaceHolder>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
                <h4></h4>
                <hr />
                <dl class="dl-horizontal">
                    <dt>Email:</dt>
                    <dd>
                        <p>
                            <%: Context.User.Identity.GetUserName()  %>
                        </p>

                    </dd>
                </dl>
                <h5>Usuarios</h5>

                <dl class="dl-horizontal">
                    <dt>Listar Usuarios:</dt>
                    <dd>
                        <asp:HyperLink NavigateUrl="/Admin/Users/UserList" Text="[Ver]" Visible="true" ID="HyperLink3" runat="server" />

                    </dd>
                     <dt>Administrar Roles:</dt>
                    <dd>
                        <asp:HyperLink NavigateUrl="/Admin/Users/ManageRoles" Text="[Ver]" Visible="true" ID="HyperLink6" runat="server" />

                    </dd>
                    <dt>Enviar Mail a Usuario</dt>
                    <dd>
                        <asp:HyperLink NavigateUrl="/Admin/Mail/SendMailForm" Text="[Enviar]" Visible="true" ID="HyperLink2" runat="server" />
                    </dd>
                </dl>
                <h5>Pedidos</h5>

                <dl class="dl-horizontal">
                    <dt>Ver Pedidos</dt>
                    <dd>
                        <asp:HyperLink NavigateUrl="/Admin/Orders/OrderList" Text="[Ver]" Visible="true" ID="HyperLink1" runat="server" />
                    </dd>
                </dl>
                <h5>Productos</h5>

                <dl class="dl-horizontal">
                    <dt>Crear Producto</dt>
                    <dd>
                        <asp:HyperLink NavigateUrl="/Admin/Products/ProductCreate" Text="[Crear]" Visible="true" ID="HyperLink5" runat="server" />
                    </dd>

                    <dt>Ver Productos</dt>
                    <dd>
                        <asp:HyperLink NavigateUrl="/Admin/Products/ProductList" Text="[Ver]" Visible="true" ID="HyperLink4" runat="server" />
                    </dd>


                </dl>
            </div>
        </div>
    </div>

</asp:Content>
