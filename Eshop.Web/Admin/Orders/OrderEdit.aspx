﻿<%@ Page Title="Editar Pedido" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderEdit.aspx.cs" Inherits="Eshop.Web.Admin.Orders.OrderEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-horizontal">
        <h4>Editar Pedido</h4><br />

        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" />

        <div class="form-group">
            <asp:Label ID="Label1" runat="server" Text="ID" CssClass="col-md-3" AssociatedControlID="txtId"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtId" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Usuario" CssClass="col-md-3" AssociatedControlID="txtUser"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtUser" runat="server" Text="" CssClass="form-control"></asp:Label>

            </div>
        </div>


        <div class="form-group">
            <asp:Label ID="Label4" runat="server" Text="Nombre" CssClass="col-md-3" AssociatedControlID="txtName"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtName" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label5" runat="server" Text="Apellido" CssClass="col-md-3" AssociatedControlID="txtLastName"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtLastName" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label6" runat="server" Text="Dirección" CssClass="col-md-3" AssociatedControlID="txtAddress"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtAddress" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label7" runat="server" Text="Teléfono" CssClass="col-md-3" AssociatedControlID="txtPhone"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtPhone" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label8" runat="server" Text="Importe" CssClass="col-md-3" AssociatedControlID="txtPrice"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtPrice" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

        <div class="form-group">
            <asp:Label ID="Label9" runat="server" Text="Fecha" CssClass="col-md-3" AssociatedControlID="txtDate"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="txtDate" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>

<%--        <div class="form-group">
            <asp:Label ID="Label2" runat="server" Text="Estado" CssClass="col-md-3" AssociatedControlID="ddlStatus"></asp:Label>
            <div class="col-md-9">
                <asp:Label ID="ddlStatus" runat="server" Text="" CssClass="form-control"></asp:Label>
            </div>
        </div>--%>

                <div class="form-group">
            <asp:Label ID="Label10" runat="server" Text="Estado" CssClass="col-md-3" AssociatedControlID="ddlStatus2"></asp:Label>
            <div class="col-md-9">
                <asp:DropDownList ID="ddlStatus2" runat="server" CssClass="form-control"></asp:DropDownList>            </div>
        </div>

        <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" Text="Actualizar" CssClass="btn btn-primary btn-lg btn-block" OnClick="btnSubmit_Click" />
   
        </div>



    </div>
</asp:Content>

