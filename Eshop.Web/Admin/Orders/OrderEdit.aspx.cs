﻿using Eshop.Application;
using Eshop.CORE;
using Eshop.DAL;
using Eshop.Web.Admin.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eshop.Web.Admin.Orders
{
    public partial class OrderEdit : System.Web.UI.Page
    {
        //Creamos managers
        ApplicationDbContext context = null;
        OrderManager orderManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Creamos contexto de datos e instancias de managers pasandoles el contexto
                context = new ApplicationDbContext();
                orderManager = new OrderManager(context);

                //Inicializacion para tratar la variable id
                int id = 0;

                // Si hay un parametro llamado id
                if (Request.QueryString["id"] != null)
                {
                    // intentamos convertir lo que nos viene en el parametro a int, y lo pasamos a id
                    // en caso de ser true, el param es numero y se consigue pasar a entero
                    if (int.TryParse(Request.QueryString["id"], out id))
                    {
                        //intentamos coger el producto del contexto
                        var order = orderManager.GetById(id);



                        // comprobamos que exista un pedido con ese id
                        if (order != null)
                        {
                            //Cargamos producto
                            LoadOrder(order);

                            //rellenamos ddl con los valores del enumerado
                            ddlStatus2.DataSource = Enum.GetValues(typeof(OrderStatus));
                            ddlStatus2.DataBind();

                        }
                    }
                }
            }
        }
        /// <summary>
        /// Metodo que carga el pedido
        /// </summary>
        /// <param name="order"></param>
        private void LoadOrder(Order order)
        {
            txtId.Text = order.Id.ToString();
            txtUser.Text = order.Email.ToString();
            txtName.Text = order.FirstName.ToString();
            txtLastName.Text = order.LastName.ToString();
            txtAddress.Text = order.Address.ToString();
            txtPhone.Text = order.Phone.ToString();
            txtPrice.Text = order.Total.ToString()+" €";
            txtDate.Text = order.OrderDate.ToString();
            ddlStatus2.Text = order.Status.ToString();


        }

        /// <summary>
        /// Metodo que actualiza el estado de un pedido
        /// </summary>
        public void UpdateStatus()
        {
            //Abrimos contexto
            context = new ApplicationDbContext();
            SendMail sendMail = new SendMail();


            int orderId = 0;
            //Recogemos de la url el pedido
            int.TryParse(Request.QueryString["id"], out orderId);

            //Recogemos el pedido buscandolo por su ID
            var result = context.Orders.SingleOrDefault(b => b.Id == orderId);

            if (result != null)
            {
                //Recogemos el indice y lo parseamos a enum
                int selectedValueInt = ddlStatus2.SelectedIndex;
                OrderStatus selectedValueEnum = (OrderStatus)Enum.ToObject(typeof(OrderStatus), selectedValueInt);

                //Actualizamos el status del pedido
                result.Status = selectedValueEnum;

                //Guardamos cambios
                context.SaveChanges();

                //Mandamos mail al cliente informando del cambio

                sendMail.SendMyMail(result.Email.ToString(), "Actualización sobre su pedido "+orderId+" Eshop", "Su pedido "+orderId+" ha sido actualizado, su nuevo estado es "+result.Status.ToString());

                //Redirigimos a la misma pagina a modo de refresco
                Response.Redirect("/Admin/Orders/OrderEdit?id="+orderId);


                
            }
        }
            protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //Se actualiza el estado
            UpdateStatus();
            

        }

       
    }
}